<?php
    global $wpdb;

    if(isset($_FILES['import'])) {
        $row = 1;
        if(($handle = fopen($_FILES['import']["tmp_name"], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
                if( $row > 1 ) {
                    createNewDoctor ($data, $wpdb);
                }
                $row++;
            }
            fclose($handle);
            exit;
        }
    }

    function createNewDoctor ($data, $wpdb) {
        $my_post = array();
        $my_post['post_title'] = strip_tags(strval($data[0]));
        $my_post['post_content'] = strip_tags(strval($data[2]));
        $my_post['post_status'] = 'publish';
        $my_post['post_type'] = 'doctors';
        $new_post = wp_insert_post( $my_post );

        $state = '"'.strval(strip_tags($data[1])).'"';
        $quote = '"'.strval(strip_tags($data[3])).'"';
        $certifications = '"'.strval(strip_tags($data[4])).'"';
        $degree = '"'.strval(strip_tags($data[5])).'"';
        $school = '"'.strval(strip_tags($data[6])).'"';
        $medical_degree = '"'.strval(strip_tags($data[7])).'"';
        $medical_school = '"'.strval(strip_tags($data[8])).'"';
        $residency = '"'.strval(strip_tags($data[9])).'"';
        $other_degrees = '"'.strval(strip_tags($data[10])).'"';
        $image_profile = '"'.strval(strip_tags($data[11])).'"';
        
        $wpdb->query("INSERT INTO wp_postmeta
                (post_id, meta_key, meta_value)
                VALUES
                ($new_post, 'doctor_biography_state', $state),
                ($new_post, '_doctor_biography_state', 'field_608aac99429c6'),
                ($new_post, 'doctor_biography_k_quote', $quote),
                ($new_post, '_doctor_biography_k_quote', 'field_608aacb4429c7'),
                ($new_post, 'doctor_biography_board_certifications', $certifications),
                ($new_post, '_doctor_biography_board_certifications', 'field_608aacc9429c8'),
                ($new_post, 'doctor_biography_undergraduate_degree', $degree),
                ($new_post, '_doctor_biography_undergraduate_degree', 'field-608aacd7429c9'),
                ($new_post, 'doctor_biography_undergraduate_school', $school),
                ($new_post, '_doctor_biography_undergraduate_school', 'field_608aace8429ca'),
                ($new_post, 'doctor_biography_medical_degree', $medical_degree),
                ($new_post, '_doctor_biography_medical_degree', 'field_608aad08429cb'),
                ($new_post, 'doctor_biography_medical_school', $medical_school),
                ($new_post, '_doctor_biography_medical_school', 'field_608aad1d429cc'),
                ($new_post, 'doctor_biography_residency', $residency),
                ($new_post, '_doctor_biography_residency', 'field_608aad31429cd'),
                ($new_post, 'doctor_biography_other_degrees', $other_degrees),
                ($new_post, '_doctor_biography_other_degrees', 'field_608aad4a429ce'),
                ($new_post, 'doctor_biography_image_profile', $image_profile),
                ($new_post, '_doctor_biography_image_profile', 'field_608aad61429cf')
            ");
    }
?>
<div class="wrap">
    <h1>Import doctors</h1>
    <div class="narrow">
        <form enctype="multipart/form-data"  method="post" class="wp-upload-form">
            <p>
                <label for="upload">Choose a file from your computer:</label> (Maximum size: 2 MB)<input type="file"  name="import" size="25">
                <input type="hidden" name="action" value="save">
                <input type="hidden" name="max_file_size" value="2097152">
            </p>
            <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Upload file and import" disabled=""></p>
        </form>
    </div>
</div>