<?php
/*
 * Add my new menu to the Admin Control Panel
 */
 
// Hook the 'admin_menu' action hook'
add_action( 'admin_menu', 'add_import_doctors_admin_link' );

// Add a new top level menu link to the ACP
function add_import_doctors_admin_link()
{
    add_menu_page(
        'Import doctors', // Title of the page
        'Import doctors', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        plugin_dir_path(__FILE__).'/import-doctors.php' // The 'slug' - file to display when clicking the link
    );
}