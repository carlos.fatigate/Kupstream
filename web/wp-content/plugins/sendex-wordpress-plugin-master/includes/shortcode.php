<?php
require_once( plugin_dir_path( __FILE__ ) .'../twilio-lib/src/Twilio/autoload.php');
use Twilio\Rest\Client;

if (isset($_POST["numbers"])) {

    $to        = (isset($_POST["numbers"])) ? $_POST["numbers"] : "";
    $ddi       = (isset($_POST["ddi"])) ? $_POST["ddi"] : "";
    $message   = 'Welcome to K! Tap the link below to download the K Health app. https://kh-i1.onelink.me/cX7T';

    $to = $ddi.$to;

    try {
        $client = new Client('ACd07895f2d5e043bcbc99efbd79b84032', '6ec82edb8b1a366141df3e7be95d95bf');
        $phone_number = $client->lookups->v1->phoneNumbers("+15108675310")->fetch(["type" => ["carrier"]]);

        if(strcmp($phone_number->carrier['type'], "mobile") == 0 && (strcmp($phone_number->carrier['type'], "US") == 0 || strcmp($phone_number->carrier['type'], "IL") == 0)){
            $response = $client->messages->create(
                $to,
                array(
                    "from" => '+16466877309',
                    "body" => $message
                )
            );
        }
    
        // self::DisplaySuccess();
    } catch (Exception $e) {
    
        // self::DisplayError($e->getMessage());
    }
}


defined( 'ABSPATH' ) or die( 'Direct script access disallowed.' );

add_shortcode( 'sendex', function( $atts ) {
  $default_atts = array();
  $args = shortcode_atts( $default_atts, $atts );

  return '
            <div class="phone_hero_twillio_box">
                <form method="post" name="cleanup_options" action="">
                    <p class="phone_text_hero_twillio">Phone number</p>
                    <input name="numbers" id="jquery-intl-phone" class="phone_hero_twillio" placeholder="Enter mobile phone number" type="tel" required>
                    <input  id="ddi" name="ddi"  type="hidden" >
                    <p class="phone_description_hero_twillio">We’ll text the app download link right to your phone</p>
                    <input class="cta_hero_twillio" type="submit" value="' . get_field('cta_text_hero_twillio') . '" name="send_sms_message" />
                </form>
            </div>
        ';
});