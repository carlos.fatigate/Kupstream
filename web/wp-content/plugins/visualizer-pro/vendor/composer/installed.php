<?php return array(
    'root' => array(
        'pretty_version' => 'v1.10.2',
        'version' => '1.10.2.0',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'a3909bb1aaa6d0719637038452853f23d012619e',
        'name' => 'codeinwp/visualizer-pro',
        'dev' => false,
    ),
    'versions' => array(
        'adodb/adodb-php' => array(
            'pretty_version' => 'v5.21.0',
            'version' => '5.21.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../adodb/adodb-php',
            'aliases' => array(),
            'reference' => '199391f639a0b3346a80d66c00c87230fa3e6a07',
            'dev_requirement' => false,
        ),
        'codeinwp/themeisle-sdk' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../codeinwp/themeisle-sdk',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => 'aeef3f159c1b35451d87672b6984ccde36c0d21d',
            'dev_requirement' => false,
        ),
        'codeinwp/visualizer-pro' => array(
            'pretty_version' => 'v1.10.2',
            'version' => '1.10.2.0',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'a3909bb1aaa6d0719637038452853f23d012619e',
            'dev_requirement' => false,
        ),
    ),
);
