<?php
/*
 * Add my new menu to the Admin Control Panel
 */
 
// Hook the 'admin_menu' action hook'
add_action( 'admin_menu', 'add_Import_Price_Admin_Link' );

// Add a new top level menu link to the ACP
function add_Import_Price_Admin_Link()
{
    add_menu_page(
        'Import drug price', // Title of the page
        'Import drug price', // Text to show on the menu link
        'manage_options', // Capability requirement to see the link
        plugin_dir_path(__FILE__).'/import-price.php' // The 'slug' - file to display when clicking the link
    );
}