<?php
    global $wpdb;

    if(isset($_FILES['import'])) {
        $row = 1;
        $last_post = '';
        $post_category = 1;
        if(($handle = fopen($_FILES['import']["tmp_name"], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
                if( $row > 1 ) {
                    if( strcmp($data[0],$last_post) != 0) {
                        $post_category = getPostCategory($data[1], $wpdb);
                    }
                    savePrice ($data, $post_category, $wpdb);
                }
                $last_post = $data[1];
                $row++;
            }
            fclose($handle);
            exit;
        }
    }

    function getPostCategory ($category_name, $wpdb) {
        $post_category_query = $wpdb->get_results( "SELECT t.term_id FROM $wpdb->terms AS t
            INNER JOIN $wpdb->term_taxonomy AS tt ON t.term_id = tt.term_id
            WHERE tt.taxonomy = 'drugpricecat' AND t.name = '$category_name'"
        );
        
        if(sizeof($post_category_query) == 0){
            $cat_defaults = array(
                'taxonomy'             => 'drugpricecat',
                'cat_name'             => $category_name,
            );
            return wp_insert_category($cat_defaults);
        } else {
            return $post_category_query[0]->term_id;
        }
    }

    function savePrice($data, $post_category, $wpdb){
        $name = strtolower($data[1]).'-'.strtolower($data[2]).'-'.strtolower($data[3]).'-'.strtolower($data[4]);
        $post_query = $wpdb->get_results( "SELECT p.ID FROM wp_posts AS p
            INNER JOIN wp_postmeta AS pm ON p.ID = pm.post_id
            WHERE pm.meta_key = 'cod_drug_table' AND pm.meta_value = '$data[0]' 
            AND p.post_title = '$name'"
        );

        if(sizeof($post_query) == 0){
            createNewPrice ($data, $post_category, $wpdb);
        } else {
            updatePrice ($data, $post_query[0]->ID, $wpdb);
        }
    }

    function updatePrice ($data, $post_id, $wpdb) {
        $wpdb->query("UPDATE wp_postmeta SET meta_value = '$data[1]' WHERE post_id = '$post_id' AND meta_key = 'name_drug_table' ");
        $wpdb->query("UPDATE wp_postmeta SET meta_value = '$data[2]' WHERE post_id = '$post_id' AND meta_key = 'packaging_drug_table' ");
        $wpdb->query("UPDATE wp_postmeta SET meta_value = '$data[3]' WHERE post_id = '$post_id' AND meta_key = 'strength_drug_table' ");
        $wpdb->query("UPDATE wp_postmeta SET meta_value = '$data[4]' WHERE post_id = '$post_id' AND meta_key = 'quantity_drug_table' ");
        $wpdb->query("UPDATE wp_postmeta SET meta_value = '$data[5]' WHERE post_id = '$post_id' AND meta_key = 'price_drug_table' ");
    }

    function createNewPrice ($data, $post_category, $wpdb) {
        $my_post = array();
        $my_post['post_title'] = strtolower($data[1]).'-'.strtolower($data[2]).'-'.strtolower($data[3]).'-'.strtolower($data[4]);
        $my_post['post_status'] = 'private';
        $my_post['post_type'] = 'drug_price';

        $new_post = wp_insert_post( $my_post );

        $taxonomy = 'drugpricecat';
        $termObj  = get_term_by( 'id', (int)$post_category, $taxonomy);
        wp_set_object_terms($new_post, (int)$post_category, $taxonomy);

        $wpdb->query("INSERT INTO wp_postmeta
                (post_id, meta_key, meta_value)
                VALUES
                ($new_post, 'cod_drug_table', '$data[0]'),
                ($new_post, '_cod_drug_table', 'field_60638b47cd678'),
                ($new_post, 'name_drug_table', '$data[1]'),
                ($new_post, '_name_drug_table', 'field_6063382bba893'),
                ($new_post, 'packaging_drug_table', '$data[2]'),
                ($new_post, '_packaging_drug_table', 'field_60648a9b7c319'),
                ($new_post, 'strength_drug_table', '$data[3]'),
                ($new_post, '_strength_drug_table', 'field_60633811ba892'),
                ($new_post, 'quantity_drug_table', '$data[4]'),
                ($new_post, '_quantity_drug_table', 'field_606336ddba891'),
                ($new_post, 'price_drug_table', '$data[5]'),
                ($new_post, '_price_drug_table', 'field_606336d5ba890')
            ");
    }
?>
<div class="wrap">
    <h1>Import drugs prices</h1>
    <div class="narrow">
        <p>This is my plugin's first page</p>
        <form enctype="multipart/form-data"  method="post" class="wp-upload-form">
            <p>
                <label for="upload">Choose a file from your computer:</label> (Maximum size: 2 MB)<input type="file"  name="import" size="25">
                <input type="hidden" name="action" value="save">
                <input type="hidden" name="max_file_size" value="2097152">
            </p>
            <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Upload file and import" disabled=""></p>
        </form>
    </div>
</div>