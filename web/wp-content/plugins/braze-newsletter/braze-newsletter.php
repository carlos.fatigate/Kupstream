<?php 
/**
 * @wordpress-plugin
 * Plugin Name:       Braze Newsletter
 * Description: This plugin is responsible for integrating Wordpress with the Braze.
 * Author: Carlos Fatigate
 */
require_once( plugin_dir_path( __FILE__ ) . '/includes/shortcode.php' );