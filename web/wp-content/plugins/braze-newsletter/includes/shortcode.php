<?php

if (isset($_POST["newsletter_email"]) && isset($_SERVER['HOME'])) { 
    $secrets_file = $_SERVER['HOME'] . '/files/private/secrets.json';
 
    if (file_exists($secrets_file)) {
        $secrets = json_decode(file_get_contents($secrets_file), 1);
    } else {
        die( 'Env dont found.' );
    }

    $url = isset($secrets["braze_url"]) ? $secrets["braze_url"] : die( 'Env dont found.' );
    $user = isset($secrets["braze_user"]) ? $secrets["braze_user"] : die( 'Env dont found.' );
    $pass = isset($secrets["braze_pass"]) ? $secrets["braze_pass"] : die( 'Env dont found.' );

    $campaign_newsletter = explode("/", $_POST["campaign_newsletter"])[0];

    $auth = base64_encode( $user . ':' . $pass );

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
            "email": "' . $_POST["newsletter_email"] . '",
            "campaign": "' . $campaign_newsletter . '"
        }',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Basic ' . $auth,
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
}

defined( 'ABSPATH' ) or die( 'Direct script access disallowed.' );

add_shortcode( 'braze-newsletter', function( $atts ) {
  $default_atts = array();
  $args = shortcode_atts( $default_atts, $atts );

  return '
            <input class="newsletter_input" name="newsletter_email" type="email" placeholder="Enter your email..."/>
            <input name="campaign_newsletter" type="hidden" value=' . get_field('campaign_newsletter') . '/>
        ';
});