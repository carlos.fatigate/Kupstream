<?php

class User_Clearscope_Meta_Ajax {
  private static $meta_key = 'clearscope_reports';
  public static $nonce_label = 'clearscope_user_settings';
	public function __construct() {
		add_action( 'wp_ajax_clearscope_add_recent_report', array( $this, 'add_recent_report' ) );
		add_action( 'wp_ajax_clearscope_get_recent_reports', array( $this, 'get_recent_reports' ) );
  }

  private function sanitized_report($report){
    $sanitized_report = array();
    if(array_key_exists('slug', $report)){
      $sanitized_report['slug'] = sanitize_text_field($report['slug']);
    }
    if(array_key_exists('query', $report)){
      $sanitized_report['query'] = sanitize_text_field($report['query']);
    }
    return $sanitized_report;
  }

  private function is_valid_report($report){
    if(array_key_exists('slug', $report) && array_key_exists('query', $report)){
      return true;
    } else {
      return false;
    }
  }

  public function add_recent_report() {
    check_ajax_referer( self::$nonce_label );
    $report = $this->sanitized_report($_POST['report']);
    if( !$this->is_valid_report($report) ){
      wp_send_json( array( 'error' => 'invalid report format' ));
      return;
    }
    $clearscope_reports = get_user_meta( get_current_user_id(), self::$meta_key, true );
    if ( !empty( $clearscope_reports ) ) {
      array_unshift($clearscope_reports, $report);
      $unique_reports = array();
      foreach ($clearscope_reports as $r)
      {
        if( !array_key_exists($r['slug'], $unique_reports) && !is_null($r) ){
          $unique_reports[$r['slug']] = $r;
        }
      }
      $clearscope_reports = array_values($unique_reports);
      $clearscope_reports = array_slice($clearscope_reports, 0, 5); 
      update_user_meta( get_current_user_id(), self::$meta_key, $clearscope_reports );
    } else {
      $clearscope_reports = array( $report );
      add_user_meta( get_current_user_id(), self::$meta_key, $clearscope_reports );
    }
    wp_send_json( array( 'reports' => $clearscope_reports ));
  }

  public function get_recent_reports() {
    check_ajax_referer( self::$nonce_label );
    $clearscope_reports = get_user_meta( get_current_user_id(), self::$meta_key, true );
    //$clearscope_reports = array_values(array_filter($clearscope_reports));
    // delete_user_meta( get_current_user_id(), self::$meta_key);
    wp_send_json( array(
      'reports' => $clearscope_reports,
    ));
  }
}