=== Clearscope ===
Contributors: mushilabs
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html
Tags: SEO, Content analysis, Readability
Requires at least: 5.0
Tested up to: 5.7
Requires PHP: 5.4
Stable tag: 0.2.13

Embed the Clearscope Optimize experience inside your WordPress Gutenberg or Classic editor sidebar.

== Description ==

Integrate Clearscope into your WordPress with the click of a button. Embed the Clearscope Optimize experience inside your WordPress Gutenberg or Classic editor sidebar. No more copy-pasting, our plugin pulls in Term recommendations, Content Grade, and Word Count so your report is updated in real-time as you write your post.

== Installation ==

Install this WordPress plugin and embed your shared Reports to access the Clearscope Optimize experience directly in your Posts and Pages.

Click the Install Now button. Be sure to Activate it afterwards.

**How to use the Clearscope plugin**

1. For a Clearscope report, enable the shared link. Copy it!
2. In a WordPress post, open the Clearscope dropdown in your right sidebar and paste the shared link inside.
3. Begin optimizing!

== Screenshots ==
1. Clearscope widget using the Classic editor.

== Changelog ==
= 0.1.0 =
Release Date: February 4th, 2020

Published beta version.

= 0.2.0 =
Release Date: March, 11th, 2020

Compatibility with Classic Editor plugin.
