<?php

class Clearscope_Plugin
{
  private $is_gutenberg;

  public function __construct()
  {
    add_action('current_screen', array($this, 'set_is_gutenberg'));
    add_action('admin_enqueue_scripts', array($this, 'enqueue_assets'));
    add_action('add_meta_boxes', array($this, 'add_meta_box'));
  }

  public function set_is_gutenberg()
  {
    if (!function_exists('get_current_screen')) {
      require_once(ABSPATH . 'wp-admin/includes/screen.php');
    }
    if (get_current_screen()->is_block_editor()) {
      $this->is_gutenberg = true;
    } else {
      $this->is_gutenberg = false;
    }
  }

  public function enqueue_assets($hook)
  {
    global $wp_version, $clearscope_plugin_version;
    if (!$this->is_loadable()) {
      return;
    }
    $dependencies = array(
      'wp-plugins',
      'wp-element',
      'wp-data',
      'wp-components',
      'wp-dom-ready'
    );
    // Only load wp-edit-post as dependency on Gutenberg to work around Yoast breaking with wp-edit-post enabled
    // wp-edit-post is only used for PluginSidebar JSX for Gutenberg
    if ($this->is_gutenberg) {
      array_push($dependencies, 'wp-edit-post');
    }
    wp_enqueue_script(
      'clearscope-wp-js',
      plugins_url('js/index-' . $clearscope_plugin_version . '.js', __FILE__),
      $dependencies
    );
    wp_localize_script('clearscope-wp-js', 'clearscope_globals',
      array(
        'user' => wp_get_current_user()->to_array(),
        'wp_version' => $wp_version,
        'php_version' => phpversion(),
        'plugin_version' => $clearscope_plugin_version,
        'latest_plugin_version' => $this->wp_latest_version(),
        'post' => get_post()->to_array(),
        'post_url' => get_edit_post_link(null, 'not display'),
        'ajax_url' => admin_url('admin-ajax.php'),
        'ajax_nonce' => wp_create_nonce(User_Clearscope_Meta_Ajax::$nonce_label),
        'is_gutenberg' => $this->is_gutenberg ? "true" : "false"
      )
    );
  }

  public function add_meta_box()
  {
    if (!$this->is_gutenberg && $this->is_loadable()) {
      add_meta_box(
        'clearscope-meta',
        'Clearscope',
        array($this, 'meta_box_html')
      );
    }
  }

  public function meta_box_html()
  {
    echo '<div id="clearscope-meta-content">';
    echo 'Loading...';
    echo '</div>';
  }

  private function is_loadable()
  {
    global $post;
    if($post){
      $post_type = get_post_type($post->ID);
      $public_post_types = get_post_types( array('public' => true ) );
      $loadable_post_types = array_keys(array_filter( $public_post_types, 'is_post_type_viewable' ));
      return in_array($post_type, $loadable_post_types);
    }
    return false;
  }

  private function wp_latest_version()
  {
    // https://wordpress.stackexchange.com/questions/123732/get-latest-plugin-version-from-wp-api
    if (!function_exists('plugins_api')) {
      require_once(ABSPATH . 'wp-admin/includes/plugin-install.php');
    }
    $plugin_information = plugins_api('plugin_information', array(
      'slug' => 'clearscope',
      'fields' => array('version' => true)
    ));
    if (!is_wp_error($plugin_information)) {
      return $plugin_information->version;
    }
  }
}