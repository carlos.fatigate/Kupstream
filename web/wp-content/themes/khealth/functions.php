<?php
add_theme_support( 'post-thumbnails' );

add_image_size( 'featured', 200, 900, false );
add_image_size( 'featured@2x', 400, 900, false );
add_image_size( 'testimonials', 125, 125, true );
add_image_size( 'testimonials@2x', 250, 250, true );
add_image_size( 'feature', 78, 78, false );
add_image_size( 'feature@2x', 156, 156, false );
add_image_size( 'cta', 1500, 400, false );
add_image_size( 'avatarf', 500, 500, true );
add_image_size( 'cta@2x', 3000, 800, false );
add_image_size( 'hero1', 1920, 400, true );

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

function pp_wp_scripts_method() {
	wp_register_script( 'vendor', get_template_directory_uri() . '/js/vendor.min.js', array( ), 1, true );
	wp_enqueue_script( 'vendor' );
}
function crunchify_stop_loading_wp_embed_and_jquery() {
	if (!is_admin()) {
		wp_deregister_script('wp-embed');
	}
}
add_action('init', 'crunchify_stop_loading_wp_embed_and_jquery');
function remove_menus() {
	remove_menu_page( 'edit-comments.php' ); 
}
add_action( 'admin_menu', 'remove_menus' );

add_action( 'wp_enqueue_scripts', 'pp_wp_scripts_method' );

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

function my_deregister_scripts(){
	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );
function itsme_disable_feed() {
	wp_die('No feed available, please visit the <a href="'. esc_url( home_url( '/' ) ) .'">homepage</a>!' );
}
remove_action('wp_head', 'wp_generator');

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'rest_output_link_wp_head' );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
remove_action( 'template_redirect', 'rest_output_link_header', 11 );

remove_action('wp_head', 'feed_links_extra', 3 );  
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
function disable_json_api () {
	add_filter('json_enabled', '__return_false');
	add_filter('json_jsonp_enabled', '__return_false');
	add_filter('rest_enabled', '__return_false');
	add_filter('rest_jsonp_enabled', '__return_false');
}
add_action( 'after_setup_theme', 'disable_json_api' );


function remove_comment_support() {
	remove_post_type_support( 'post', 'comments' );
	remove_post_type_support( 'page', 'comments' );
}

function mytheme_admin_bar_render() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
}
add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );



function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

include(get_template_directory() . '/blocks.php');


function smartwp_remove_wp_block_library_css(){
	wp_dequeue_style( 'wp-block-library' );
	wp_dequeue_style( 'wp-block-library-theme' );
}
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css' );


add_filter( 'wp_get_attachment_image_attributes', 'wpse8170_add_lazyload_to_attachment_image', 10, 2 );
function wpse8170_add_lazyload_to_attachment_image( $attr, $attachment ) {
	if (!isset($attr['no-src'])) {
		if (strpos($attr['src'], '.svg') !== false) {
		} else {
			$attr['data-src'] = $attr['src'];
			if (isset($attr['srcset'])) {
			$attr['data-srcset'] = $attr['srcset'];
			}
			unset($attr['src']);
			unset($attr['srcset']);
		}
	}
	return $attr;
}


add_action( 'init', 'slider_posts_init' );

function slider_posts_init() {

	$args = array(
		'labels' => array(
			'name' => ( 'Testimonials' ),
		),
		'public' => true,
		'publicly_queryable' => false,
		'show_in_rest' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-excerpt-view',
		'supports' => array( 'title', 'editor'),
		'has_archive' => false
	);
	register_post_type( 'testiomonials', $args );

	register_taxonomy(
		'testiomonialscat',
		array('testiomonials'),
		array(
			'label' => __( 'Category' ),
			'hierarchical' => true,
			'show_in_rest' => true,
			'publicly_queryable' => false,
			'show_admin_column' => true,
		)
	);

	$args = array(
		'labels' => array(
			'name' => ( 'Doctors' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'show_in_rest' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-businessperson',
		'supports' => array( 'title', 'editor', 'thumbnail'),
		'has_archive' => false
	);
	register_post_type( 'doctors', $args );

	register_taxonomy(
		'doccat',
		array('doctors'),
		array(
			'label' => __( 'Category' ),
			'hierarchical' => true,
			'show_in_rest' => true,
			'publicly_queryable' => false,
			'show_admin_column' => true,
		)
	);

	$args = array(
		'labels' => array(
			'name' => ( 'FAQ' ),
		),
		'public' => true,
		'publicly_queryable' => false,
		'show_in_rest' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-media-text',
		'supports' => array( 'title', 'editor'),
		'has_archive' => false
	);
	register_post_type( 'faq', $args );
	register_taxonomy(
		'faqcat',
		array('faq'),
		array(
			'label' => __( 'Category' ),
			'hierarchical' => true,
			'show_in_rest' => true,
			'publicly_queryable' => false,
			'show_admin_column' => true,
		)
	);
	$args = array(
		'labels' => array(
			'name' => ( 'Drug Template' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'show_in_rest' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-media-text',
		'supports' => array( 'title', 'editor'),
		'has_archive' => false
	);
	register_post_type( 'drug', $args );
	register_taxonomy(
		'drugcat',
		array('drug'),
		array(
			'label' => __( 'Category' ),
			'hierarchical' => true,
			'show_in_rest' => true,
			'publicly_queryable' => false,
			'show_admin_column' => true,
		)
	);
	$args = array(
		'labels' => array(
			'name' => ( 'Drug Price' ),
		),
		'public' => true,
		'publicly_queryable' => false,
		'show_in_rest' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-media-text',
		'supports' => array( 'title', 'editor'),
		'has_archive' => false
	);
	register_post_type( 'drug_price', $args );

	register_taxonomy(
		'drugpricecat',
		array('drug_price'),
		array(
			'label' => __( 'Category' ),
			'hierarchical' => false,
			'show_in_rest' => true,
			'publicly_queryable' => false,
			'show_admin_column' => true,
		)
	);

	register_taxonomy(
		'pagecat',
		array('page'),
		array(
			'label' => __( 'Category' ),
			'hierarchical' => true,
			'show_in_rest' => true,
			'publicly_queryable' => false,
			'show_admin_column' => true,
		)
	);
	
	$args = array(
		'labels' => array(
			'name' => ( 'Pricing Page' ),
		),
		'public' => true,
		'publicly_queryable' => true,
		'show_in_rest' => true,
		'query_var' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-clipboard',
		'supports' => array( 'title', 'editor'),
		'has_archive' => false
	);
	register_post_type( 'pricing_page', $args );
}

// function wpse28782_remove_menu_items() {
//     remove_menu_page( 'edit.php?post_type=drug_price' );
// }
// add_action( 'admin_menu', 'wpse28782_remove_menu_items' );

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_606335f766516',
		'title' => 'Drug Price',
		'fields' => array(
			array(
				'key' => 'field_60638b47cd678',
				'label' => 'Cod',
				'name' => 'cod_drug_table',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_6063382bba893',
				'label' => 'Name',
				'name' => 'name_drug_table',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_60648a9b7c319',
				'label' => 'Packaging',
				'name' => 'packaging_drug_table',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_60633811ba892',
				'label' => 'Strength',
				'name' => 'strength_drug_table',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_606336ddba891',
				'label' => 'Quantity',
				'name' => 'quantity_drug_table',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_606336d5ba890',
				'label' => 'Price',
				'name' => 'price_drug_table',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'drug_price',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => array(
			0 => 'permalink',
			1 => 'the_content',
			2 => 'excerpt',
			3 => 'discussion',
			4 => 'comments',
			5 => 'revisions',
			6 => 'slug',
			7 => 'author',
			8 => 'format',
			9 => 'page_attributes',
			10 => 'featured_image',
			11 => 'categories',
			12 => 'tags',
			13 => 'send-trackbacks',
		),
		'active' => true,
		'description' => '',
	));
	
	endif;

function pp_wp_register_my_menus() {

	register_nav_menus(
		array(
			'mainnavigation' => 'Main meniu',
			'footnavigation' => 'Footer meniu',
		)
	);
}

add_action( 'init', 'pp_wp_register_my_menus' );

function wp_pp_sidebar_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar page', "theme-admin" ),
		'id' => 'side1',
		'before_widget' => '<div class="side-widget entry %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Sidebar blog page', "theme-admin" ),
		'id' => 'side2',
		'before_widget' => '<div class="side-widget-2 %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Col #1', "theme-admin" ),
		'id' => 'footer-1',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Col #2', "theme-admin" ),
		'id' => 'footer-2',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Col #3', "theme-admin" ),
		'id' => 'footer-3',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Col #4', "theme-admin" ),
		'id' => 'footer-4',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Col #5', "theme-admin" ),
		'id' => 'footer-5',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Col #6', "theme-admin" ),
		'id' => 'footer-6',
		'before_widget' => '<div class="widget %2$s">',
		'after_widget' => "</div>",
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );
}

add_action( 'widgets_init', 'wp_pp_sidebar_widgets_init' );
add_shortcode( 'search', 'shortcode_search' );
function shortcode_search( $atts ) {
    return '<form role="search" method="get" action="/"><label class="screen-reader-text" for="s"></label><input type="search" class="search-field" value="" name="s" id="s" placeholder="How can we help?"><button type="submit" id="searchsubmit" class="search-submit">Go</button></form>';
}

add_shortcode( 'logo', 'shortcode_logo' );
function shortcode_logo( $atts ) {
    return '<span class="logo"><svg class="icon mobile-only"><use xlink:href="#logo"></use></svg><svg class="icon desktop-only"><use xlink:href="#logo-s"></use></svg></span>';
}
add_shortcode( 'social', 'shortcode_social' );
function shortcode_social( $atts ) {
	ob_start();
	echo '<ul class="social">';
	$social = get_field("social", "option");
	if ($social) {
		foreach ($social as $key => $soc) {
			if ($soc) {
				echo '<li><a href="'.$soc.'" target="_blank">';
				switch ($key) {
					case 'twitter':
					echo '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32.004 32"><path d="M31.118 3.48a13.188 13.188 0 01-4.17 1.594A6.55 6.55 0 0022.156 3a6.566 6.566 0 00-6.396 8.06A18.64 18.64 0 012.228 4.2a6.516 6.516 0 00-.888 3.3 6.563 6.563 0 002.92 5.464 6.567 6.567 0 01-2.974-.824v.082a6.572 6.572 0 005.266 6.438 6.566 6.566 0 01-2.966.112 6.574 6.574 0 006.132 4.56 13.176 13.176 0 01-8.154 2.81c-.53 0-1.052-.032-1.566-.092A18.59 18.59 0 0010.062 29c12.076 0 18.68-10.004 18.68-18.68 0-.284-.006-.568-.02-.85a13.232 13.232 0 003.28-3.394 13.11 13.11 0 01-3.77 1.034 6.585 6.585 0 002.886-3.63z"/></svg>';
					break;
					case 'facebook':
					echo '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M12.054 32h5.996l.004-16 5.446-.002.584-5.514H18.05l.008-2.76c0-1.438.098-2.208 2.162-2.208h3.812V0h-5.466c-5.298 0-6.518 2.738-6.518 7.236l.006 3.248-4.022.002V16h4.022v16z"/></svg>';
					break;
					case 'instagram':
					echo '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M30.556 0H1.444C.648 0 0 .646 0 1.442v29.116C0 31.352.648 32 1.444 32h29.112c.796 0 1.444-.648 1.444-1.442V1.442C32 .646 31.352 0 30.556 0zm-8.972 16a5.584 5.584 0 11-11.168 0 5.584 5.584 0 0111.168 0zM28 28H4V14h2.634a9.56 9.56 0 00-.218 2c0 5.284 4.3 9.584 9.584 9.584s9.584-4.3 9.584-9.584a9.56 9.56 0 00-.218-2H28v14zm0-18h-6V4h6v6z"/></svg>';
					break;
					case 'youtube':
					echo '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-youtube" viewBox="0 0 16 16"><path d="M8.051 1.999h.089c.822.003 4.987.033 6.11.335a2.01 2.01 0 0 1 1.415 1.42c.101.38.172.883.22 1.402l.01.104.022.26.008.104c.065.914.073 1.77.074 1.957v.075c-.001.194-.01 1.108-.082 2.06l-.008.105-.009.104c-.05.572-.124 1.14-.235 1.558a2.007 2.007 0 0 1-1.415 1.42c-1.16.312-5.569.334-6.18.335h-.142c-.309 0-1.587-.006-2.927-.052l-.17-.006-.087-.004-.171-.007-.171-.007c-1.11-.049-2.167-.128-2.654-.26a2.007 2.007 0 0 1-1.415-1.419c-.111-.417-.185-.986-.235-1.558L.09 9.82l-.008-.104A31.4 31.4 0 0 1 0 7.68v-.122C.002 7.343.01 6.6.064 5.78l.007-.103.003-.052.008-.104.022-.26.01-.104c.048-.519.119-1.023.22-1.402a2.007 2.007 0 0 1 1.415-1.42c.487-.13 1.544-.21 2.654-.26l.17-.007.172-.006.086-.003.171-.007A99.788 99.788 0 0 1 7.858 2h.193zM6.4 5.209v4.818l4.157-2.408L6.4 5.209z"/></svg>';
					break;
					case 'linkedin':
					echo '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M2.064 10H8v22H2.064zm18.11.002c-2.174 0-3.258 1.036-4.174 1.738V10h-5.968v22H16V18.15s-.218-2.758 3.088-2.758c1.992 0 2.912 1.134 2.912 2.758V32h6.288V18.344c0-7.5-6.038-8.342-8.114-8.342zM2 5a3 3 1080 106 0 3 3 1080 10-6 0z"/></svg>';
					break;
					default:
					break;
				}
				echo "</a></li>";
			}
		}
	}
	echo '</ul>';
	return ob_get_clean();
}

function wpshock_search_filter( $query ) {
	if (!is_admin()) {
		if ( $query->is_search ) {
			$query->set( 'post_type', array('post') );
		}
	}
    return $query;
}
add_filter('pre_get_posts','wpshock_search_filter');

function smartwp_disable_new_user_notifications() {
	//Remove original use created emails
	remove_action( 'register_new_user', 'wp_send_new_user_notifications' );
	remove_action( 'edit_user_created_user', 'wp_send_new_user_notifications', 10, 2 );
	
	//Add new function to take over email creation
	add_action( 'register_new_user', 'smartwp_send_new_user_notifications' );
	add_action( 'edit_user_created_user', 'smartwp_send_new_user_notifications', 10, 2 );
}

function smartwp_send_new_user_notifications( $user_id, $notify = 'user' ) {
	  return;
}
add_action( 'init', 'smartwp_disable_new_user_notifications' );

add_action( 'init', array ( 'T5_Rewrite_Tag_Tag', 'init' ) );

/**
 * Adds '%tag%' as rewrite tag (placeholder) for permalinks.
 */
class T5_Rewrite_Tag_Tag
{
    /**
     * Add tag and register 'post_link' filter.
     *
     * @wp-hook init
     * @return  void
     */
    public static function init()
    {
        add_rewrite_tag( '%tag%', '([^/]+)' );
        add_filter( 'post_link', array( __CLASS__, 'filter_post_link' ) , 10, 2 );
    }

    /**
     * Parse post link and replace the placeholder.
     *
     * @wp-hook post_link
     * @param   string $link
     * @param   object $post
     * @return  string
     */
    public static function filter_post_link( $link, $post )
    {
        static $cache = array (); // Don't repeat yourself.

        if ( isset ( $cache[ $post->ID ] ) )
            return $cache[ $post->ID ];

        if ( FALSE === strpos( $link, '%tag%' ) )
        {
            $cache[ $post->ID ] = $link;
            return $link;
        }

        $tags = get_the_tags( $post->ID );

        if ( ! $tags )
        {
            $cache[ $post->ID ] = str_replace( '%tag%', 'tag', $link );
            return $cache[ $post->ID ];
        }

        $first              = current( (array) $tags );

        $cache[ $post->ID ] = str_replace( '%tag%', $first->slug, $link );

        return $cache[ $post->ID ];
    }
}

// Primary Category only
function get_post_primary_category($post_id, $term='category', $return_all_categories=false){
        $return = array();

        if (class_exists('WPSEO_Primary_Term')){
            // Show Primary category by Yoast if it is enabled & set
            $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
            $primary_term = get_term($wpseo_primary_term->get_primary_term());

            if (!is_wp_error($primary_term)){
                $return['primary_category'] = $primary_term;
            }
        }

        if (empty($return['primary_category']) || $return_all_categories){
            $categories_list = get_the_terms($post_id, $term);

            if (empty($return['primary_category']) && !empty($categories_list)){
                $return['primary_category'] = $categories_list[0];  //get the first category
            }
            if ($return_all_categories){
                $return['all_categories'] = array();

                if (!empty($categories_list)){
                    foreach($categories_list as &$category){
                        $return['all_categories'][] = $category->term_id;
                    }
                }
            }
        }

        return $return;
    }   
//Primary Catregory only

// function wpd_fix_author_structure(){
//     global $wp_rewrite;
//     $wp_rewrite->author_structure = 'doctor/%author%';
// }
// add_action( 'init', 'wpd_fix_author_structure' );

add_action( 'wp_enqueue_scripts', 'secure_enqueue_script' );
function secure_enqueue_script() {
  wp_register_script( 'secure-ajax-access', esc_url( add_query_arg( array( 'js_global' => 1 ), site_url() ) ) );
  wp_enqueue_script( 'secure-ajax-access' );
}

add_action( 'wp_enqueue_scripts', 'secure_enqueue_script_calculator' );
function secure_enqueue_script_calculator() {
  wp_register_script( 'secure-ajax-access', esc_url( add_query_arg( array( 'js_global_calculator' => 1 ), site_url() ) ) );
  wp_enqueue_script( 'secure-ajax-access' );
}

add_action( 'template_redirect', 'javascript_variables' );
function javascript_variables() {
  if ( !isset( $_GET[ 'js_global' ] ) ) return;

  $nonce = wp_create_nonce('get_drug_price_nonce');
  $nonce_select_option = wp_create_nonce('get_select_option_calculator_nonce');

  $variaveis_javascript = array(
    'get_drug_price_nonce' => $nonce,
	'get_select_option_calculator_nonce' => $nonce_select_option,
    'xhr_url'             => admin_url('admin-ajax.php') 
  );

  $new_array = array();
  foreach( $variaveis_javascript as $var => $value ) $new_array[] = esc_js( $var ) . " : '" . esc_js( $value ) . "'";

  header("Content-type: application/x-javascript");
  printf('var %s = {%s};', 'js_global', implode( ',', $new_array ) );
  exit;
}

add_action('wp_ajax_nopriv_get_drug_price', 'get_drug_price');
add_action('wp_ajax_get_drug_price', 'get_drug_price');

function get_drug_price() {
  if( ! wp_verify_nonce( $_POST['get_drug_price_nonce'], 'get_drug_price_nonce' ) ) {
    echo '401'; 
    die();
  }

  $args = array(
    'name' => strtolower($_POST['related_term_name']).'-'.strtolower($_POST['packaging']).'-'.strtolower($_POST['dose']).'-'.strtolower($_POST['quantity']), 
	'post_type' => 'drug_price',
	'post_status' => 'private',
	'posts_per_page' => 1
  );

  $wp_query = new WP_Query( $args );

  if( $wp_query->have_posts() ) {
	  $drugs = $wp_query->posts;

	  $drug = array(
		'packaging' => get_field('packaging_drug_table', $drugs[0]->ID),
		'dose' => get_field('strength_drug_table', $drugs[0]->ID),
		'quantity' => get_field('quantity_drug_table', $drugs[0]->ID),
		'price' => str_replace('$','',get_field('price_drug_table', $drugs[0]->ID)),
	  );
    echo json_encode( $drug );
  } else {
    echo 402;
  }

  exit;
}

add_action('wp_ajax_nopriv_get_select_option_calculator', 'get_select_option_calculator');
add_action('wp_ajax_get_select_option_calculator', 'get_select_option_calculator');

function get_select_option_calculator() {
	if( ! wp_verify_nonce( $_POST['get_select_option_calculator_nonce'], 'get_select_option_calculator_nonce' ) ) {
		echo '401'; 
		die();
	}

	$doses = array();
    $quantities = array();
    $packaging_array = array();
    $price_drug_array = array();

  	$posts_array = get_posts(
		array(
			'posts_per_page' => -1,
			'post_type' => 'drug_price',
			'post_status' => 'private',
			'tax_query' => array(
				array(
					'taxonomy' => 'drugpricecat',
					'field' => 'name',
					'terms' => $_POST['related_term_name'],
					'include_children' => false
				)
			)
		)
	);

	foreach ($posts_array as $post) { 
		$dose = get_field('strength_drug_table', $post->ID);
		$quantity = get_field('quantity_drug_table', $post->ID);
		$packaging = get_field('packaging_drug_table', $post->ID);
		$price_drug = get_field('price_drug_table', $post->ID);

		if (!in_array($packaging, $packaging_array)) { 
			array_push($packaging_array, $packaging);
		}

		$dose_array = explode(' ',$dose);

		if (!in_array($dose_array[0], $doses)) { 
			array_push($doses, $dose_array[0]);
		}

		if (!in_array($quantity, $quantities)) { 
			array_push($quantities, $quantity);
		}
		array_push($price_drug_array, (int)explode('$',$price_drug)[1]);
	}

	$min_price_key = array_keys($price_drug_array, min($price_drug_array))[0];
	sort($doses);
	foreach ($doses as $key => $value) {
		$doses[$key] = $value.' '.$dose_array[1];
	}
	sort($quantities);

	$packaging_get = isset($_GET['packaging']) ? $_GET['packaging'] : get_field('packaging_drug_table', $posts_array[$min_price_key]->ID);
    $dose_get = isset($_GET['dose']) ? $_GET['dose'] : get_field('strength_drug_table', $posts_array[$min_price_key]->ID);
    $quantity_get = isset($_GET['quantity']) ? $_GET['quantity'] : get_field('quantity_drug_table', $posts_array[$min_price_key]->ID);
    $related_term_name = $_POST['related_term_name'];

	$respose['doses'] = $doses;
	$respose['quantities'] = $quantities;
	$respose['packaging_array'] = $packaging_array;
	$respose['price_drug_array'] = $price_drug_array;
	$respose['packaging_get'] = $packaging_get;
	$respose['quantity_get'] = $quantity_get;
	$respose['dose_get'] = $dose_get;

	echo json_encode( $respose );
	exit;
}

function wpdocs_custom_password_form() {
    global $post;
 
    $loginurl = site_url() . '/wp-login.php?action=postpass';
    $label = 'pwbox-' . ( ! empty( $post->ID ) ? $post->ID : rand() );
 
    ob_start();
    ?>
    <div class="container">            
        <form action="<?php echo esc_attr( $loginurl ) ?>" method="post" class="center-custom search-form password-form" role="search">
            <label class="post-password-label">
				<?php esc_attr_e( "This content is password protected. To view it please enter your password below:" ) ?>
			</label>
            <label for="<?php echo esc_attr( $label ) ?>" class="post-password-label">
				<?php _e( "Password:" ) ?>
				<input name="post_password" id="<?php echo esc_attr( $label ) ?>" class="input post-password-class" type="text" />
			</label>
            <input type="submit" name="Submit" class="btn btn-primary submit-btn" value="<?php esc_attr_e( "Enter" ) ?>" />            
        </form>
    </div>
 
    <?php
    return ob_get_clean();
}   
add_filter( 'the_password_form', 'wpdocs_custom_password_form', 9999 );

function auto_id_headings( $content ) {

	$content = preg_replace_callback( '/(\<h[1-6](.*?))\>(.*)(<\/h[1-6]>)/i', function( $matches ) {
		if ( ! stripos( $matches[0], 'id=' ) ) :
			$matches[0] = $matches[1] . $matches[2] . ' id="h-' . sanitize_title( $matches[3] ) . '">' . $matches[3] . $matches[4];
		endif;
		return $matches[0];
	}, $content );

    return $content;

}
add_filter( 'the_content', 'auto_id_headings' );

add_action('pre_get_posts', 'remove_my_cpt_from_search_results');

function remove_my_cpt_from_search_results($query) {
    if (is_admin() || !$query->is_main_query() || !$query->is_search()) {
        return $query;
    }

    // can exclude multiple post types, for ex. array('staticcontent', 'cpt2', 'cpt3')
    $post_types_to_exclude = array('doctors');

    if ($query->get('post_type')) {
        $query_post_types = $query->get('post_type');

        if (is_string($query_post_types)) {
            $query_post_types = explode(',', $query_post_types);
        }
    } else {
        $query_post_types = get_post_types(array('exclude_from_search' => false));
    }

    if (sizeof(array_intersect($query_post_types, $post_types_to_exclude))) {
        $query->set('post_type', array_diff($query_post_types, $post_types_to_exclude));
    }

    return $query;
}

//khealth theme files
function khealth_files () {
	wp_enqueue_script('main-khealth-js', get_theme_file_uri('/dist/bundle.js'), array(), '1.0', true);

   // allow for reference of website url despite environment (ex. WP API can be called using "queryData.root_url + '/wp-json/wp/v2/posts)"
	wp_localize_script('main-khealth-js', 'queryData', array(
		'root_url' => get_site_url()
	));
}
add_action('wp_enqueue_scripts', 'khealth_files');