<?php
/**
 * Template Name: Policy Page
 * 
 * pixelperfect lt: Sample Page Template
 * 
 * @package WordPress
 * @subpackage pixelperfectlt
 */
get_header();
if ( have_posts() ):
	while ( have_posts() ) : the_post(); ?>
		<div class="page-hero v2">
			<div class="container">
				<div class="logo">
					<?php the_post_thumbnail( "full" ); ?> 
				</div>
				<div class="set">
					<div class="logo-f">
						<svg class="icon"><use xlink:href="#logo"></use></svg>
					</div>
					<h1><?php the_title() ?></h1>
				</div>
			</div>
		</div>
		<div class="fast-links">
		  <div class="container">
			<ul>
				<?php if( have_rows('boxes') ):
					while( have_rows('boxes') ): the_row(); ?>
			  			<li>
							<a href="#<?php echo sanitize_title(get_sub_field('title')); ?>">
								<?php the_sub_field('title'); ?>
							</a>
						</li>
  					<?php endwhile;
				endif; ?>
			</ul>
		  </div>
		</div>
		<div class="page-full">
			<div class="entry">
				<?php if( have_rows('boxes') ):
					while( have_rows('boxes') ): the_row(); ?>
						<div class="box-group" id="<?php echo sanitize_title(get_sub_field('title')); ?>">
							<div class="container">
								<h2><?php the_sub_field('title'); ?></h2>
								<?php the_sub_field("entry"); ?>
							</div>
						</div>
					<?php endwhile;
				endif; ?>
			</div>
		</div>
		</div>
		</div>
	<?php endwhile;
endif; 
get_footer(); ?>
