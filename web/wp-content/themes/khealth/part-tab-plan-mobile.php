<?php
	$first_tab_mobile_title = get_field("first_tab_mobile_title"); 
	$second_tab_mobile_title = get_field("second_tab_mobile_title"); 
	$second_table_tab_plan = get_field("second_table_tab_plan");
	$table_tab_plan = get_field("table_tab_plan");
    $first_row = $table_tab_plan[0];
	$second_tab = $second_table_tab_plan[0];
	$title_tab_plan = get_field("title_tab_plan"); 
?>

<?php if($title_tab_plan){ ?> 
	<div class="tab-plan-title-block">
		<div class="container">
			<h1 class="tab-plan-title"><?= $title_tab_plan ?></h1>
			<div class="tab-plan-subtitle"><?= get_field("description_tab_plan"); ?></div>
		</div>
	</div>
<?php } ?>
<div class="tab-plan-mobile">
	<?php if($first_tab_title) { ?>
		<button class="plan-tab-links" id="defaultPlanTabMobile" onclick="openPlanTabMobile(event, 'primary-care-mobile')">
			<p class="plan-tab-links-title"><?= $first_tab_mobile_title; ?></p>
		</button>
	<?php } ?>
	<?php if($second_tab_mobile_title) { ?>
		<button class="plan-tab-links" <?= !$first_tab_title ? 'id="defaultPlanTabMobile"' : null; ?> onclick="openPlanTabMobile(event, 'specialized-care-mobile')">
			<p class="plan-tab-links-title"><?= $second_tab_mobile_title; ?></p>
		</button>
	<?php } ?>
</div>
<div class="container">
	<?php if($first_tab_title) { ?>
		<div id="primary-care-mobile" class="plan-tab-content">
			<div class="specialized-care-subtitle">
				<?= $first_row['title_1st_mobile_title']; ?>
			</div>
			<div class="primary-care-mobile-box">
				<?php if($first_row["most_popular_flag"] == 2) { ?>
					<div class="plan-tab-most-popular">
						<p>Most popular</p>
						<div id="right-triangle"></div>
					</div>
				<?php } ?>
				<div class="plan-tab-content-box" >
					<p class="plan-tab-content-title"><?= $first_row["title_2nd_row"] ?></p>
					<div class="plan-tab-content-description"><?= $first_row["price_2nd_row"] ?></div>
					<a class="btn btn-secondary" href="<?= $first_row["button_link_2nd_row"] ?>"><?= $first_row["button_text_2nd_row"] ?></a>
				</div>
				<?php if($first_row['mobile_rows_second']) { ?>
					<div class="plan-tab-content-itens">
						<?php foreach ($first_row['mobile_rows_second'] as $key => $row) { ?>
							<p class="<?= $key >= 4 ? 'hide-rows-mobile': null; ?>" ><?= $row['text']; ?></p>
						<?php } ?>
					</div>
					<?php if($first_row['mobile_rows_second'] && sizeof($first_row['mobile_rows_second']) > 4) { ?>
						<a onclick="hideRowsMobile()" id="show-rows-btn-mobile" class="show-rows-btn">
							<div class="show-more-tab-plan">
								Show more <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path fill-rule="evenodd" clip-rule="evenodd" d="M0.851493 0.60186C0.493513 0.95984 0.493513 1.54024 0.851493 1.89822L6.35149 7.39822C6.70947 7.7562 7.28988 7.7562 7.64786 7.39822L13.1479 1.89822C13.5058 1.54024 13.5058 0.95984 13.1479 0.601859C12.7899 0.243878 12.2095 0.243878 11.8515 0.601859L6.99968 5.45368L2.14786 0.601859C1.78988 0.243879 1.20947 0.243879 0.851493 0.60186Z" fill="#2B8FFF"/>
										</svg>
							</div>
							<div class="show-less-tab-plan">
								Show less <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path fill-rule="evenodd" clip-rule="evenodd" d="M0.851493 7.39814C0.493513 7.04016 0.493513 6.45976 0.851493 6.10178L6.35149 0.601777C6.70947 0.243796 7.28988 0.243796 7.64786 0.601777L13.1479 6.10178C13.5058 6.45976 13.5058 7.04016 13.1479 7.39814C12.7899 7.75612 12.2095 7.75612 11.8515 7.39814L6.99968 2.54632L2.14786 7.39814C1.78988 7.75612 1.20947 7.75612 0.851493 7.39814Z" fill="#2B8FFF"/>
										</svg>
							</div>
						</a>
					<?php } ?>
				<?php } ?>
			</div>
			<div class="primary-care-mobile-box">
				<?php if($first_row["most_popular_flag"] == 1) { ?>
					<div class="plan-tab-most-popular">
						<p>Most popular</p>
						<div id="right-triangle"></div>
					</div>
				<?php } ?>
				<div class="plan-tab-content-box" >
					<p class="plan-tab-content-title"><?= $first_row["title_1st_row"] ?></p>
					<div class="plan-tab-content-description"><?= $first_row["price_1st_row"] ?></div>
					<a class="btn btn-secondary" href="<?= $first_row["button_link_1st_row"] ?>"><?= $first_row["button_text_1st_row"] ?></a>
				</div>
				<?php if($first_row['mobile_rows_first']) { ?>
					<div class="plan-tab-content-itens">
						<?php foreach ($first_row['mobile_rows_first'] as $key => $row) { ?>
							<p class="<?= $key >= 4 ? 'hide-rows-sec-mobile': null; ?>" ><?= $row['text']; ?></p>
						<?php } ?>
						<?php if($first_row['mobile_rows_first'] && sizeof($first_row['mobile_rows_first']) > 4) { ?>
							<a onclick="hideRowsSecMobile()" id="show-rows-btn-sec-mobile" class="show-rows-btn">
								<div class="show-more-tab-plan">
									Show more <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill-rule="evenodd" clip-rule="evenodd" d="M0.851493 0.60186C0.493513 0.95984 0.493513 1.54024 0.851493 1.89822L6.35149 7.39822C6.70947 7.7562 7.28988 7.7562 7.64786 7.39822L13.1479 1.89822C13.5058 1.54024 13.5058 0.95984 13.1479 0.601859C12.7899 0.243878 12.2095 0.243878 11.8515 0.601859L6.99968 5.45368L2.14786 0.601859C1.78988 0.243879 1.20947 0.243879 0.851493 0.60186Z" fill="#2B8FFF"/>
											</svg>
								</div>
								<div class="show-less-tab-plan">
									Show less <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path fill-rule="evenodd" clip-rule="evenodd" d="M0.851493 7.39814C0.493513 7.04016 0.493513 6.45976 0.851493 6.10178L6.35149 0.601777C6.70947 0.243796 7.28988 0.243796 7.64786 0.601777L13.1479 6.10178C13.5058 6.45976 13.5058 7.04016 13.1479 7.39814C12.7899 7.75612 12.2095 7.75612 11.8515 7.39814L6.99968 2.54632L2.14786 7.39814C1.78988 7.75612 1.20947 7.75612 0.851493 7.39814Z" fill="#2B8FFF"/>
											</svg>
								</div>
							</a>
						<?php } ?>
					</div>
				<?php } ?>
			</div>
		</div>
	<?php } ?>
	
	<?php if($second_tab_mobile_title) { ?>
		<div id="specialized-care-mobile" class="plan-tab-content">
			<h2 class="specialized-care-title">
				<?= $second_tab['section_title_mobile']; ?>
			</h2>
			<div class="specialized-care-subtitle">
				<?= $second_tab['section_title']; ?>
			</div>
			<div class="plan-tab-specialized-box">
				<div class="plan-tab-specialized">
					<div class="plan-tab-content-title specialized-box-title first-color">
						<?= $second_tab["title_1st_row"] ?>
					</div>
					<div class="plan-tab-content-title">
						<div class="plan-tab-content-description">
							<?= $second_tab["price_1st_row"] ?>
						</div>
						<a class="btn btn-secondary" href="<?= $second_tab["button_link_1st_row"] ?>">
							<?= $second_tab["button_text_1st_row"] ?>
						</a>
					</div>
				</div>
				<div class="plan-tab-specialized">
					<div class="plan-tab-content-title specialized-box-title second-color">
						<?= $second_tab["title_2nd_row"] ?>
					</div>
					<div class="plan-tab-content-title">
						<div class="plan-tab-content-description">
							<?= $second_tab["price_2nd_row"] ?>
						</div>
						<a class="btn btn-secondary" href="<?= $second_tab["button_link_2nd_row"] ?>">
							<?= $second_tab["button_text_2nd_row"] ?>
						</a>
					</div>
				</div>
			</div>
			<h2 class="contains_in_the_plan_title"><?= $second_tab['contains_in_the_plan_title']; ?></h2>
			<?php if($second_tab['rows']) { ?>
				<?php foreach ($second_tab['rows'] as $key => $value) { ?>
					<div class="contains_in_the_plan_box">
						<div style="width:20px">
							<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
								<circle cx="10" cy="10" r="10" fill="#2B8FFF"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M16.1823 6.26895C16.5861 6.64578 16.6079 7.27857 16.2311 7.68232L9.23106 15.1823C9.04601 15.3806 8.78839 15.4952 8.51723 15.4999C8.24608 15.5045 7.98466 15.3989 7.79289 15.2071L4.29289 11.7071C3.90237 11.3166 3.90237 10.6834 4.29289 10.2929C4.68342 9.90237 5.31658 9.90237 5.70711 10.2929L8.4752 13.061L14.7689 6.31769C15.1458 5.91393 15.7786 5.89211 16.1823 6.26895Z" fill="white"/>
							</svg>
						</div>
						<p><?= $value['title']; ?></p>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	<?php } ?>
</div>
<script>
    function openPlanTabMobile(evt, planTabName) {
        var i, planTabContent, planTabLinks;

        planTabContent = document.getElementsByClassName("plan-tab-content");
        for (i = 0; i < planTabContent.length; i++) {
            planTabContent[i].style.display = "none";
        }

        planTabLinks = document.getElementsByClassName("plan-tab-links");
        for (i = 0; i < planTabLinks.length; i++) {
            planTabLinks[i].className = planTabLinks[i].className.replace(" active", "");
        }

        document.getElementById(planTabName).style.display = "block";
        evt.currentTarget.className += " active";
    }

	function hideRowsMobile() {
        document.getElementById('show-rows-btn-mobile').classList.toggle("active")
        let rows = document.querySelectorAll('.hide-rows-mobile')
        rows.forEach(element => {
            element.classList.toggle("hide")
        })
    }

	function hideRowsSecMobile() {
        document.getElementById('show-rows-btn-sec-mobile').classList.toggle("active")
        let rows = document.querySelectorAll('.hide-rows-sec-mobile')
        rows.forEach(element => {
            element.classList.toggle("hide")
        })
    }

	document.addEventListener("DOMContentLoaded", function(event) {
		hideRowsMobile()
		hideRowsSecMobile()
	});
	window.addEventListener("load", function(event) {
		var widthM = window.innerWidth;
		if (widthM < 981 ) {
			document.getElementById("defaultPlanTabMobile").click();
		}
	});

	var widthMobile = false;
	var widthDesktop = false;

	window.addEventListener('resize', function () {
		var widthM = window.innerWidth;

		if (widthM < 981 ) {
			if(!widthMobile) {
				document.getElementById("defaultPlanTabMobile").click();
				widthMobile = true
				widthDesktop = false
			}
		} else {
			if(!widthDesktop) {
				document.getElementById("defaultPlanTab").click();
				widthMobile = false
				widthDesktop = true
			}
		}
	});
</script>