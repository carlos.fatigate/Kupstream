<?php
/**
 * Template Name: Full Page
 * 
 * pixelperfect lt: Sample Page Template
 * 
 * @package WordPress
 * @subpackage pixelperfectlt
 */
get_header();
if ( have_posts() ):
	while ( have_posts() ) : the_post();
		?>
		<div class="page-hero bg-gradient">
			<div class="container flex-center">
				<div class="logo">
					<svg class="icon"><use xlink:href="#logo-s"></use></svg>
				</div>
				<h1><?php the_title() ?></h1>
			</div>
		</div>
		<div class="page-full">
			<div class="container">
				<div class="entry">
					<?php the_content();  ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php
endwhile;
endif; 
get_footer(); ?>
