<div class="btns">
  <?php $inde =0; if( have_rows('buttons') ): while( have_rows('buttons') ): the_row(); $inde++; ?>
    <?php $arrow_up = get_sub_field('arrow_up'); ?>
    <?php $link = get_sub_field('button'); 
    if( $link ): ?>
      <a href="<?php echo $link['url']; ?>" class="<?php 
        if (isset($secondary)) {
          echo "btn btn-secondary";
        } else {
          echo ($inde ==2 && isset($secondLink)) ? "link" : "btn btn-primary";
        } 
        if($arrow_up) { echo " arrow_up"; }?>" 
        <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?> 
        <?php if(isset($style)) { echo $style; }; ?> 
        onMouseOver= <?php if(isset($cta_background_color_hover)) { echo $mouse_over; } else { echo ""; }; ?>
        onMouseOut= <?php if(isset($cta_background_color_hover)) { echo $mouse_out; } else { echo ""; }; ?>
       >
        <?php echo $link['title']; ?><?php if($arrow_up) { ?>
          <svg width="15" height="14" viewBox="0 0 15 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M8.08893 
            0.57733C7.76349 0.251893 7.23586 0.251893 6.91042 0.57733L1.07709 6.41066C0.751649 6.7361 
            0.751649 7.26374 1.07709 7.58917C1.40252 7.91461 1.93016 7.91461 2.2556 7.58917L6.66634 
            3.17843V12.8333C6.66634 13.2935 7.03944 13.6666 7.49967 13.6666C7.95991 13.6666 8.33301 
            13.2935 8.33301 12.8333V3.17843L12.7438 7.58917C13.0692 7.91461 13.5968 7.91461 13.9223 
            7.58917C14.2477 7.26374 14.2477 6.7361 13.9223 6.41066L8.08893 0.57733Z" fill="<?= $svg_color ?>"/>
          </svg>
        <?php } ?>
      </a>
    <?php endif; ?>
  <?php endwhile; endif; ?>
</div>