<?php 
    $first_tab_title = get_field("first_tab_title"); 
    $second_tab_title = get_field("second_tab_title"); 
    $title_tab_plan = get_field("title_tab_plan"); 
    $table_tab_plan = get_field("table_tab_plan");
    $first_row = $table_tab_plan[0];
    $second_table_tab_plan = get_field("second_table_tab_plan");
    $second_tab = $second_table_tab_plan[0];
?>
<?php if($title_tab_plan){ ?> 
	<div class="tab-plan-title-block">
		<div class="container">
			<h1 class="tab-plan-title"><?= $title_tab_plan ?></h1>
			<div class="tab-plan-subtitle"><?= get_field("description_tab_plan"); ?></div>
		</div>
	</div>
<?php } ?>
<div class="container">
	<div class="tab-plan <?php if($title_tab_plan) { echo 'margin-top-minus'; }?>">
		<?php if($first_tab_title) { ?>
			<button class="plan-tab-links" id="defaultPlanTab" onclick="openPlanTab(event, 'primary-care')">
				<p class="plan-tab-links-title"><?= $first_tab_title; ?></p>
				<p class="plan-tab-links-description"><?= get_field("first_tab_subtitle"); ?></p>
			</button>
		<?php } ?>
		<?php if($second_tab_title) { ?>
			<button class="plan-tab-links" <?= !$first_tab_title ? 'id="defaultPlanTab"' : null; ?> onclick="openPlanTab(event, 'specialized-care')">
				<p class="plan-tab-links-title"><?= $second_tab_title; ?></p>
				<p class="plan-tab-links-description"><?= get_field("second_tab_subtitle"); ?></p>
			</button>
		<?php } ?>
	</div>
	<?php if($first_tab_title) { ?>
		<div id="primary-care" class="plan-tab-content">
			<div>
				<table class="plan-tab-table">
					<thead>
						<tr>
							<th></th>
							<th>
								<?php if($first_row["most_popular_flag"] == 1) { ?>
									<div class="plan-tab-most-popular">
										<p>Most popular</p>
										<div id="right-triangle"></div>
									</div>
								<?php } ?>
								<div class="plan-tab-content-box" >
									<p class="plan-tab-content-title"><?= $first_row["title_1st_row"] ?></p>
									<div class="plan-tab-content-description"><?= $first_row["price_1st_row"] ?></div>
									<a class="btn btn-secondary" href="<?= $first_row["button_link_1st_row"] ?>"><?= $first_row["button_text_1st_row"] ?></a>
								</div>
							</th>
							<th class="plan-tab-second-row">
								<?php if($first_row["most_popular_flag"] == 2) { ?>
									<div class="plan-tab-most-popular">
										<p>Most popular</p>
										<div id="right-triangle"></div>
									</div>
								<?php } ?>
								<div class="plan-tab-content-box" >
									<p class="plan-tab-content-title"><?= $first_row["title_2nd_row"] ?></p>
									<div class="plan-tab-content-description"><?= $first_row["price_2nd_row"] ?></div>
									<a class="btn btn-secondary" href="<?= $first_row["button_link_2nd_row"] ?>"><?= $first_row["button_text_2nd_row"] ?></a>
								</div>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php if($first_row['rows']) { ?>
							<?php foreach ($first_row['rows'] as $key => $row) { ?>
								<tr class="<?= $key >= 4 ? 'hide-rows': null; ?>">
									<td><?= $row['title']; ?></td>
									<td class="plan-tab-content-values">
										<?php if($row['text_1st_colunm']) { ?>
											<?= $row['text_1st_colunm'] ?>
										<?php } else { ?>
											<?php 
												switch ($row['image_1st_column']) {
													case 'check':
														echo '<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
														<circle cx="15" cy="15" r="15" fill="#6FCF97"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M24.2735 9.40342C24.8791 9.96867 24.9118 10.9179 24.3466 11.5235L13.8466 22.7735C13.569 23.0709 13.1826 23.2428 12.7759 23.2498C12.3691 23.2568 11.977 23.0983 11.6893 22.8107L6.43934 17.5607C5.85355 16.9749 5.85355 16.0251 6.43934 15.4393C7.02513 14.8536 7.97487 14.8536 8.56066 15.4393L12.7128 19.5915L22.1534 9.47653C22.7187 8.8709 23.6679 8.83817 24.2735 9.40342Z" fill="white"/>
														</svg>';
														break;
													case 'nohave':
														echo '<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
														<circle cx="15" cy="15" r="15" fill="#4D7088"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M21.1231 7.90342C21.7287 8.46867 21.7614 9.41786 21.1962 10.0235L10.6962 21.2735C10.1309 21.8791 9.18176 21.9118 8.57614 21.3466C7.97051 20.7813 7.93778 19.8322 8.50303 19.2265L19.003 7.97653C19.5683 7.3709 20.5175 7.33817 21.1231 7.90342Z" fill="white"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M8.57613 7.90342C7.9705 8.46867 7.93777 9.41786 8.50302 10.0235L19.003 21.2735C19.5683 21.8791 20.5175 21.9118 21.1231 21.3466C21.7287 20.7813 21.7614 19.8322 21.1962 19.2265L10.6962 7.97653C10.1309 7.3709 9.18175 7.33817 8.57613 7.90342Z" fill="white"/>
														</svg>';
														break;
													default:
														echo '<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
														<circle cx="15" cy="15" r="15" fill="#4D7088"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M21.1231 7.90342C21.7287 8.46867 21.7614 9.41786 21.1962 10.0235L10.6962 21.2735C10.1309 21.8791 9.18176 21.9118 8.57614 21.3466C7.97051 20.7813 7.93778 19.8322 8.50303 19.2265L19.003 7.97653C19.5683 7.3709 20.5175 7.33817 21.1231 7.90342Z" fill="white"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M8.57613 7.90342C7.9705 8.46867 7.93777 9.41786 8.50302 10.0235L19.003 21.2735C19.5683 21.8791 20.5175 21.9118 21.1231 21.3466C21.7287 20.7813 21.7614 19.8322 21.1962 19.2265L10.6962 7.97653C10.1309 7.3709 9.18175 7.33817 8.57613 7.90342Z" fill="white"/>
														</svg>';
														break;
												}
											?>
										<?php } ?>
									</td>
									<td class="plan-tab-content-values plan-tab-second-row">
										<?php if($row['text_2nd_colunm']) { ?>
											<?= $row['text_2nd_colunm'] ?>
										<?php } else { ?>
											<?php
												switch ($row['image_2nd_column']) {
													case 'check':
														echo '<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
														<circle cx="15" cy="15" r="15" fill="#6FCF97"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M24.2735 9.40342C24.8791 9.96867 24.9118 10.9179 24.3466 11.5235L13.8466 22.7735C13.569 23.0709 13.1826 23.2428 12.7759 23.2498C12.3691 23.2568 11.977 23.0983 11.6893 22.8107L6.43934 17.5607C5.85355 16.9749 5.85355 16.0251 6.43934 15.4393C7.02513 14.8536 7.97487 14.8536 8.56066 15.4393L12.7128 19.5915L22.1534 9.47653C22.7187 8.8709 23.6679 8.83817 24.2735 9.40342Z" fill="white"/>
														</svg>';
														break;
													case 'nohave':
														echo '<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
														<circle cx="15" cy="15" r="15" fill="#4D7088"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M21.1231 7.90342C21.7287 8.46867 21.7614 9.41786 21.1962 10.0235L10.6962 21.2735C10.1309 21.8791 9.18176 21.9118 8.57614 21.3466C7.97051 20.7813 7.93778 19.8322 8.50303 19.2265L19.003 7.97653C19.5683 7.3709 20.5175 7.33817 21.1231 7.90342Z" fill="white"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M8.57613 7.90342C7.9705 8.46867 7.93777 9.41786 8.50302 10.0235L19.003 21.2735C19.5683 21.8791 20.5175 21.9118 21.1231 21.3466C21.7287 20.7813 21.7614 19.8322 21.1962 19.2265L10.6962 7.97653C10.1309 7.3709 9.18175 7.33817 8.57613 7.90342Z" fill="white"/>
														</svg>';
														break;
													default:
														echo '<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
														<circle cx="15" cy="15" r="15" fill="#4D7088"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M21.1231 7.90342C21.7287 8.46867 21.7614 9.41786 21.1962 10.0235L10.6962 21.2735C10.1309 21.8791 9.18176 21.9118 8.57614 21.3466C7.97051 20.7813 7.93778 19.8322 8.50303 19.2265L19.003 7.97653C19.5683 7.3709 20.5175 7.33817 21.1231 7.90342Z" fill="white"/>
														<path fill-rule="evenodd" clip-rule="evenodd" d="M8.57613 7.90342C7.9705 8.46867 7.93777 9.41786 8.50302 10.0235L19.003 21.2735C19.5683 21.8791 20.5175 21.9118 21.1231 21.3466C21.7287 20.7813 21.7614 19.8322 21.1962 19.2265L10.6962 7.97653C10.1309 7.3709 9.18175 7.33817 8.57613 7.90342Z" fill="white"/>
														</svg>';
														break;
												}
											?>
										<?php } ?>
									</td>
								</tr>
							<?php } ?>
						<?php } ?>
					</tbody>
				</table>
				<?php if($first_row['rows'] && sizeof($first_row['rows']) > 4) { ?>
					<a onclick="hideRows()" id="show-rows-btn" class="show-rows-btn">
						<div class="show-more-tab-plan">
							Show more <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" clip-rule="evenodd" d="M0.851493 0.60186C0.493513 0.95984 0.493513 1.54024 0.851493 1.89822L6.35149 7.39822C6.70947 7.7562 7.28988 7.7562 7.64786 7.39822L13.1479 1.89822C13.5058 1.54024 13.5058 0.95984 13.1479 0.601859C12.7899 0.243878 12.2095 0.243878 11.8515 0.601859L6.99968 5.45368L2.14786 0.601859C1.78988 0.243879 1.20947 0.243879 0.851493 0.60186Z" fill="#2B8FFF"/>
									</svg>
						</div>
						<div class="show-less-tab-plan">
							Show less <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path fill-rule="evenodd" clip-rule="evenodd" d="M0.851493 7.39814C0.493513 7.04016 0.493513 6.45976 0.851493 6.10178L6.35149 0.601777C6.70947 0.243796 7.28988 0.243796 7.64786 0.601777L13.1479 6.10178C13.5058 6.45976 13.5058 7.04016 13.1479 7.39814C12.7899 7.75612 12.2095 7.75612 11.8515 7.39814L6.99968 2.54632L2.14786 7.39814C1.78988 7.75612 1.20947 7.75612 0.851493 7.39814Z" fill="#2B8FFF"/>
									</svg>
						</div>
					</a>
				<?php } ?>
				<div>
					<p class="cancel-text"><?= get_field("cancel_text_tab_plan"); ?></p>
				</div>
			</div>
		</div>
	<?php } ?>
	<?php if($second_tab_title) { ?>
		<div id="specialized-care" class="plan-tab-content">
			<h2 class="specialized-care-title"><?= $second_tab['section_title']; ?></h2>
			<div class="plan-tab-specialized-box">
				<div class="plan-tab-specialized">
					<div class="plan-tab-content-title specialized-box-title first-color">
						<?= $second_tab["title_1st_row"] ?>
					</div>
					<div class="plan-tab-content-title">
						<div class="plan-tab-content-description">
							<?= $second_tab["price_1st_row"] ?>
						</div>
						<a class="btn btn-secondary" href="<?= $second_tab["button_link_1st_row"] ?>">
							<?= $second_tab["button_text_1st_row"] ?>
						</a>
					</div>
				</div>
				<div class="plan-tab-specialized">
					<div class="plan-tab-content-title specialized-box-title second-color">
						<?= $second_tab["title_2nd_row"] ?>
					</div>
					<div class="plan-tab-content-title">
						<div class="plan-tab-content-description">
							<?= $second_tab["price_2nd_row"] ?>
						</div>
						<a class="btn btn-secondary" href="<?= $second_tab["button_link_2nd_row"] ?>">
							<?= $second_tab["button_text_2nd_row"] ?>
						</a>
					</div>
				</div>
			</div>
			<h2 class="contains_in_the_plan_title"><?= $second_tab['contains_in_the_plan_title']; ?></h2>
			<?php if($second_tab['rows']) { ?>
				<?php foreach ($second_tab['rows'] as $key => $value) { ?>
					<div class="contains_in_the_plan_box">
						<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
							<circle cx="10" cy="10" r="10" fill="#2B8FFF"/>
							<path fill-rule="evenodd" clip-rule="evenodd" d="M16.1823 6.26895C16.5861 6.64578 16.6079 7.27857 16.2311 7.68232L9.23106 15.1823C9.04601 15.3806 8.78839 15.4952 8.51723 15.4999C8.24608 15.5045 7.98466 15.3989 7.79289 15.2071L4.29289 11.7071C3.90237 11.3166 3.90237 10.6834 4.29289 10.2929C4.68342 9.90237 5.31658 9.90237 5.70711 10.2929L8.4752 13.061L14.7689 6.31769C15.1458 5.91393 15.7786 5.89211 16.1823 6.26895Z" fill="white"/>
						</svg>
						<p><?= $value['title']; ?></p>
					</div>
				<?php } ?>
			<?php } ?>
		</div>
	<?php } ?>
</div>

<script>
    function openPlanTab(evt, planTabName) {
        var i, planTabContent, planTabLinks;

        planTabContent = document.getElementsByClassName("plan-tab-content");
        for (i = 0; i < planTabContent.length; i++) {
            planTabContent[i].style.display = "none";
        }

        planTabLinks = document.getElementsByClassName("plan-tab-links");
        for (i = 0; i < planTabLinks.length; i++) {
            planTabLinks[i].className = planTabLinks[i].className.replace(" active", "");
        }

        document.getElementById(planTabName).style.display = "block";
        evt.currentTarget.className += " active";

        if(planTabName == 'specialized-care') {
            hideRowsSec()
        }
    }

	function hideRows() {
        document.getElementById('show-rows-btn').classList.toggle("active")
        let rows = document.querySelectorAll('.hide-rows')
        rows.forEach(element => {
            element.classList.toggle("hide")
        })
    }

	function hideRowsSec() {
        document.getElementById('show-rows-btn-sec').classList.toggle("active")
        let rows = document.querySelectorAll('.hide-rows-sec')
        rows.forEach(element => {
            element.classList.toggle("hide")
        })
    }

	document.addEventListener("DOMContentLoaded", function(event) {
		hideRows()
		document.getElementById("defaultPlanTab").click();
	});
</script>
