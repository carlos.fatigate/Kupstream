<?php

/*
 * pixelperfect lt: Page
 * 
 * @package WordPress
 * @subpackage pixelperfectlt
 */
get_header();
?>
<div class="section-feature">
	<div class="container">
		<div class="entry">
			<h1><?php _e("404" ,"theme") ?></h1>
		</div>
	</div>
</div>
<?php get_footer(); ?> 
