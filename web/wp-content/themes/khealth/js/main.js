jQuery(document).ready(function($) {

  $.fn.isOnScreen = function(ind){

    var win = $(window);
    var viewport = {
      top : win.scrollTop()
    };
    viewport.bottom = viewport.top + win.height()*ind;

    var bounds = this.offset();
    bounds.bottom = bounds.top + this.outerHeight();

    return (!(viewport.bottom < bounds.top || viewport.top > bounds.bottom));

  };

  function scrollCheck() {
    if ($(window).scrollTop() > 200) {
      $("body").addClass("header-moved2");
    } else {
      $("body").removeClass("header-moved2");
    }
    if ($(window).scrollTop() > 400) {
      $("body").addClass("header-moved");
    } else {
      $("body").removeClass("header-moved");
    }
  }
  var  $carousel = $('.slider-3-items').flickity({
    pageDots: false,
    imagesLoaded: true,
    lazyLoad: true,
    prevNextButtons: true,
    wrapAround: true,
    groupCells: true,
    cellAlign: 'left',
    contain: true,
    arrowShape: "M35.4 52.6l23.9 23.9c1.4 1.4 3.8 1.4 5.2 0 1.4-1.4 1.4-3.8 0-5.2L43.2 50l21.3-21.3c1.4-1.4 1.4-3.8 0-5.2-1.4-1.4-3.8-1.4-5.2 0L35.4 47.4c-.7.7-1.1 1.7-1.1 2.6.1 1 .4 1.9 1.1 2.6z"

  });

  function lazyload() {
    $("[data-srcset]:visible").each(function(){
      if ($(this).isOnScreen(1.5)) {
        $(this).attr('srcset', $(this).attr('data-srcset')).removeAttr('data-srcset');
      }
    })
    $("[data-data-flickity-lazyload]:visible").each(function(){
      if ($(this).isOnScreen(1.5)) {
        $(this).attr('data-flickity-lazyload', $(this).attr('data-data-flickity-lazyload')).removeAttr('data-data-flickity-lazyload');
        if ($(this).attr('data-data-flickity-lazyload-srcset')) {

          $(this).attr('data-flickity-lazyload-srcset', $(this).attr('data-data-flickity-lazyload-srcset')).removeAttr('data-data-flickity-lazyload-srcset');
        }
      }
      $(this).parentsUntil(".slider-3-items").parent(".slider-3-items").flickity( 'select', 1 )
    })

    $("[data-style]:visible").each(function(){
      if ($(this).isOnScreen(1.5)) {
        $(this).attr('style', $(this).attr('data-style')).removeAttr('data-style');
      }
    })
    $("[data-src]:visible").each(function(){
      if ($(this).isOnScreen(1.5)) {
        $(this).attr('src', $(this).attr('data-src')).removeAttr('data-src');
      }
    })
    $(".widget [data-js-src]").each(function(){
      if ($(this).parent().parent().isOnScreen(1.5)) {
        $(this).attr('src', $(this).attr('data-js-src')).removeAttr('data-js-src');
      }
    })

  }

  lazyload();
  scrollCheck();

  $(window).on('scroll resize', function() {
    lazyload();
    scrollCheck();
  });


  $(document).on('click', "[href*='#']", function () {
    if ($(this).attr("href").length > 1) {
      if ($($(this).attr("href")).length) {
        var next = $($(this).attr("href")).offset().top;
        $('html, body').stop().animate({
          scrollTop: next - $(".header").height()
        }, 400);
        return false;
      }
    }
  });
  $(document).on('click', ".faq-item .faq-title", function () {
    $(this).toggleClass("active").next().slideToggle();
    return false;
  });
  $(document).on('click', ".js-more", function () {
    $(this).toggleClass("active").next().slideToggle(300, function() {

    lazyload();
    });
    lazyload();
    return false;
  });
});
