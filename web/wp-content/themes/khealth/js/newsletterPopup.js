jQuery(document).ready(function($) {
    // set var
    var newsletterModal = $('#newsletterModal'),
    btnPopup = $('.newsletter-popup-btn'),
    btnClose = $('.newsletter-modal-close');
    $('.newsletter_request_response_popup').hide();

    // hide modal initially
    newsletterModal.hide();

    // When the user clicks the button, open the modal 
    btnPopup.click(function(){
        newsletterModal.show();
        analytics.track("Button Clicked", {
            button_context: "modal",
            button_href: "Redeem Now Modal",
            cta_text : "Redeem Now",
            page_url : window.location.pathname,
            session_id: analytics.user().anonymousId()
        });
    });
    // When the user clicks the close button, close the modal 
    btnClose.click(function(){
        console.log('btn close');
        newsletterModal.hide();
    });
    // When the user clicks out side the modal, close modal
    $(window).click(function(e) {
        // console.log('hit', e.target.id);
        if(e.target.id == 'newsletterModal') {
            newsletterModal.hide();
        }

    });
});

function sendNewsletterPopup() {
       
    $( `#newsletter-popup-form` ).submit(function( event ) {

        // Stop form from submitting normally
        event.preventDefault();
        event.stopImmediatePropagation();
        
        $("#newsletter_send_btn_popup").prop("disabled",true);
        $sending = false
        if(!$sending){
            $sending = true
            // Get some values from elements on the page:
            var $form = $( this ),
            newsletter_email = $form.find( "input[name='newsletter_email']" ).val(),
            campaign_newsletter = $form.find( "input[name='campaign_newsletter']" ).val(),
            url = $form.attr( "action" );

            // Send the data using post
            var posting = $.post( url, { newsletter_email, campaign_newsletter } );

            // Put the results in a div
            posting.done(function( data ) {
                $sending = false
                $("#newsletter_send_btn_popup").prop("disabled",false);
                $( `#newsletter-popup-form` ).find( "input[name='newsletter_email']" ).val("")
                $(".newsletter_request_response_popup").attr("style", "display:block")
            });
        }
    });
}

