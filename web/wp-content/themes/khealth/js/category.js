jQuery(document).ready(function($) {


  // Featured articles carousel block
  
  var  $categoryHerocarousel = $('.feat-cat-article-carousel').flickity({
    pageDots: true,
    imagesLoaded: true,
    lazyLoad: true,
    prevNextButtons: false,
    wrapAround: true,
    groupCells: 1,
    cellAlign: 'left',
    contain: true,
    adaptiveHeight: false,
    autoPlay: true
  });
});
