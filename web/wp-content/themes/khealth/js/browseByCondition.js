jQuery(document).ready(function($) {
  // if browser width is less than or = to 981
  if($(window).width() <= 981){
    // console.log('its 981px');
    // set variable
    x=3;
    // display first three items
    $('.conditon-items li').slice(0, 3).show();
    // on click
    $('#seeAll').on('click', function (e) {
        e.preventDefault();
        // add 3 + 3
        x = x+3;
        // display all items
        $('.conditon-items li').slice(0, x).slideDown();
        // hide #seeAll
        $('#seeAll').hide();
    });
  };

  // if user decides to resize browser window
  $(window).resize(function() {
    // if browser width is less than or = to 981
    if( $(this).width() <= 981 ) {
      // console.log('its 981px resize');
      // set variable
      x=3;
      // display first three items
      $('.conditon-items li').slice(0, 3).show();
      // on click
      $('#seeAll').on('click', function (e) {
        e.preventDefault();
        // add 3 + 3
        x = x+3;
        // display all items
        $('.conditon-items li').slice(0, x).slideDown();
        // hide #seeAll
        $('#seeAll').hide();
      });
    };
  });
});
