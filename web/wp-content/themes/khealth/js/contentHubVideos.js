class ContentHubVideos {
    constructor() {
        this.mainVideoHTML = $(".main-video");
        this.videoClicked = 
        this.clickedVideoArr = [];
        this.clickedVideoDetails = {
           url: "",
           doctorName: "",
           doctorUrl: "",
           date: "",
           blurb: "",
           title: "",
           dateTime: "",
           imageAlt: ""
        }
        this.columnVideos = document.querySelectorAll(".videos-column > div");
      
        this.events();
    }

    events() { 
        this.columnVideos.forEach(videoClicked => {
            videoClicked.addEventListener("click", () => this.changeActiveVideo(videoClicked));
        });
    }

    changeActiveVideo(videoClicked) {
        this.videoClicked = videoClicked;
        let clickedVideoDetails = $("span p", videoClicked);
        this.clickedVideoArr = [];
        for (let clickedDetail of clickedVideoDetails) {
            
            this.clickedVideoArr.push(clickedDetail);
        }

        this.clickedVideoDetails.url = this.clickedVideoArr[0]      
        this.clickedVideoDetails.date = this.clickedVideoArr[1]      
        this.clickedVideoDetails.doctorName = this.clickedVideoArr[2]      
        this.clickedVideoDetails.doctorUrl = this.clickedVideoArr[3]      
        this.clickedVideoDetails.blurb = this.clickedVideoArr[4]      
        this.clickedVideoDetails.title = this.clickedVideoArr[5]      
        this.clickedVideoDetails.dateTime = this.clickedVideoArr[6]      
        this.clickedVideoDetails.imageAlt = this.clickedVideoArr[7]      

        this.setActiveVideoHTML();
    }

    setActiveVideoHTML() {
        $('div iframe', this.mainVideoHTML).attr('src', this.clickedVideoDetails.url.innerHTML);
        $('.main-video_doctorName').text(this.clickedVideoDetails.doctorName.innerHTML);
        $('.main-video_doctorUrl').attr('href', this.clickedVideoDetails.doctorUrl.innerHTML)
        $('.main-video_date', this.mainVideoHTML).text(this.clickedVideoDetails.date.innerHTML)
        $('p:nth-of-type(2)', this.mainVideoHTML).text(this.clickedVideoDetails.blurb.innerHTML)
        
        $('.videos-column > div').each(function() { if($(this).hasClass("videos-column--active")) $(this).removeClass("videos-column--active") }); 
        
        $(this.videoClicked).addClass("videos-column--active");
    }
   
 
}

// only instantiate this Content Hub Videos class if block HTML in on a page
const contentHubVideosHTML = document.querySelector('#content-hub-videos-cont');
if (contentHubVideosHTML) { const contentHubVideos = new ContentHubVideos(); }

