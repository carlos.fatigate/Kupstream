jQuery(document).ready(function($) {

  $('#tabs li a:not(:first)').addClass('inactive');
  $('.article-item').hide();
  $('.article-item:first').show();
      
  $('#tabs li a').click(function(){
      var t = $(this).attr('id');
      console.log('t: ', t);
      if($(this).hasClass('inactive')){ //this is the start of our condition 
          $('#tabs li a').addClass('inactive');           
          $(this).removeClass('inactive');
          
          $('.article-item').hide();
          $('#'+ t + 'C').fadeIn('slow');
      }
  });

  // Featured articles carousel block
  
  var  $featArtcarousel = $('.featured_articles-slider').flickity({
    pageDots: true,
    imagesLoaded: true,
    lazyLoad: true,
    prevNextButtons: false,
    wrapAround: true,
    groupCells: 1,
    cellAlign: 'left',
    contain: true,
    adaptiveHeight: true

  });
});
