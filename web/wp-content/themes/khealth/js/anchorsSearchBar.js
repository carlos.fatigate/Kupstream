class AnchorsSearch {
    constructor() {
        this.searchField = document.querySelector("#search-term");
        this.closeButton = document.querySelector(".search-overlay__close");
        this.searchOverlay = document.querySelector(".search-overlay");
        this.resultsDiv = document.querySelector("#search-overlay__results");
        this.isOverlayOpen = false;
        this.typingTimer;
        this.isSpinnerVisible = false;
        this.previousValue;
        this.events();
    }

    events() { 
        this.searchField.addEventListener("keyup", e => this.keyPress(e))  
        this.closeButton.addEventListener("click", () => this.closerOverlay())
    }

    keyPress(e) {
        if ((e.keyCode >= 48 && e.keyCode <= 90) && !this.isOverlayOpen) {
            this.openOverlay()
        } 

        if ((e.keyCode == 13) && this.isOverlayOpen) {
            window.location.href = `${queryData.root_url + "?s=" + this.searchField.value}`;
        } 
        
        if (this.searchField.value != this.previousValue) {
            clearTimeout(this.typingTimer)
      
            if (this.searchField.value) {
                if (!this.isSpinnerVisible) {
                    this.resultsDiv.innerHTML = '<div class="spinner-loader"></div>';
                    this.isSpinnerVisible = true
                }
                this.typingTimer = setTimeout(this.getResults.bind(this), 750);
            } else if(this.searchField.value == '') {    
                this.closerOverlay();
            } else {
                this.resultsDiv.innerHTML = "";
                this.isSpinnerVisible = false; 
            }
          }
      
        this.previousValue = this.searchField.value
    } 

    async getResults() {
        try {
            const postsResponse = await fetch(queryData.root_url + '/wp-json/wp/v2/posts?per_page=3&search=' + this.searchField.value);
            const postsData = await postsResponse.json();


            this.resultsDiv.innerHTML = `
                <p class="search-overlay__section-title">RESULTS</p>
                ${postsData.length || this.resultsDiv.innerHTML == "" ? '<ul class="search-overlay__link-list min-list">' : "<p>No results</p>"}
                    ${postsData.map(results => `<li><a href="${results.link}">${results.title.rendered}</a></li>`).join("")}
                ${postsData.length ? `<li><a href="${queryData.root_url + "?s=" + this.searchField.value}">View All</a></li></ul>` : ""}
            `

            this.isSpinnerVisible = false;
        } catch(e) {
            this.resultsDiv.innerHTML = "<p>Unexpected error. Please contact administrator."
            console.log(e);
        }
    }

    openOverlay() {
        this.searchOverlay.classList.add("search-overlay--active")
        document.body.classList.add("body-no-scroll");
        this.isOverlayOpen = true
        return false;
    }

    closerOverlay() {
        this.searchField.value = ""
        this.searchOverlay.classList.remove("search-overlay--active")
        document.body.classList.remove("body-no-scroll")
        this.isOverlayOpen = false
    }   
}

// only instantiate this Anchors Search class if block HTML in on a page
const anchorsSearchBar = document.querySelector('#anchors-search-bar-block');
if (anchorsSearchBar) { const anchorsSearch = new AnchorsSearch(); }


