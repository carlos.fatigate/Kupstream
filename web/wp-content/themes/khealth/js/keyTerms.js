class KeyTerms {
    constructor() {
        this.keyTerms = document.querySelectorAll('.key-terms_terms > p');
        this.definitions = document.querySelectorAll('.key-terms_definitions-cont > div');
        this.events();
    }

    events() {
        this.keyTerms.forEach(termClicked => {
            termClicked.addEventListener("click", () => this.changeDefinition(termClicked));
        });
    }

    changeDefinition(termClicked) {

    //    console.log(this.definitions) 
       
        this.definitions.forEach(definition => {
           
            let defTitleNodeEl = $('p:nth-of-type(1)', definition)[0];
            let defTitle = $(defTitleNodeEl).text();

            

            if(defTitle === termClicked.innerHTML) {
                console.log("fired")
                $('div.def-active').removeClass('def-active')
                $(definition).addClass('def-active');
                $('p.term-active').removeClass('term-active');
                $(termClicked).addClass('term-active');
            }

        }) 
        
    }
}

const keyTerms = new KeyTerms();
