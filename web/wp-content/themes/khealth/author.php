<?php

/*
 * pixelperfect lt: Page
 * 
 * @package WordPress
 * @subpackage pixelperfectlt
 */
get_header();
$author = get_queried_object();

?>

<div class="post-author-details">
  <div class="container">
    <div class="row">
      <div class="col-md-7">

        <h1><?php echo get_userdata($author->ID)->display_name ?>
        </h1>
        <div class="entry">
<?php echo get_user_meta($author->ID, "profile_biography", true) ?>
          <?php if (get_user_meta($author->ID, "featured_quote", true) ) { ?>

          <blockquote>
            <p><?php echo get_user_meta($author->ID, "featured_quote", true) ?>
            </p>
          </blockquote>
            <?php
          } ?>


        </div>
        </div>
        <div class="col-md-5">
          <div class="image">
                <?php echo wp_get_attachment_image( get_user_meta($author->ID, "profile_photo", true), "avatarf" ); ?>
          </div>
        </div>
      </div>

          <?php if (get_user_meta($author->ID, "fast_facts", true) ) { ?>
      <div class="row">
        <div class="col-md-7">
          <div class="entry facts">
            <h3><?php _e("Fast facts", "theme") ?></h3><?php echo get_user_meta($author->ID, "fast_facts", true) ?>
          </div>
        </div>
      </div>
            <?php
          } ?>
    </div>
  </div>
</div>


      <?php 
      if ( have_posts() ): ?>
<div class="related-posts">
  <div class="container">
    <h2><?php _e("More posts from", "theme") ?> <?php echo get_the_author() ?>
    </h2>
    <div class="row "><?php
        while ( have_posts() ) : the_post();
          $col = "col-sm-6 col-md-4";
          include(get_template_directory() . '/part-post.php');
        endwhile;
   ?>
    </div>
    <div class="pagination">
      <?php next_posts_link('&laquo; Older Entries') ?>
      <?php previous_posts_link('Next Entries &raquo;') ?>
    </div>
  </div>
</div>
<?php     endif; ?>
<?php
get_footer(); ?>
