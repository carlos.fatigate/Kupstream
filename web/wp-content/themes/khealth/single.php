<?php
/*
 * pixelperfect lt: Page
 * 
 * @package WordPress
 * @subpackage pixelperfectlt
 */
$option_fields = get_fields("options");

get_header();
if ( have_posts() ):
	while ( have_posts() ) : the_post();
		$author = get_the_author_meta( 'ID' ) ?>
		<div class="breadcrumbs">
			<div class="container">
				<?php if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					echo '<span class="breadcrumbs-dots">...</span>';
				} else { ?>
					<ul>
						<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php _e("Home") ?></a></li>
						<?php $term_obj_list = get_the_terms( get_the_ID(), 'category' ); 
							$post_categories = get_post_primary_category(get_the_ID(), 'category'); 
							if(isset($post_categories['primary_category'])){
								$primary_category = $post_categories['primary_category'];
							}else{
								$primary_category = null;
							}
							if ($term_obj_list) {
								foreach ($term_obj_list as $key => $value) {
									echo '<li><a href="'.get_term_link( $value ).'">'.$value->name.'</a></li>';
								}
							} 
						?>
						<li><?php $pieces = explode(" ", get_the_title());
						echo implode(" ", array_splice($pieces, 0, 3));
						?></li>
					</ul>
				<?php } ?>
			</div>
		</div>
		<div>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="cat">
							<?php if(!empty($primary_category) && $primary_category->term_id != 1) { 
								echo '<a href="'.get_term_link( $primary_category ).'">'.$primary_category->name.'</a>';
							 } else {
								if ($term_obj_list) {
									foreach ($term_obj_list as $key => $value) {
										if($value->term_id != 1) { 
											echo '<a href="'.get_term_link( $value ).'">'.$value->name.'</a>';
										}
									}
								}
							} ?>
						</div>
						<h1><?php the_title() ?></h1>
						<div class="author">
							<a href="<?php echo esc_url( get_author_posts_url( $author ) ); ?>" title="<?php echo esc_attr( get_the_author() ); ?>">
								<span>By </span><span class="guids-doctor-name"><?php the_author(); ?></span> 
							</a>
							<?php if(get_field('medically_reviewed_check')) { ?>
								<div class="health-guids-divider"></div>
								<div class="medically-reviewed-box">
									<img class="medically-reviewed-image" src="<?= $option_fields["medically_reviewed_check"]; ?>">Medically reviewed
								</div>	
							<?php } ?>
							<div class="health-guids-divider"></div>
							<?php echo get_post_time('F j, Y', true); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="page-with-sidebar postes">
		<div class="container">
			<div class="row">
				<?php if(!get_field('remove_the_navigation_health_guides')) { ?>
					<div class="col-md-4">
						<div id="fixed-sidebar" class="sidebar fixed-sidebar">
							<p class="nav-title"> Table of contents</p>
							<?php if(get_field('anchor_links_nav_health_guides')) { ?>
								<?php foreach (get_field('anchor_links_nav_health_guides') as $link) { ?>
									<a class="anchor_links_nav_health_guides" href="<?= $link['anchor'] ?>"><?= $link['title'] ?></a>
								<?php } ?>
							<?php } ?>
						</div> 
						<div class="dropdown-health-guides">
							<div id="toggle-dropdown" class="dropdown-drugs-box toggle-dropdown" onclick="toggleDropdown()">
								<span class="nav-title"> Table of contents</span>
								<span class="dropdown-drugs-flex-item drugs-arrow-down"><i class="arrow down"></i></span> 
								<span class="dropdown-drugs-flex-item drugs-arrow-up"><i class="arrow up"></i></span> 
							</div>
							<div id="toggle-dropdown-drugs-content">
								<div class="dropdown-drugs-content dropdown-health-guids-anchor">
								<?php if(get_field('anchor_links_nav_health_guides')) { ?>
									<?php foreach (get_field('anchor_links_nav_health_guides') as $link) { ?>
										<a class="anchor_links_nav_health_guides" href="<?= $link['anchor'] ?>"><?= $link['title'] ?></a>
									<?php } ?>
								<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-8">
				<?php } else { ?>
					<div class="col-md-12">
				<?php } ?>
					<div id="links-box" class="entry">
						<?php  the_content() ?>
						<?php 
							$rows = get_field('faqs'); 
							if( $rows ) { ?>
								<div id="faq-block" class="section-faq">
									<div>
										<?php if (get_field("faqs_title")) { ?>
											<h2 class="section-head">
												<?php the_field("faqs_title") ?>
											</h2>
										<?php } ?>
										<div class="row">
											<?php foreach( $rows as $key => $row ) {
												$faq_ld = array(
															"@type" => "Question",
															"name" => $row['question'],
															"acceptedAnswer" => array(
																"@type" => "Answer",
																"text" => $row['answer']
															)
														);
												$faq_array_ld[] = $faq_ld; ?>
												<div class="col-md-12 health-guides-faq">
													<div class="faq-item">
														<div class="faq-title">
															<?= $row['question'] ?>
															<div class="toggler"></div>
														</div>
														<div class="faq-content">
															<div class="entry">
																<?= $row['answer'] ?>
															</div>
														</div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
						<?php } ?>
						<?php if(!get_field('disable_medical_disclaimer_health_guides')) { ?>
							<div class='medical-disclaimer'>
								<?php echo $option_fields["medical_disclaimer"]; ?>
							</div>
						<?php } ?>
						<div>
							<?php 
								$sources = get_field('sources');
								if( $sources ) { ?>
									<div class="source-block">
										<button id="source-box" class="source-box">
											<div class="source-test"> 
												<?= sizeof($sources) ?> Sources 
											</div> 
											<div class="toggler"></div>
										</button>
										<div id="source-list">
											<p class="source-description">K Health has strict sourcing guidelines and relies on peer-reviewed studies, academic research institutions, 
											and medical associations. We avoid using tertiary references.</p>
											<ul>
												<?php foreach( $sources as $row ) { ?>
													<li>
														<a href="<?= $row['url'] ?>" target="_blank">
															<p>
																<?= $row['source'] ?> <span><?= $row['url']; ?></span>
															</p>
														</a>
													</li>
												<?php } ?>
											</ul>
										</div>
									</div>
							<?php } ?>
						</div>
					</div>
					<div class="author-details">
						<div class="image">
							<a href="<?php echo esc_url( get_author_posts_url( $author ) ); ?>" title="<?php echo esc_attr( get_the_author() ); ?>">
								<?php echo wp_get_attachment_image( get_user_meta($author, "profile_photo", true), "thumbnail" ); ?>
							</a>
						</div>
						<div class="flex-description">
							<h4>
								<a href="<?php echo esc_url( get_author_posts_url( $author ) ); ?>" title="<?php echo esc_attr( get_the_author() ); ?>">
									<?php the_author(); ?>
								</a>
							</h4>
							<p>
								<?php echo get_user_meta($author, "description", true) ?>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		<?php
		if (get_field("title")) {
			include(get_template_directory() . '/blocks/cta.php');
		}
	endwhile;
endif; 
get_footer(); ?>

<script>
	document.addEventListener("DOMContentLoaded", function(event) {
		$(".body").css("overflow", "initial")
	});
	$(document).on('click', ".source-box", function () {
		$(this).toggleClass("active").next().slideToggle();
		return false;
	});
</script>

<?php 
	if(isset($faq_array_ld)){
		$faq_array_ld_json = json_encode($faq_array_ld);
	}?>
<script type="application/ld+json">
	{
	  "@context": "https://schema.org",
	  "@type": "FAQPage",
	  "mainEntity": [<?= $faq_array_ld_json ;?>]
	}
</script>
<script>
	$(document).ready(function() {
		var x = $('#links-box a[href^="#"]')
		var count = 0
		for (let index = 0; index < x.length; index++) {
			count++
			$('<a>', {
				class: 'item link',
				href: '#'+x[index].href.split('#')[1],
				text: x[index].text
			}).appendTo('.fixed-sidebar');

			$('<a>', {
				class: 'item link',
				href: '#'+x[index].href.split('#')[1],
				text: x[index].text
			}).appendTo('.dropdown-health-guids-anchor');
			
		}
		if(count = 0) {
			$('.fixed-sidebar').hide();
		}
	});
</script>