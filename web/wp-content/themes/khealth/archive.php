<?php

/*
 * pixelperfect lt: Page
 * 
 * @package WordPress
 * @subpackage pixelperfectlt
 */
get_header();
$post_obj = $wp_query->query;

if(strcmp($post_obj["post_type"], 'doctors') == 0) {
  header("Location:  /doctors/".$post_obj["name"]);
}
?>

<div class="page-hero text-dark bg-grey">
  <div class="container">
    <h1><?php


    if ( is_category() ) {
      $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
      $title = "".single_tag_title( '', false );
    } elseif ( is_author() ) {
      $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_post_type_archive() ) {
      $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
      $title = single_term_title( '', false );
    } elseif ( is_home() ) {
      $title  = get_the_title( get_option('page_for_posts', true) );

    } else {
     $title = 'Results for "'.get_search_query().'"';
    }

    echo $title;
    ?></h1>
    <div class="sub-entry">
      <?php 
      $category = get_queried_object();
      if ($category) {
      echo $category->description;
      }
      if ( is_home() ) {

$post_12 = get_post(get_option('page_for_posts', true)); 
echo $post_12->post_content;

    } 
      ?>
    </div>
  </div>
</div>

<div class="page-with-sidebar bg-white">
  <div class="container">
    <div class="row ">
      <div class="col-md-9">
        <div class="posts">
          <h2><?php _e("All articles", "theme") ?></h2>
          <div class="row">
            <?php 
            if ( have_posts() ):
              while ( have_posts() ) : the_post();
                include(get_template_directory() . '/part-post.php');
              endwhile;
            endif; ?>
          </div>
          <div class="pagination">
            <?php next_posts_link('&laquo; Older Entries') ?>
            <?php previous_posts_link('Next Entries &raquo;') ?>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="sidebar">
          <?php if ( !dynamic_sidebar( 'side2' ) ) : ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
get_footer(); ?>
