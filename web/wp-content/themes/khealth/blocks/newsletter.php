<?php 
    $background_color_newsletter = get_field("background_color_newsletter");
    $title_newsletter = get_field("title_newsletter");
    $description_newsletter = get_field("description_newsletter");
    $imagem_newsletter = get_field("imagem_newsletter");
    $cta_text_newsletter = get_field("cta_text_newsletter");
    $successful_newsletter_message = get_field("newsletter", "option");
    $success_message_newsletter = get_field("success_message_newsletter");

    if($success_message_newsletter){
        $successful_newsletter_message = $success_message_newsletter;
    }
    

    $url_img = get_stylesheet_directory_uri();
    
    $formName = "newsletter-email";

    if($imagem_newsletter){
        $formName = "newsletter-email-img";
    }
?>
<?php if($imagem_newsletter) { ?>
    <div class="newsletter_img">
<?php } ?>
        <div id="newsletter-block" class="<?= $background_color_newsletter; ?>">
            <div class="content_newsletter">
                <div class="container">
                    <h2 class="title_newsletter"><?= $title_newsletter; ?><div class="circle"></div></h2>
                    <div class="description_newsletter"><?= $description_newsletter; ?></div>
                    <div>
                        <form class="newsletter-email" method="post" id="<?= $formName; ?>" action="#newsletter-block">
                            <div class="newsletter_input_box">
                                <?= do_shortcode("[braze-newsletter]"); ?>
                                <?php if(!$cta_text_newsletter){ ?>
                                        <button onclick="sendNewsletter()" id="nesletter_send_btn">
                                            <svg class='nesletter_img' width="42" height="21" viewBox="0 0 42 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M31.3333 19.6667L40.6666 10.3333L31.3333 1" stroke="#A0C8E5" stroke-width="2" stroke-linejoin="round"/>
                                                <path d="M0.5 10.333L40.6667 10.333" stroke="#A0C8E5" stroke-width="2" stroke-linejoin="round"/>
                                            </svg>
                                        </button>
                                <?php } ?>
                            </div>
                            <?php if($cta_text_newsletter){ ?>
                                    <button onclick="sendNewsletter()" id="nesletter_send_btn" class="cta_text_newsletter"><?= $cta_text_newsletter; ?></button>
                            <?php } ?>
                            <div class="newsletter_request_response">
                                <?= $success_message_newsletter; ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
<?php if($imagem_newsletter) { ?>
            <div class="newsletter_img_bg" style="background-image:url('<?= $imagem_newsletter; ?>')"></div>
        </div>
    </div>
<?php } else { ?>
    </div>
<?php } ?>
<?php 
    $cta_text_newsletter_segment = "";
    if($cta_text_newsletter) {
        $cta_text_newsletter_segment = $cta_text_newsletter;
    }
?>
<script>
    function sendNewsletter() {

        analytics.track("Button Clicked", {
            button_context: "body",
            button_href: "Subscribe to newsletter",
            cta_text : "<?= $cta_text_newsletter_segment; ?>",
            page_url : window.location.pathname,
            session_id: analytics.user().anonymousId()
        });

        const form = '<?= $formName; ?>';
       
        $( `#${form}` ).submit(function( event ) {

            // Stop form from submitting normally
            event.preventDefault();
            event.stopImmediatePropagation();
            
            $("#nesletter_send_btn").prop("disabled",true);
            $sending = false
            if(!$sending){
                $sending = true
                // Get some values from elements on the page:
                var $form = $( this ),
                newsletter_email = $form.find( "input[name='newsletter_email']" ).val(),
                campaign_newsletter = $form.find( "input[name='campaign_newsletter']" ).val(),
                url = $form.attr( "action" );
                email_domain = newsletter_email.split("@")[1]

                analytics.track("Email Added", {
                    context: 'body',
                    email_domain: email_domain,
                    newsletter_campaign_name : campaign_newsletter,
                    session_id: analytics.user().anonymousId()
                });
            
                // Send the data using post
                var posting = $.post( url, { newsletter_email, campaign_newsletter } );
    
                // Put the results in a div
                posting.done(function( data ) {
                    $sending = false
                    $("#nesletter_send_btn").prop("disabled",false);
                    $( `#${form}` ).find( "input[name='newsletter_email']" ).val("")
                    $(".newsletter_request_response").attr("style", "display:block")
                });
            }
        });
    }
</script>