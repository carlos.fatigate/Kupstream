<?php 
	$container = 'container drugs-template';
	$col = "col-md-12";
	$col_block = "col-12 col-md-10 drugs-template-faq";

	$args = array(
		'posts_per_page' => -1,
			'post_type' => 'faq',
			'tax_query' => array(
				array(
					'taxonomy' => 'faqcat',
					'field'    => 'term_id',
					'terms'    => get_field("faq_categories"),
					'operator' => 'IN',
				),
			),
		);
	$wp_query = new WP_Query( $args );	
?>
<div id="faq-block" class="section-faq bg-<?php the_field("background") ?>">
	<div class="<?= $container; ?>">
		<?php if (get_field("title")) { ?>
			<h2 class="section-head">
				<?php the_field("title") ?>
			</h2>
		<?php } ?>
		<?php if (get_field("description")) { ?>
			<div class="sub-heading">
				<?php the_field("description") ?>
			</div>
		<?php } ?>
		<div class="row">
			<div class="<?= $col_block ?>">
				<?php while ($wp_query->have_posts()) : $wp_query->the_post();
					$faq_ld = array(
								"@type" => "Question",
								"name" => apply_filters('the_title', get_post_field('post_title', get_the_ID())),
								"acceptedAnswer" => array(
									"@type" => "Answer",
									"text" => apply_filters('the_content', get_post_field('post_content', get_the_ID()))
								)
							);
					$faq_array_ld[] = $faq_ld; ?>
					<div class="<?= $col ?>">
						<div class="faq-item">
							<div class="faq-title">
								<?php the_title() ?>
								<div class="toggler"></div>
							</div>
							<div class="faq-content">
								<div class="entry">
									<?php the_content() ?>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile;
					wp_reset_query(); ?>
			</div>
		</div>
	</div>
</div>
<?php $faq_array_ld_json = json_encode($faq_array_ld);?>
<script type="application/ld+json">
	{
	  "@context": "https://schema.org",
	  "@type": "FAQPage",
	  "mainEntity": [<?= $faq_array_ld_json ;?>]
	}
</script>
