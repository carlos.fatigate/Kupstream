<?php
    $background_color_hero_hub = get_field("background_color_hero_hub");
    $background_image_hero_hub = get_field("background_image_hero_hub");
    $title_hero_hub = get_field("title_hero_hub");
    $subtitle_hero_hub = get_field("subtitle_hero_hub");
    $cards_hero_hub = get_field("cards_hero_hub");
?>

<div id="hero-hub" style="background-color:<?= $background_color_hero_hub; ?>">
    <div class="hero-hub-image">
        <div class="container">
            <h1 class="title_hero_hub">
                <?= $title_hero_hub ?> <div class="circle"></div>
            </h1>
            <?php if($subtitle_hero_hub) { ?>
                <div class="subtitle_hero_hub">
                    <?= $subtitle_hero_hub; ?>
                </div>
            <?php } ?>
            <?php if($cards_hero_hub) { ?>
                <div class="cards_hero_hub_box">
                    <?php foreach ($cards_hero_hub as $key => $card) { ?>
                        <div class="card_hero_hub">
                            <div class="card_hero_hub_title">
                                <img class="icon_card_hero_hub" src="<?= $card['icon']?>" />
                                <p class="title_card_hero_hub">
                                    <?= $card['title']?>
                                </p>
                            </div>
                            <div class="card_hero_hub_title_mobile js-more" >
                                <div class="card_hero_hub_title">
                                    <img class="icon_card_hero_hub" src="<?= $card['icon']?>" />
                                    <p class="title_card_hero_hub">
                                        <?= $card['title']?>
                                    </p>
                                </div>
                                <svg class="arrow-right" width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1 13L7 7L1 1" stroke="#00406B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                <svg class="arrow-down" width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M1.00006 0.999999L7.00006 7L13.0001 1" stroke="#00406B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                            </div>
                            <div class="content_card_hero_hub_box">
                                <div class="content_card_hero_hub">
                                    <?= $card['content']?>
                                </div>
                                <a class="link_card_hero_hub" href="<?= $card['link']?>">
                                    See more
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<style>
    @media screen and (min-width: 981px) {
        .hero-hub-image {
            background-image:url('<?= $background_image_hero_hub; ?>')
        }
    }
</style>