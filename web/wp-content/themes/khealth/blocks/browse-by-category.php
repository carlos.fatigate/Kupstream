<?php
    $category_items = get_field('category_items');
    // var_dump($category_items);
?>

<div id="browse-by-category-block" class="browse_by_category">
    <h2>Browse by category</h2>
    <div class="container">
        <?php if($category_items): ?>
        <ul class="row cat-items desktop-category-only">
        <?php 
            foreach ($category_items as $category_item => $value):
                // set values for CF
                $category_feat_article = $value['cat_featured_article'];
                $category_bg_color = $value['bg_color_cat_card'];
                $icon_img = $value['icon_image'];
                $icon_img_altText = $value['icon_image_alt_text'];
                $category_title = $value['cat_title'];
                $category_link = $value['cat_link'];

        ?>
            <?php 
            // if $category_feat_article is true
            if($category_feat_article): 
                // set post ID from relationship CF
                $articleID = $value['select_cat_featured_article'][0];
                $permalink = get_permalink( $articleID );
                $title = get_the_title( $articleID );
                $date = get_the_date('F j, Y', $articleID);
                $feat_img = get_the_post_thumbnail_url($articleID,'full');
                $contentex = get_post_field( 'post_content', $articleID );
                $author = get_post_field( 'post_author', $articleID );
                $author_name = get_the_author_meta( 'display_name', $author );
                $category = get_the_category($articleID);

                $readTime = $value['article_read_time'];
            
            ?>

            <li class="col-md-9 feat-cat">
                <div class="cat-item" style="background-color:#<?= $category_bg_color; ?>">
                    <div class="quote-cat">
                        <div class="img-icon"><img src="<?= $icon_img; ?>" alt="<?= $icon_img_altText; ?>" /></div>
                        <h3>"<?= $category_title ?>"</h3>
                        <a href="<?= $category_link; ?>" title="See articles" class="see-article-link">See articles <span>→</span></a>
                    </div>
                    <div class="feat-cat-img">
                        <div class="feat-cat-img-hldr" style="background:url(<?= $feat_img; ?>) no-repeat center center; background-size: cover;"></div>
                    </div>
                    <div class="feat-cat-content">
                        <div class="category-read"><?php echo esc_html( $category[0]->cat_name ); ?> – <?= $readTime; ?> MIN READ</div>
                        <h4><?= $title; ?></h4>
                        <div class="author-date">By <?= $author_name; ?> | <?= $date; ?></div>
                        <div class="article-excerpt">
                            <p><?php echo wp_trim_words( $contentex, 17, '...' ); ?></p>
                        </div>
                        <a href="<?php echo esc_url( $permalink ); ?>" title="Read more">READ MORE</a>
                    </div>
                </div>
            </li>
            
            <?php else: ?>

            <li class="col-md-3">
                <div class="cat-item" style="background-color:#<?= $category_bg_color; ?>">
                    <div class="img-icon"><img src="<?= $icon_img; ?>" alt="<?= $icon_img_altText; ?>" /></div>
                    <h3><?= $category_title; ?></h3>
                    <a href="<?= $category_link; ?>" title="See articles" class="see-article-link">See articles <span>→</span></a>
                </div>
            </li>
            
            <?php endif; ?>
        <?php endforeach; ?>
        </ul>
        <?php endif; ?>
        
        <?php if($category_items): ?>
        <div class="slider-3-items row gutters-30 browse_by_category-slider mobile-only">
            <?php 
                foreach ($category_items as $category_item => $value):
                    // set values for CF
                    $category_feat_article = $value['cat_featured_article'];
                    $category_bg_color = $value['bg_color_cat_card'];
                    $icon_img = $value['icon_image'];
                    $icon_img_altText = $value['icon_image_alt_text'];
                    $category_title = $value['cat_title'];
                    $category_link = $value['cat_link'];

            ?>
            <?php
                // if $category_feat_article is true
                if(!$category_feat_article): 
             ?>
            <div class="col-sm-6 col-md-4 slide-item">
                <div class="cat-item" style="background-color:#<?= $category_bg_color; ?>">
                    <div class="img-icon"><img src="<?= $icon_img; ?>" alt="<?= $icon_img_altText; ?>" /></div>
                    <h3><?= $category_title; ?></h3>
                    <a href="<?= $category_link; ?>" title="See articles" class="see-article-link">See articles →</a>
                </div>
            </div>
            <?php 
                endif;
            endforeach; 
            ?>

        </div>
    </div>
    <?php endif; ?>
    
</div>