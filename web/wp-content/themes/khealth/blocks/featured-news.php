
<div id="featured-news-block" class="section-f-news bg-<?php the_field("background") ?>">
  <div class="container">
    <?php if (get_field("title")) {
      ?>
      <h2 class="section-head">
        <?php the_field("title") ?>
      </h2>
      <?php
    }  ?>
    <?php if (get_field("description")) {
      ?>
      <div class="sub-entry">
        <?php the_field("description") ?>
      </div>
      <?php
    }  ?>
    <div class="f-news-items row">
      <?php if( have_rows('posts') ):
        while( have_rows('posts') ): the_row(); ?>

          <div class="col-sm-6 col-md-3">
            <div class="f-news-item">
              <div class="image">
                <?php echo wp_get_attachment_image( get_sub_field('icon'), "full" ); ?>
              </div>
              <h3><?php the_sub_field('title'); ?></h3>
              <div class="date">
                <?php the_sub_field('date'); ?>
              </div>
              <?php $link = get_sub_field('link'); if( $link ): ?>
              <a href="<?php echo $link['url']; ?>" class="btn btn-secondary" <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?>><?php echo $link['title']; ?></a>
            <?php endif; ?>
          </div>
        </div>
      <?php endwhile;
    endif; ?>
  </div>
</div>
</div>