<?php 
	$newsletter_popup = get_field("newsletter_popup");
    $activate_newsletter_popup = get_field("activate_newsletter_popup");
	$text = $newsletter_popup['text'];
	$link_text = $newsletter_popup['link_text'];
	$content = $newsletter_popup['content'];
	$subtitle = $content['subtitle'];
	$title = $content['title'];
	$description = $content['description'];
	$cta_text = $content['cta_text'];
	$info = $content['info'];
    $successful_newsletter_popup_message = $content['successful_newsletter_popup_message'];
?>
<?php if ($activate_newsletter_popup) { ?>
   
    <div class="newsletter-popup">
        <?= $text ?><a class="newsletter-popup-btn"><?= $link_text ?></a>
    </div>

    <div>
        <div id="newsletterModal" class="newsletter-modal">
            <div class="newsletter-modal-content">
                <span class="newsletter-modal-close">&times;</span>
                <p class="special-offer"><?= $subtitle ?></p>
                <h3><?= $title ?></h3>
                <div class="newsletter-modal-copy">
                    <?= $description ?>
                </div>
                <form class="newsletter-popup-form" method="post" id="newsletter-popup-form" action="#newletter-popup">
                    <div class="newsletter_input_box">
                        <?= do_shortcode("[braze-newsletter]"); ?>
                    </div>
                    <button onclick="sendNewsletterPopup()" id="newsletter_send_btn_popup" class="cta_text_newsletter_popup btn btn-primary"><?= $cta_text; ?></button>
                    <div class="newsletter_request_response_popup">
                        <?= $successful_newsletter_popup_message; ?>
                    </div>
                </form>
                <div class="newsletter-modal-disclaimer">
                    <?= $info ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php 
    $cta_text_newsletter_segment = "";
    if($cta_text) {
        $cta_text_newsletter_segment = $cta_text;
    }
?>

<script>
    function sendNewsletterPopup() {

        analytics.track("Button Clicked", {
            button_context: "modal",
            button_href: "Promo Email Submit",
            cta_text : "<?= $cta_text_newsletter_segment; ?>",
            page_url : window.location.pathname,
            session_id: analytics.user().anonymousId()
        });
       
        $( `#newsletter-popup-form` ).submit(function( event ) {

            // Stop form from submitting normally
            event.preventDefault();
            event.stopImmediatePropagation();
            
            $("#newsletter_send_btn_popup").prop("disabled",true);
            $sending = false
            if(!$sending){
                $sending = true
                // Get some values from elements on the page:
                var $form = $( this ),
                newsletter_email = $form.find( "input[name='newsletter_email']" ).val(),
                campaign_newsletter = $form.find( "input[name='campaign_newsletter']" ).val(),
                url = $form.attr( "action" );
                email_domain = newsletter_email.split("@")[1]

                analytics.track("Email Added", {
                    context: 'web',
                    email_domain: email_domain,
                    newsletter_campaign_name : campaign_newsletter,
                    session_id: analytics.user().anonymousId()
                });
            
                // Send the data using post
                var posting = $.post( url, { newsletter_email, campaign_newsletter } );
    
                // Put the results in a div
                posting.done(function( data ) {
                    $sending = false
                    $("#newsletter_send_btn_popup").prop("disabled",false);
                    $( `#newsletter-popup-form` ).find( "input[name='newsletter_email']" ).val("")
                    $(".newsletter_request_response_popup").attr("style", "display:block")
                });
            }
        });
    }
</script>

