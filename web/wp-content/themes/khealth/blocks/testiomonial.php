<?php
    $image_testiomonial = get_field('image_testiomonial');
    $image_background_testiomonial = get_field('image_background_testiomonial');
    $title_testiomonial = get_field('title_testiomonial');
    $name_testiomonial = get_field('name_testiomonial');
    $role_testiomonial = get_field('role_testiomonial');
?>
<div id="testimonial_block" style="background-image:url('<?= $image_background_testiomonial; ?>')">
    <div class="container">
        <div class="image_testiomonial_box">
            <?php if($image_testiomonial) { ?>
                <div class="image_testiomonial">
                    <img src="<?= $image_testiomonial; ?>" />
                </div>
            <?php } ?>
            <div style="width:100%">
                <h3 class="title_testiomonial"><?= $title_testiomonial ?></h3>
                <p class="name_testiomonial"><?= $name_testiomonial; ?></p>
                <p class="role_testiomonial"><?= $role_testiomonial ?></p>
            </div>
        </div>
    </div>
</div>

<style>
    #testimonial_block {
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>