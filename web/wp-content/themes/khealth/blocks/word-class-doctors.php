<?php
    $title_word_class_doctors = get_field('title_word_class_doctors');
    $logos_word_class_doctors = get_field('logos_word_class_doctors');
    $itens_list_word_class_doctors = get_field('itens_list_word_class_doctors');
    $cta_text_word_class_doctors = get_field('cta_text_word_class_doctors');
    $cta_link_word_class_doctors = get_field('cta_link_word_class_doctors');
    $doctors_word_class_doctors = get_field('doctors_word_class_doctors');
    $background_image_word_class_doctors = get_field('background_image_word_class_doctors');
?>
<div id="word-class-doctors" style="background-image:url('<?= $background_image_word_class_doctors; ?>')">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="slider-3-items row gutters-30">
                    <?php if($doctors_word_class_doctors) { ?>
                        <?php foreach ($doctors_word_class_doctors as $key => $doctor) { 
                            $doctor_img = wp_get_attachment_image_src( get_post_thumbnail_id( $doctor ), 'full' );
                            if($doctor_img) {
                                $doctor_img = $doctor_img[0];
                            } else {
                                $doctor_img = get_field("doctor_biography_image_profile",$doctor);
                            }
                            $doctor_position = get_the_title($doctor);
                        ?>
                            <div class="col-12 slide-item">
                                <div style="margin:auto">
                                    <a href="<?php echo esc_url( get_permalink( $doctor ) ); ?>">
                                        <div class="doctors_word_class_doctors_img">
                                            <img src="<?= $doctor_img ?>" />
                                        </div>
                                    </a>
                                    <p class="doctors_word_class_doctors_position"><?= $doctor_position; ?></p>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
                <?php if($cta_text_word_class_doctors && $cta_link_word_class_doctors) { ?>
                    <a class="cta_text_word_class_doctors hide-desktop" href="<?= $cta_link_word_class_doctors ?>" ><?= $cta_text_word_class_doctors; ?></a>
                <?php } ?>
            </div>
            <div class="col-md-6">
                <h2><?= $title_word_class_doctors; ?></h2>
                <?php if($logos_word_class_doctors) { ?>
                    <div class="logos_word_class_doctors_box">
                        <?php foreach ($logos_word_class_doctors as $key => $logo) { ?>
                            <div>
                                <img src="<?= $logo['image']?>" />
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if($itens_list_word_class_doctors) { ?>
                    <ul class="itens_list_word_class_doctors">
                        <?php foreach ($itens_list_word_class_doctors as $key => $item) { ?>
                            <li>
                                <div><p><?= $item['text']?></p></div>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <?php if($cta_text_word_class_doctors && $cta_link_word_class_doctors) { ?>
                    <a class="cta_text_word_class_doctors hide-mobile" href="<?= $cta_link_word_class_doctors ?>" ><?= $cta_text_word_class_doctors; ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<style>
    #word-class-doctors {
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>