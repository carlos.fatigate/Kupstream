<?php
    $background = get_field('background_color_icon_banner');
    $text_color = get_field('text_color_icon_banner');
    $images = get_field('images_icon_banner');
?>
<div id="icon-banner" style='background-color:<?= $background ?>'>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="icon-banner-cards">
                    <?php  foreach ($images as $key => $value) { ?>
                        <div class="icon-banner-box">
                            <div><img class="icon-baner-image" src="<?= $value['image']?>" /></div>    
                            <div><p class="icon-baner-text" style="color:<?= $text_color?>"><?= $value['text']?></p></div>    
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>