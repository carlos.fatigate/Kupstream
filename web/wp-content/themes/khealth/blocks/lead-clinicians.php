<?php 
    $lead_clinicians = get_field("doctors_lead_clinicians"); 
    $lead_clinicians_size = sizeof($lead_clinicians); 
    $for_base = $lead_clinicians_size >= 4 ? 4 : $lead_clinicians_size;
?>
<div id="lead-clinicians">
    <div class="container">
        <h2 class="title-lead-clinicians">
            <?= get_field("title_lead_clinicians"); ?>
        </h2>
        <div class="description_lead-clinicians">
            <?= get_field("description_lead-clinicians"); ?>
        </div>
        <?php if($lead_clinicians) { ?>
            <div class="hide-mobile">
                <div class="row">
                    <?php foreach ($lead_clinicians as $key => $doctor) {
                        $id = $doctor->ID; ?>
                        <div class="col-6">
                            <div class="doctor-lead-clinicians">
                                <div class="row">
                                    <div class="col-12 col-md-3">
                                        <div class="image">
                                            <?php if(get_field("doctor_biography_image_profile",$id) ) { ?>
                                                <img src="<?= get_field("doctor_biography_image_profile",$id); ?>" alt="">
                                            <?php } else { ?>
                                                <?php 
                                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials' );
                                                    $imageR = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials@2x' );
                                                ?>
                                                <img data-src="<?= $image[0]; ?>" data-srcset="<?= $imageR[0]; ?> 2x" alt="">
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <h4 class="doctors-lead-clinicians-title"><?= $doctor->post_title ?></h4>
                                        <p class="doctors-lead-clinicians-position"><?= get_field("doctor_biography_doctor_position",$id) ?></p>
                                        <p class="doctors-lead-clinicians-degree-title">Bachelor's Degree</p>
                                        <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_undergraduate_school",$id) ?></p>
                                        <p class="doctors-lead-clinicians-degree-title">Medical Degree</p>
                                        <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_medical_degree",$id) ?></p>
                                        <p class="doctors-lead-clinicians-degree-title">Residency</p>
                                        <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_residency",$id) ?></p>
                                        <a class="doctors-lead-clinicians-link" href="<?= esc_url(get_permalink($id)); ?>">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="hide-desktop">
                <div class="row">
                    <?php for ($i=0; $i < $for_base; $i++) { ?>
                        <?php $id = $lead_clinicians[$i]->ID; ?>
                       <div class="col-6">
                            <div class="doctor-lead-clinicians">
                                <div class="row">
                                    <div class="col-12 col-md-3">
                                        <div class="image">
                                            <?php if(get_field("doctor_biography_image_profile",$id) ) { ?>
                                                <img src="<?= get_field("doctor_biography_image_profile",$id); ?>" alt="">
                                            <?php } else { ?>
                                                <?php 
                                                    $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials' );
                                                    $imageR = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials@2x' );
                                                ?>
                                                <img data-src="<?= $image[0]; ?>" data-srcset="<?= $imageR[0]; ?> 2x" alt="">
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <h4 class="doctors-lead-clinicians-title"><?= $lead_clinicians[$i]->post_title ?></h4>
                                        <p class="doctors-lead-clinicians-position"><?= get_field("doctor_biography_doctor_position",$id) ?></p>
                                        <p class="doctors-lead-clinicians-degree-title">Bachelor's Degree</p>
                                        <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_undergraduate_school",$id) ?></p>
                                        <p class="doctors-lead-clinicians-degree-title">Medical Degree</p>
                                        <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_medical_degree",$id) ?></p>
                                        <p class="doctors-lead-clinicians-degree-title">Residency</p>
                                        <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_residency",$id) ?></p>
                                        <a class="doctors-lead-clinicians-link" href="<?= esc_url(get_permalink($id)); ?>">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php if ($lead_clinicians_size > 4) { ?>
                    <div>
                        <div class="text-more js-more doctors-lead-clinicians-js-more">
                            <?php _e("View more", "theme") ?>
                        </div>
                        <div class="row" style="display:none">
                            <?php for ($i=$for_base; $i < $lead_clinicians_size; $i++) { ?>
                                <?php $id = $lead_clinicians[$i]->ID; ?>
                                <div class="col-6">
                                    <div class="doctor-lead-clinicians">
                                        <div class="row">
                                            <div class="col-12 col-md-3">
                                                <div class="image">
                                                    <?php if(get_field("doctor_biography_image_profile",$id) ) { ?>
                                                        <img src="<?= get_field("doctor_biography_image_profile",$id); ?>" alt="">
                                                    <?php } else { ?>
                                                        <?php 
                                                            $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials' );
                                                            $imageR = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials@2x' );
                                                        ?>
                                                        <img data-src="<?= $image[0]; ?>" data-srcset="<?= $imageR[0]; ?> 2x" alt="">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-9">
                                                <h4 class="doctors-lead-clinicians-title"><?= $lead_clinicians[$i]->post_title ?></h4>
                                                <p class="doctors-lead-clinicians-position"><?= get_field("doctor_biography_doctor_position",$id) ?></p>
                                                <p class="doctors-lead-clinicians-degree-title">Bachelor's Degree</p>
                                                <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_undergraduate_school",$id) ?></p>
                                                <p class="doctors-lead-clinicians-degree-title">Medical Degree</p>
                                                <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_medical_degree",$id) ?></p>
                                                <p class="doctors-lead-clinicians-degree-title">Residency</p>
                                                <p class="doctors-lead-clinicians-degree"><?= get_field("doctor_biography_residency",$id) ?></p>
                                                <a class="doctors-lead-clinicians-link" href="<?= esc_url(get_permalink($id)); ?>">Read more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <a class="doctors-lead-clinicians-directory" href="/browse-by-the-alpahabet/">See doctor directory</a>
            <?php } ?>
    </div>
</div>