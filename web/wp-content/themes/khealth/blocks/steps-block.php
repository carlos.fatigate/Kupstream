<?php
    $image_steps_block = get_field('image_steps_block');
    $title_steps_block = get_field('title_steps_block');
    $description_steps_block = get_field('description_steps_block');
    $steps_block = get_field('steps_block');
    $cta_text_steps_block = get_field('cta_text_steps_block');
    $cta_link_steps_block = get_field('cta_link_steps_block');
?>

<div id="steps_block">
    <div class="image_steps_block" style="background-image:url('<?= $image_steps_block; ?>')"></div>
    <div class="steps_block_content">
        <h2 class="title_steps_block" ><?= $title_steps_block; ?></h2>
        <?php if($steps_block) { ?>
            <div class="steps_block" >
                <?php foreach ($steps_block as $key => $step) { ?>
                    <div class="step_block" >
                        <div class="step_block_number">
                            <span>No.</span>
                            <p>0<?= $key + 1 ;?></p>
                        </div>
                        <div>
                            <p class="step_block_text"><?= $step['text']; ?></p>
                            <p class="description_steps_block"><?= $step['description']; ?></p>
                        </div>
                    </div>
                <?php }?>
            </div>
        <?php } ?>
        <?php if($cta_text_steps_block && $cta_link_steps_block) { ?>
            <a class="cta_steps_block" href="<?= $cta_link_steps_block; ?>"><?= $cta_text_steps_block; ?></a>
        <?php } ?>
    </div>
</div>