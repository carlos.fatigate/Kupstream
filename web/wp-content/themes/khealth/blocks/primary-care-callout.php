<?php
    $callout_title = get_field("callout_title");
    $callout_subtitle = get_field("callout_subtitle");
    $callout_cta_text = get_field("callout_cta_text");
    $callout_cta_link = get_field("callout_cta_link");
    $callout_cta_text_color = get_field("callout_cta_text_color");
    $callout_cta_background_color = get_field("callout_cta_background_color");
    $callout_cta_background_hover_color = get_field("callout_cta_background_hover_color");
    $callout_itens = get_field("callout_itens");
    $callout_model_image = get_field("callout_model_image");
    $callout_parallax_image = get_field("callout_parallax_image");

    $style_callout = "style = ";
    $btn_background_callout = "#8ef3d0";
    
    if ($callout_cta_background_color) { 
        $style_callout .= 'background-color:'.$callout_cta_background_color.';'; 
        $btn_background_callout = $callout_cta_background_color;
    }
    if ($callout_cta_text_color) { 
        $style_callout .= 'color:'.$callout_cta_text_color.';'; 
    }

    $style_callout .= "' '";

    $mouse_over_callout = "' '";
    $mouse_out_callout = "' '";

    if ($callout_cta_background_hover_color) {
        $mouse_over_callout = 'this.style.background="'.$callout_cta_background_hover_color.'"';
        $mouse_out_callout = 'this.style.background="'.$btn_background_callout.'"';
    }

    $hero_parallax_image_callout = get_field("callout_parallax_image");
    $hero_parallax_image_mobile_callout = get_field("callout_parallax_image_mobile");
    $callout_parallax_image_mobile_iphone = get_field("callout_parallax_image_mobile_iphone");
    
    if(!$hero_parallax_image_mobile_callout){
        $hero_parallax_image_mobile_callout = $hero_parallax_image_callout;
    }

    $search = 'Iphone';
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $is_iphone = false;
    if(preg_match("/{$search}/i", $user_agent)) {
        $is_iphone = true;
        if($callout_parallax_image_mobile_iphone){
            $hero_parallax_image_mobile_callout = $callout_parallax_image_mobile_iphone;
        } else {
            $hero_parallax_image_mobile_callout = $hero_parallax_image_callout;
        }
    }
?>

<div id="primary-care-callout">
    <div class="callout-parallax parallax-background_callout <?php if($is_iphone) { echo "parallax-background-iphone"; }?>">
        <div class="container">
            <div class="callout-flex">
                <div class="callout-model hide-mobile">
                    <img src="<?= $callout_model_image ?>" />
                </div>
                <div class="callout-content">
                    <div class="callout-content-box">
                        <h2 class="callout_title"><?= $callout_title ?></h2>
                        <p class="callout_subtitle"><?= $callout_subtitle ?></p>
                        <?php if($callout_itens) { ?>
                            <div>
                                <?php foreach ($callout_itens as $key => $item) { ?>
                                    <div class="callout_itens">
                                        <div class="callout_itens_svg">
                                            <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <circle cx="10" cy="10.1089" r="10" fill="#2B8FFF"/>
                                                <path fill-rule="evenodd" clip-rule="evenodd" 
                                                d="M16.1823 6.37784C16.5861 6.75467 16.6079 7.38746 16.2311 7.79121L9.23106 15.2912C9.04601 15.4895 
                                                8.78839 15.6041 8.51723 15.6087C8.24608 15.6134 7.98466 15.5078 7.79289 15.316L4.29289 11.816C3.90237 
                                                11.4255 3.90237 10.7923 4.29289 10.4018C4.68342 10.0113 5.31658 10.0113 5.70711 10.4018L8.4752 13.1699L14.7689 
                                                6.42657C15.1458 6.02282 15.7786 6.001 16.1823 6.37784Z" fill="white"/>
                                            </svg>
                                        </div>
                                        <p><?= $item['text']; ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div class="callout_cta">
                            <a href="<?= $callout_cta_link; ?>" class="btn btn-primary" 
                                <?php if ($callout_cta_link) { echo 'target="'.$callout_cta_link.'"'; } ?> 
                                <?php if(isset($style_callout)) { echo $style_callout; }; ?> 
                                onMouseOver = <?php if(isset($callout_cta_background_hover_color)) { echo $mouse_over_callout; } else { echo ""; }; ?>
                                onMouseOut = <?php if(isset($callout_cta_background_hover_color)) { echo $mouse_out_callout; } else { echo ""; }; ?>
                            >
                                <?= $callout_cta_text; ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .parallax-background_callout {
        background-image:url('<?= $hero_parallax_image_mobile_callout; ?>')
    }

    .parallax-background-iphone {
        background-image:url('<?= $hero_parallax_image_mobile_callout; ?>')
    }

    @media screen and (min-width: 981px) {
        .parallax-background_callout {
            background-image:url('<?= $hero_parallax_image_callout; ?>')
        }
    }
</style>