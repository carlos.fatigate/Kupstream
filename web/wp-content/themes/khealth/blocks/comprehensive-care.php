<?php
    $title_comprehensive_care = get_field('title_comprehensive_care');
    $cards_comprehensive_care = get_field('cards_comprehensive_care');
    $cta_text_comprehensive_care = get_field('cta_text_comprehensive_care');
    $cta_link_comprehensive_care = get_field('cta_link_comprehensive_care');
    $cta_color_comprehensive_care = get_field('cta_color_comprehensive_care');
    $bubbles_background_comprehensive_care = get_field('bubbles_background_comprehensive_care');
    $style = "";
    if($cta_color_comprehensive_care){
        $style = "background-color:".$cta_color_comprehensive_care;
    }
?>
<div id="comprehensive-care">
    <div class="container">
        <h2><?= $title_comprehensive_care ?></h2>
        <?php if($cards_comprehensive_care) { ?>
            <div class="cards_comprehensive_care_container">
                <?php foreach ($cards_comprehensive_care as $key => $item) { ?>
                    <a class="cards_comprehensive_care" href="<?= $item['link'] ?>">
                        <img class="cards_comprehensive_care_img" src="<?= $item['image'] ?>"/>
                        <p class="cards_comprehensive_care_title"><?= $item['title'] ?></p>
                        <p class="cards_comprehensive_care_description"><?= $item['description'] ?></p>
                    </a>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if($cta_text_comprehensive_care && $cta_link_comprehensive_care) { ?>
            <div class="btns center-row">
                <a style="<?= $style; ?>" href="<?= $cta_link_comprehensive_care; ?>" class="btn btn-primary"><?= $cta_text_comprehensive_care; ?></a>
            </div>
        <?php } ?>
    </div>
</div>

<style>
    #comprehensive-care {
        background-image: url('<?= $bubbles_background_comprehensive_care; ?>');
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>