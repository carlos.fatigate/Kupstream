
      <div class="section-cols bg-<?php the_field("background") ?> <?php echo get_field("style") == "reverse" ? 'reverse': '' ?> <?php echo get_field("background") == "dark" ? 'text-white': '' ?>" <?php echo get_field("background_copy") ? 'style="background: '.get_field("background_copy").'"': '' ?>>
        <div class="container">
    <?php if (get_field("sub_title")) {
      ?>
      <div class="sub-head">
        <?php the_field("sub_title") ?>
      </div>
      <?php
    }  ?>
    <?php if (get_field("title")) {
      ?>
      <h2 class="section-head">
        <?php the_field("title") ?>
      </h2>
      <?php
    }  ?>
    <?php if (get_field("description")) {
      ?>
      <div class="sub-entry">
        <?php the_field("description") ?>
      </div>
      <?php
    } if (get_field("sub_content")) {
      ?>
      <div class="sub-entry">
        <?php the_field("sub_content") ?>
      </div>
      <?php
    } 
if (get_field("style_copy") == "arcticles") {
  ?>
        <div class="row top">
        <?php if( have_rows('arcticles') ):
          while( have_rows('arcticles') ): the_row(); ?>

          <div class="col-md-6">
            <div class="article-insert">
              <div class="image">
                <?php echo wp_get_attachment_image( get_sub_field('image'), "full" ); ?>
              </div>
              <div class="text entry">
        <?php the_sub_field("content") ?>
              </div>
            </div>
          </div>
        <?php endwhile;
      endif; ?>
        </div>

  <?php
} else {
  ?>
          <div class="row">
            <div class="<?php echo get_field("style_copy") == "largecol" ? 'col-md-8': 'col-md-6' ?>">
              <div class="entry <?php echo get_field("style_copy") == "large" ? 'lg': '' ?>">
                <?php the_field("content") ?>
              </div>
              <?php 
              if (get_field("style_copy") == "largecol") {
                $secondary = true;
              }

              include(get_template_directory()."/part-btns.php"); ?>
            </div>
            <div class="<?php echo get_field("style_copy") == "largecol" ? 'col-md-4': 'col-md-6' ?> <?php echo get_field("style_copy") == "large" ? 'a-s-e': '' ?>">
              <?php echo get_field("style_copy") == "border" || get_field("style_copy") == "largecol" ? '<div class="image-border">': '' ?>
                <?php echo wp_get_attachment_image( get_field('image'), "full" ); ?>
              <?php echo get_field("style_copy") == "border" || get_field("style_copy") == "largecol" ? '</div>': '' ?>
            </div>
          </div>
  <?php
}

     ?>
        </div>
      </div>

