<?php 
$image = wp_get_attachment_image_src( get_field('background_image'), "cta" );
$imageR = wp_get_attachment_image_src( get_field('background_image'), "cta@2x" );

$cta_background_color = get_field('cta_background_color');
$cta_background_color_hover = get_field('cta_background_color_hover');
$cta_text_color = get_field('cta_text_color');

$style = "style = ";
$btn_background = "#8ef3d0";
if ($cta_background_color) { 
  $style .= 'background-color:'.$cta_background_color.';'; 
  $btn_background = $cta_background_color;
}
$svg_color = "#003355";
if ($cta_text_color) { 
  $style .= 'color:'.$cta_text_color.';'; 
  $svg_color = $cta_text_color;
}

$style .= "' '";
$mouse_over = "' '";
$mouse_out = "' '";
if ($cta_background_color_hover) {
  $mouse_over = 'this.style.background="'.$cta_background_color_hover.'"';
  $mouse_out = 'this.style.background="'.$btn_background.'"';
}

if ($image) { ?>

  <div class="section-started"  <?php echo get_field("background_color") ? 'style="background-color:'.get_field("background_color").'"' : "" ?>>
    <div class="bg retina" data-style="background-image: url(<?php echo $imageR[0] ?>);"></div>
    <div class="bg non-retina" data-style="background-image: url(<?php echo $image[0] ?>);"></div>
    <?php
  } else {?>
    <div class="section-cta <?php echo get_field("display_style") == "inline" ? 'inline' : "" ?> 
      <?php echo get_field("sub_title_color") == "dark" ? 'dark-sub-title' : "" ?>" <?php echo get_field("background_color") ? 'style="background-color:'.get_field("background_color").'"' : "" ?>>
    <?php } ?>
    <div class="container">
      <div class="sub-head">
        <?php the_field("sub_title") ?>
      </div>
      <h2 class="cta_title"><?php the_field("title") ?></h2>
        <?php if( have_rows('featured_items') ):?>
          <div class="row">
          <?php
          while( have_rows('featured_items') ): the_row(); ?>

            <div class="col-md-4">
              <div class="cta-feature">
                <div class="image">
                  <?php echo wp_get_attachment_image( get_sub_field('image'), "full" ); ?>
                </div>
                <?php the_sub_field("title") ?>
              </div>
            </div>

        <?php endwhile;?>
      </div>
          <?php
      endif; ?>

        <?php if( have_rows('white_boxes') ):?>
          <div class="row">
          <?php
          while( have_rows('white_boxes') ): the_row(); ?>

            <div class="col-md-6">
              <div class="whitebox-text entry lg">
                <?php the_sub_field("content") ?>
                <?php include(get_template_directory()."/part-btns.php"); ?>
              </div>
            </div>
        <?php endwhile;?>
      </div>
          <?php
      endif; ?>
      <?php include(get_template_directory()."/part-btns.php"); ?>
    </div>
  </div>