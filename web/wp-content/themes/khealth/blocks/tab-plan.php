<div id="tab-plan-block">
    <div class="hide-mobile">
        <?php include(get_template_directory()."/part-tab-plan-desktop.php"); ?>
    </div>
    <div class="hide-desktop">
        <?php include(get_template_directory()."/part-tab-plan-mobile.php"); ?>
    </div>
</div>
