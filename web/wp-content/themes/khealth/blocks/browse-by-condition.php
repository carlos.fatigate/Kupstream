<?php
    $condition_items = get_field('browse_by_condition_items');
    // var_dump($category_items);
?>

<div id="browse-by-condition-block" class="browse_by_condition">
    <h2>Browse by condition</h2>
    <div class="container">
        <?php if($condition_items): ?>
        <ul class="row conditon-items desktop-condition-only">
        <?php 
            foreach ($condition_items as $condition_item => $value):
                // set values for CF
                $condition_bg_color = $value['bg_color'];
                $icon_img = $value['icon_image'];
                $icon_img_altText = $value['icon_img_alt_text'];
                $condition_title = $value['condition_label'];
                $condition_link = $value['condition_link'];

        ?>
            

            <li class="col-md-4">
                <div class="conditon-item" style="background-color:#<?= $condition_bg_color; ?>">
                    <a href="<?= $condition_link; ?>" title="<?= $condition_title; ?>">
                        <div class="img-icon"><img src="<?= $icon_img; ?>" alt="<?= $icon_img_altText; ?>" /></div>
                        <h3><?= $condition_title; ?></h3>
                    </a>
                </div>
            </li>
            
           
        <?php endforeach; ?>
        </ul>
        <div id="seeAll">See all<svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path fill-rule="evenodd" clip-rule="evenodd" d="M0.910582 0.410704C1.23602 0.0852667 1.76366 0.0852667 2.08909 0.410704L7.08909 5.4107C7.41453 5.73614 7.41453 6.26378 7.08909 6.58921L2.08909 11.5892C1.76366 11.9147 1.23602 11.9147 0.910582 11.5892C0.585145 11.2638 0.585145 10.7361 0.910582 10.4107L5.32133 5.99996L0.910582 1.58921C0.585145 1.26378 0.585145 0.736141 0.910582 0.410704Z" fill="#2B8FFF"/>
        </svg></div>
        <?php endif; ?>
    </div>
        
        
    
</div>