
<div id="more-about-block" class="section-more-about bg-<?php the_field("background") ?>">
  <div class="container">
    <?php if (get_field("title")) {
      ?>
      <h2 class="section-head">
        <?php the_field("title") ?>
      </h2>
      <?php
    }  ?>
    <?php if (get_field("description")) {
      ?>
      <div class="sub-entry">
        <?php the_field("description") ?>
      </div>
      <?php
    }  ?>
    <div class="f-news-items row">
      <?php if( have_rows('posts') ):
        while( have_rows('posts') ): the_row(); ?>

                <div class="col-md-4">
              <?php $link = get_sub_field('link'); if( $link ): ?>
              <a href="<?php echo $link['url']; ?>" class="m-about-item" <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?>>
                    <div class="image">
                <?php echo wp_get_attachment_image( get_sub_field('icon'), "full" ); ?>
                    </div>
                    <h3><?php echo $link['title']; ?></h3>
                  </a>
            <?php endif; ?>
                </div>

      <?php endwhile;
    endif; ?>
  </div>
</div>
</div>

