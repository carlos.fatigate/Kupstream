<?php 
    $text_warning_block = get_field('text_warning_block');
    $below_column_block_warning_block = get_field('below_column_block_warning_block');
    $warning_box = get_field("warning_box","options");

    if($text_warning_block){
        $text_warning = $text_warning_block;
    } else {
        $text_warning = $warning_box['text'];
    }

    $style_warning = '';
    if($below_column_block_warning_block) {
        $style_warning = 'style="padding-top:7px"';
    }
?>
<div id="warning-block" <?= $style_warning; ?>>
    <div class="container">
        <div class="warning-block-box">
            <p><?= $text_warning; ?></p>
        </div>
    </div>
</div>