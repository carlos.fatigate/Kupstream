<?php
$title = get_field('title');
$terms = get_field('key_term');
?>

<div id="key-terms-block">
    <div class="key-terms_container">
        <p class="key-terms_title"><?= esc_attr($title); ?></p>
        <div class="key-terms_terms">
            <p class="term-active"><?php echo esc_attr($terms['term1']); ?></p>
            <p><?php echo esc_attr($terms['term2']); ?></p>
            <p><?php echo esc_attr($terms['term3']); ?></p>
            <p><?php echo esc_attr($terms['term4']); ?></p>
        </div>
        <div class="key-terms_definitions-cont"> 
            <div class="key-terms_definition def-active">   
                <p class="term-defTitle"><?= esc_attr($terms['term1']); ?></p>
                <p class="definition"><?php echo esc_attr($terms['def1']); ?></p>
            </div>
            <div class="key-terms_definition">   
                <p class="term-defTitle"><?= esc_attr($terms['term2']); ?></p>
                <p class="definition"><?php echo esc_attr($terms['def2']); ?></p>
            </div>
            <div class="key-terms_definition">   
                <p class="term-defTitle"><?= esc_attr($terms['term3']); ?></p>
                <p class="definition"><?php echo esc_attr($terms['def3']); ?></p>
            </div>
            <div class="key-terms_definition">   
                <p class="term-defTitle"><?= esc_attr($terms['term4']); ?></p>
                <p class="definition"><?php echo esc_attr($terms['def4']); ?></p>
            </div> 
            <div class="circle-peach"></div>
        </div>  

        <!-- absolute positioned ressponsive colored circles for background -->
        <div class="key-term_circles">
            <div class="circle-yellow"></div>
            <div class="circle-blue"></div>
            <div class="circle-mint"></div>
        </div>
        <!-- --------------------------------- -->
    </div>
</div>
