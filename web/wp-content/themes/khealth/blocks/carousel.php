<?php
    $carousel = get_field("title");
    $description = get_field("description");
    $stars_above_carousel = get_field("stars_above_carousel_text_carousel");
    $show_stars_above_carousel = get_field("stars_above_carousel_text_carousel");
?>

<div id="carousel-block" class="section-doctors <?php the_field("background_color") ?>">
    <div class="container">
        <?php if($stars_above_carousel) { ?>
            <div class="stars_above_carousel">
                <p><?= $show_stars_above_carousel ?></p>
                <svg width="204" height="35" viewBox="0 0 204 35" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.5 0L21.429 12.0788H34.1435L23.8572 19.5439L27.7862 31.6227L17.5 24.1576L7.21376 31.6227L11.1428 19.5439L0.85651 12.0788H13.571L17.5 0Z" fill="#FECD51"/>
                    <path d="M62.5 0L66.429 12.0788H79.1435L68.8572 19.5439L72.7862 31.6227L62.5 24.1576L52.2138 31.6227L56.1428 19.5439L45.8565 12.0788H58.571L62.5 0Z" fill="#FECD51"/>
                    <path d="M107.5 0L111.429 12.0788H124.143L113.857 19.5439L117.786 31.6227L107.5 24.1576L97.2138 31.6227L101.143 19.5439L90.8565 12.0788H103.571L107.5 0Z" fill="#FECD51"/>
                    <path d="M152.5 0L156.429 12.0788H169.143L158.857 19.5439L162.786 31.6227L152.5 24.1576L142.214 31.6227L146.143 19.5439L135.857 12.0788H148.571L152.5 0Z" fill="#FECD51"/>
                    <mask id="mask0" mask-type="alpha" maskUnits="userSpaceOnUse" x="178" y="0" width="26" height="35">
                    <rect x="178" width="26" height="34.96" fill="#C4C4C4"/>
                    </mask>
                    <g mask="url(#mask0)">
                    <path d="M197.738 0.00062561L201.667 12.0794H214.382L204.096 19.5445L208.025 31.6233L197.738 24.1582L187.452 31.6233L191.381 19.5445L181.095 12.0794H193.809L197.738 0.00062561Z" fill="#FECD51"/>
                    </g>
                </svg>
            </div>
        <?php } ?>
        <?php if ($carousel) { ?>
            <h2 class="section-head carousel-title">
                <?= $carousel ?>
            </h2>
        <?php } ?>
        <?php if ($description) { ?>
            <div class="sub-entry">
                <?= $description ?>
            </div>
        <?php } ?>
        <div class="slider-3-items row gutters-30 <?php if (get_field("slides") == "testimonials") { echo 'testimonials-carousel'; }; ?> ">
            <?php if (get_field("slides") == "testimonials") {
                if(get_field('testimonials_category_carousel_block')) {
                    $args = array(
                        'post_type' => array( 'testiomonials' ),
                        'posts_per_page' => 12,
                        'tax_query' => array(
                        array(
                            'taxonomy' => 'testiomonialscat',
                            'field' => 'term_id',
                            'terms' => get_field('testimonials_category_carousel_block')
                        )
                        )
                    );
                } else {
                    $args = array(
                        'post_type' => array( 'testiomonials' ),
                        'posts_per_page' => 12,
                    );
                }
                $wp_query = new WP_Query( $args );
                $numberPost = 0;
                while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                    <div class="col-sm-6 col-md-3 slide-item">
                        <div class="quote-item testimonials">
                            <blockquote>
                                <p class="title_testimonials"> <?= get_field("title_testimonials", get_the_ID()); ?> </p>
                                <div class="title_testimonials_stars">
                                    <svg width="92" height="16" viewBox="0 0 92 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M8 0.961151L9.68386 6.14352H15.1329L10.7245 9.34641L12.4084 14.5288L8 11.3259L3.59161 14.5288L5.27547 9.34641L0.867076 6.14352H6.31614L8 0.961151Z" fill="#FECD51"/>
                                        <path d="M27 0.961151L28.6839 6.14352H34.1329L29.7245 9.34641L31.4084 14.5288L27 11.3259L22.5916 14.5288L24.2755 9.34641L19.8671 6.14352H25.3161L27 0.961151Z" fill="#FECD51"/>
                                        <path d="M46 0.961151L47.6839 6.14352H53.1329L48.7245 9.34641L50.4084 14.5288L46 11.3259L41.5916 14.5288L43.2755 9.34641L38.8671 6.14352H44.3161L46 0.961151Z" fill="#FECD51"/>
                                        <path d="M65 0.961151L66.6839 6.14352H72.1329L67.7245 9.34641L69.4084 14.5288L65 11.3259L60.5916 14.5288L62.2755 9.34641L57.8671 6.14352H63.3161L65 0.961151Z" fill="#FECD51"/>
                                        <path d="M84 0.961151L85.6839 6.14352H91.1329L86.7245 9.34641L88.4084 14.5288L84 11.3259L79.5916 14.5288L81.2755 9.34641L76.8671 6.14352H82.3161L84 0.961151Z" fill="#FECD51"/>
                                    </svg>
                                </div>
                                <div>
                                    <div class="testimonial_content" id="number-post-<?= $numberPost; ?>">
                                        <?php the_content() ?>
                                    </div>
                                    <a class="js-more-testimonial">Read more</a>
                                    <a class="js-less-testimonial">Read less</a>
                                </div>
                                <cite>
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                        viewBox="0 0 16 16" style="enable-background:new 0 0 16 16;" xml:space="preserve">
                                    <style type="text/css">
                                        .st0{fill:#2B8FFF;}
                                        .st1{fill:none;stroke:#FFFFFF;stroke-width:0.7174;stroke-linecap:round;stroke-linejoin:round;}
                                    </style>
                                    <g>
                                        <circle class="st0" cx="8" cy="8" r="7.5"/>
                                        <path class="st1" d="M11,6l-4.1,4.1L5,8.3"/>
                                    </g>
                                    </svg>
                                    <?php the_title() ?>
                                    | <?= get_the_date('M d Y'); ?>
                                </cite>
                            </blockquote>
                        </div>
                    </div>
                    <?php $numberPost++; ?>
                <?php endwhile;
                wp_reset_query();
            } elseif (get_field("slides") == "doctors") {
                $args = array(
                    'post_type' => array( 'doctors' ),
                    'orderby' => "date",
                    'order' => "DESC",
                    'posts_per_page' => 20,
                    'meta_query' => array(
                        array(
                            'key'     => 'slider_text',
                        ),
                    ),
                );
                $wp_query = new WP_Query( $args );

                while ($wp_query->have_posts()) : $wp_query->the_post(); 
                    echo '<div class="col-sm-6 col-md-4 slide-item">';
                    $sliderR = true;
                    include(get_template_directory()."/part-doctor.php");
                    echo '</div>';
                endwhile;
                wp_reset_query();
            } ?>
        </div>

        <?php $link = get_field('read_more_button'); if( $link ): ?>
            <div class="read-more">
                <a href="<?php echo $link['url']; ?>" class="btn btn-secondary" <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?>><?php echo $link['title']; ?></a>
            </div>
        <?php endif; ?>
    </div>
</div>