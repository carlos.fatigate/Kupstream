<?php
    $faq_image = get_field('faq_image');
    $faq_image_title = get_field('faq_image_title');
    $faq_image_questions = get_field('faq_image_questions');
    $faq_image_link_text = get_field('faq_image_link_text');
    $faq_image_link_url = get_field('faq_image_link_url');
?>
<div id="faq_image">
    <div class="faq_image_src" style="background-image:url('<?= $faq_image ?>')"></div>
    <div class="faq_image_content">
        <h2 class="faq_image_title"><?= $faq_image_title; ?></h2>
        <div>
            <?php if($faq_image_questions) { ?>
                <?php foreach ($faq_image_questions as $key => $question) { ?>
                    <div class="faq_image_question_box">
                        <div class="faq-item">
                            <div class="faq_image_question faq-title-image ">
                                <svg class="faq_image_plus" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M12 5V19" stroke="#00406B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M5 12H19" stroke="#00406B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                <svg class="faq_image_minus" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5 12H19" stroke="#00406B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                <p>
                                    <?= $question->post_title?>
                                </p>
                            </div>
                            <div class="faq_image_question_answer" id="faq_image_question_answer">
                                <?= $question->post_content?>
                            </div>
                        </div>
                    </div>
                <?php $faq_ld = array(
								"@type" => "Question",
								"name" => $question->post_title,
								"acceptedAnswer" => array(
									"@type" => "Answer",
									"text" => $question->post_content
								)
							);
					$faq_array_ld[] = $faq_ld; } ?>
            <?php } ?>
        </div>
        <?php if($faq_image_link_url) { ?>
            <a class="faq_image_link_text" href="<?= $faq_image_link_url; ?>">
                <?= $faq_image_link_text; ?>
                <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.10198 0.851818C1.45996 0.493837 2.04036 0.493837 2.39834 0.851818L7.89835 6.35182C8.25633 6.7098 8.25633 7.2902 7.89835 7.64818L2.39834 13.1482C2.04036 13.5062 1.45996 13.5062 1.10198 13.1482C0.744001 12.7902 0.744001 12.2098 1.10198 11.8518L5.9538 7L1.10198 2.14818C0.744001 1.7902 0.744001 1.2098 1.10198 0.851818Z" fill="#2B8FFF"/>
                </svg>
            </a>
        <?php } ?>
    </div>
</div>
<?php $faq_array_ld_json = json_encode($faq_array_ld);?>
<script type="application/ld+json">
	{
	  "@context": "https://schema.org",
	  "@type": "FAQPage",
	  "mainEntity": [<?= $faq_array_ld_json ;?>]
	}
</script>
