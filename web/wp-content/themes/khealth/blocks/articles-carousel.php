<?php
   $section_title = get_field("articles_section_title");
   $articles_block_articles = get_field("articles_carousel");
   $background_color = get_field("background_color");
?>
<div id="articles-carousel-block" style="background-color:<?php echo esc_attr($background_color); ?>">
    <div class="container">
        <h2 class="title_articles-carousel"><?= $section_title ?></h2>
        <div class="slider-3-items row gutters-30">
            <?php 

                $index = 1;
                foreach ($articles_block_articles as $article => $value) { 
                    // set post ID from relationship CF
                    $articleID = $value['article'][0];
                    $permalink = get_permalink( $articleID );
                    $title = get_the_title( $articleID );
                    $date = get_the_date('F j, Y', $articleID);
                    $dateToUpper = strtoupper($date);
                    $feat_img = get_the_post_thumbnail_url($articleID,'full');
                    $contentex = get_post_field( 'post_content', $articleID );
                    $category = get_the_category($articleID);
                    $cat = $category[0]->cat_name;
                    $catToUpper = strtoupper($cat);
                    $author = get_post_field( 'post_author', $articleID );
                    $author_name = get_the_author_meta( 'display_name', $author );
                    $authNameToUpper = strtoupper($author_name);
                    $author_ID = get_the_author_meta( 'ID', $author );
                    
                    $readTime = $value['read_time'];
                
                ?>
                <div class="col-12 col-md-6 col-lg-4 col-xl-4 slide-item">
                    <a href="<?= esc_url( $permalink ); ?>" >
                        <div class="articles-carousel">
                            <div class="articles-carousel_info">
                                <div class="articles_carousel_feat-img">
                                  <img src="<?php echo esc_attr($feat_img); ?>" />
                                </div>
                                <p class="articles-carousel_category"><?php echo esc_attr($catToUpper); ?> - <?php echo esc_attr($readTime); ?> MIN READ</p>    
                                <p class="articles-carousel_title" >
                                    <?= wp_strip_all_tags( $title, true ); ?> 
                                </p>
                                <div class="articles-carousel_author-info">
                                    <div>
                                        <?php echo wp_get_attachment_image( get_user_meta($author_ID, "profile_photo", true), "avatarf" ); ?>
                                    </div>    
                                    <p>BY <?= esc_attr($authNameToUpper) ?><br>
                                        <?= esc_attr($dateToUpper) ?>
                                    </p>
                                </div>
                                <div>
                                    <p class="articles-carousel_article-content"><?php echo wp_trim_words( $contentex, 25, '...' ); ?></p>
                                    <a class="articles-carousel_read-more" href="<?php echo esc_attr($permalink); ?>">READ MORE</a>
                                </div>
                                <div class="articles-carousel_bottom-border"></div>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    
    </div>
</div>