<div id='youtube-block' class="video_container">
    <?php if (get_field("video_code")) { ?>
        <iframe class="responsive-iframe" src="https://www.youtube.com/embed/<?php the_field('video_code'); ?>"></iframe>
    <?php }  ?>
</div>