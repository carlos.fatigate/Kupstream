<div id="hero-block-lp" class="section-hero" style="background-color: <?= get_field('background_color_hero_lp'); ?>;">
    <div class="container">
        <div class="row a-center">
            <div class="col-12">
                <p class="subtitle-hero-lp"><?= get_field('subtitle_hero_lp'); ?></p>
                <h1 class="title-hero-lp"><?= get_field('title_hero_lp'); ?></h1>
                <p class="description-hero-lp"><?= get_field('description_hero_lp'); ?></p>
                <div class="jump-links-hero-lp-box">
                    <?php foreach (get_field('jump_links_hero_lp') as $key => $value) { ?>
                        <a class="jump-links-hero-lp" href="<?= $value['anchor_id']; ?>" >
                            <div class="jump-links-hero-lp-flex-box">
                                <div>
                                    <?= $value['text']; ?>
                                </div>
                                <div>
                                    <span id="source-arrow"><i class="arrow down"></i></span>
                                </div>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cta-hero-lp-box">
    <div class="container">
        <a class="cta-hero-lp btn" href="<?php 
									$url = get_field('cta_link_hero_lp');
									$url = str_replace("[acf field='", "", $url);
									$url = str_replace("']", "", $url);
									print_r(get_field($url, "option"));
								?>"><?= get_field('cta_text_hero_lp'); ?></a>
    </div>
</div>