<?php $doctors_list = get_field('doctors_leader_block'); 
    $doctors_list_size = sizeof($doctors_list); 
    $for_base = $doctors_list_size;
    $for_base_mobile = $doctors_list_size;
    $title_doctors_leader = get_field("title_doctors_leader");
    $description_doctors_leader = get_field("description_doctors_leader");
?>
<div id="doctors-list-leader" class="section-text bg-white" style="background-color: <?= get_field('background_color_leader_block')?>">
    <div class="container">
        <?php if ($title_doctors_leader) { ?>
            <h2 class="section-head">
                <?= $title_doctors_leader ?>
            </h2>
        <?php } ?>
        <?php if ($description_doctors_leader) { ?>
            <div class="sub-entry">
                <?= $description_doctors_leader ?>
            </div>
        <?php } ?>
        <div class="hide-mobile">
            <div class="doctors-list row gutters-30 doctors-list-block">
                <?php for ($i=0; $i < $for_base; $i++) {
                    $id = $doctors_list[$i]->ID;
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials' );
                    $imageR = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials@2x' );?>
                    <div class="col-12 col-sm-6 col-md-3">
                        <div class="doctor-leader">
                            <div class="image">
                                <img data-src="<?= $image[0]; ?>" data-srcset="<?= $imageR[0]; ?> 2x" alt="">
                            </div>
                            <h4 class="doctors-leader-title"><?= $doctors_list[$i]->post_title ?></h4>
                            <?php if (get_field("role", $id)) { ?>
                                <div class="doctors-leader-role">
                                    <?= get_field("role", $id) ?>
                                </div>
                            <?php } ?>
                            <div class="text-more js-more doctors-leader-read-more">
                                <?php _e("Read more", "theme") ?>
                            </div>
                            <div class="doctors-leader-description" style="display:none">
                                <?= get_field("description", $id); ?>
                                <div class="text-more doctors-leader-read-more doctors-leader-read-more-prev">
                                    <?php _e("Read less", "theme") ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
        </div>
        <div class="hide-desktop">
            <div class="doctors-list row gutters-30 doctors-list-block">
                <?php for ($i=0; $i < $for_base_mobile; $i++) {
                    $id = $doctors_list[$i]->ID;
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials' );
                    $imageR = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials@2x' );?>
                    <div class="col-6">
                        <div class="doctor-leader">
                            <div class="image">
                                <img data-src="<?= $image[0]; ?>" data-srcset="<?= $imageR[0]; ?> 2x" alt="">
                            </div>
                            <h4 class="doctors-leader-title"><?= $doctors_list[$i]->post_title ?></h4>
                            <?php if (get_field("role", $id)) { ?>
                                <div class="doctors-leader-role">
                                    <?= get_field("role", $id) ?>
                                </div>
                            <?php } ?>
                            <div class="text-more js-more doctors-leader-read-more">
                                <?php _e("Read more", "theme") ?>
                            </div>
                            <div class="doctors-leader-description" style="display:none">
                                <?= get_field("description", $id); ?>
                                <div class="text-more doctors-leader-read-more doctors-leader-read-more-prev">
                                    <?php _e("Read less", "theme") ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
