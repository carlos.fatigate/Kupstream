<?php
    // title
    $symptom_title = get_field('symptom_title');
    // copy
    $symptom_copy = get_field('symptom_copy');
    // image and image alt text
    $symptom_bg_img = get_field('bg_img');
    $symptom_bg_img_alt = get_field('bg_img_alt');
    // CTA attributes
    $symptom_cta_att = get_field('cta_attribute');
    $symptom_cta_text = $symptom_cta_att['cta_text'];
    $symptom_mobile_cta_text = $symptom_cta_att['mobile_cta_text'];
    $symptom_cta_url = $symptom_cta_att['cta_url'];
    $symptom_cta_bg = $symptom_cta_att['cta_bg_color'];
    // var_dump($symptom_cta_att);
    
?>

<div id="symptom-checker-cta-block" class="symptom-checker-cta">
    <div class="container">
        <div class="symptom-hldr row">
            <div class="content-copy col-md-6">
                <div class="content-copy-hldr">
                    <h2><?= $symptom_title; ?></h2>
                    <p><?= $symptom_copy; ?></p>
                    <a href="<?= $symptom_copy; ?>" title="<?= $symptom_cta_text; ?>" style="background: #<?= $symptom_cta_bg; ?>;" class="btn btn-primary"><span class="desktop-only"><?= $symptom_cta_text; ?></span><span class="mobile-only"><?= $symptom_mobile_cta_text; ?></span></a>
                </div>
            </div>
            <div class="symptom-img col-md-6">
                <img src="<?= $symptom_bg_img; ?>" alt="<?= $symptom_bg_img_alt; ?>" />
            </div> 
        </div>
    </div>
</div>