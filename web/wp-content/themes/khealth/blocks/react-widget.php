<?php 
    $my_options = get_field("links_widget","options");
    $diagnoses_names = [];
    $diagnoses_links = [];
    if($my_options){
        foreach ($my_options as $option) {
            $diagnoses_names[] = $option['title'];
            $diagnoses_links[] = $option['link'];
        }
    }
?>
<div>
    <input type="hidden" value="<?= get_field('types');  ?>" id="widgetType" />
    <?php echo do_shortcode("[erw_widget]"); ?>
</div>

<script>
	function generateLink(linkId, diagnoses_names, diagnoses_link){
		let diagnose0 = diagnoses_names.indexOf(document.getElementById(linkId).text)
		if(diagnose0 >=0) {
			if(diagnoses_link[diagnose0] && diagnoses_link[diagnose0] != 'undefined' ){
				document.getElementById(linkId).href=diagnoses_link[diagnose0]
			} else {
				document.getElementById(linkId+'-svg').style.display = "none";
			}
		} else {
			document.getElementById(linkId+'-svg').style.display = "none";
		}
	} 

	var targetNode = document.getElementById('diagnoses');
	var observer = new MutationObserver(function(){
		if(!targetNode.classList.contains('hide-diagnoses')){
			let diagnoses_names = <?php echo json_encode($diagnoses_names); ?>;
			let diagnoses_link = <?php echo json_encode($diagnoses_links); ?>;
			
			generateLink('diagnose0', diagnoses_names, diagnoses_link)
			generateLink('diagnose1', diagnoses_names, diagnoses_link)
			generateLink('diagnose2', diagnoses_names, diagnoses_link)
			generateLink('diagnose3', diagnoses_names, diagnoses_link)
			generateLink('diagnose4', diagnoses_names, diagnoses_link)
			generateLink('diagnose5', diagnoses_names, diagnoses_link)

			let elementErw = document.getElementById("erw-root");
            elementErw.classList.add("hide-diagnoses");

			let elementErwTitle = document.getElementById("diagnoses-box-title");
			elementErwTitle.classList.add("hide-diagnoses");
			
		}
	});

	if(targetNode){
		observer.observe(targetNode, { attributes: true, childList: true });
	}

	function backToWidget() {
		let elementErw = document.getElementById("erw-root");
        elementErw.classList.remove("hide-diagnoses");

		let elementErwTitle = document.getElementById("diagnoses-box-title");
		elementErwTitle.classList.remove("hide-diagnoses");

		let elementDiagnoses = document.getElementById("diagnoses");
		elementDiagnoses.classList.add("hide-diagnoses");
	}

</script>