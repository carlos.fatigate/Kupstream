<?php
    $text_disclaimer_banner = get_field('text_disclaimer_banner');
    $background_color_disclaimer_banner = get_field('background_color_disclaimer_banner');
?>
<div id="disclaimer-banner" style="background-color: <?= $background_color_disclaimer_banner;?>">
    <div class="container">
        <div class="text_disclaimer_banner">
            <?= $text_disclaimer_banner;?>
        </div>
    </div>
</div>