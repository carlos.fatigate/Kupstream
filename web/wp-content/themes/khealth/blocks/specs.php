
<div id="specs-block" class="section-specs bg-<?php the_field("background") ?>">
  <div class="container">
    <h2 class="section-head">
      <?php the_field("title") ?>
    </h2>
    <div class="specs-list">
      <ul class="row">
        <?php if( have_rows('list') ): while( have_rows('list') ): the_row(); ?>
          <?php $link = get_sub_field('link'); if( $link ):

          $image = wp_get_attachment_image_src( get_sub_field('icon'), "feature" );
          $imageR = wp_get_attachment_image_src( get_sub_field('icon'), "feature@2x" );
          ?>
          <li class="col-md-6">
              <?php if ($image) {
                ?>
            <a <?php echo $link['url'] == "#" ? '' : 'href="'.$link['url'].'"'; ?> <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?>>
              <img data-src="<?php echo $image[0]; ?>" data-srcset="<?php echo $imageR[0]; ?> 2x">
              <?php echo $link['title']; ?>
            </a>
                <?php
              } else {
                ?>
          <div class="sitem">
              <?php echo $link['title']; ?>
            </div>
                <?php
              } ?>

          </li>
          <?php endif; ?>
        <?php endwhile; endif; ?>
      </ul>
    <?php if (get_field("more_text")) {
      ?>
      <h2 class="more">
        <?php the_field("more_text") ?>
      </h2>
      <?php
    }  ?>
    </div>
  </div>
</div>