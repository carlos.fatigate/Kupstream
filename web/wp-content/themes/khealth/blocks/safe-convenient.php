<?php
    $image_safe_convenient = get_field('image_safe_convenient');
    $image_safe_convenient_mobile = get_field('image_safe_convenient_mobile');
    $title_safe_convenient = get_field('title_safe_convenient');
    $smal_text_safe_convenient = get_field('smal_text_safe_convenient');
    $content_safe_convenient = get_field('content_safe_convenient');
    $cta_text_safe_convenient = get_field('cta_text_safe_convenient');
    $cta_link_safe_convenient = get_field('cta_link_safe_convenient');
    $background_color_safe_convenient = get_field('background_color_safe_convenient');
    $background_color_safe_convenient_mobile = get_field('background_color_safe_convenient_mobile');
    $full_image_safe_convenient = get_field('full_image_safe_convenient');

    if($background_color_safe_convenient_mobile) {
        $background_mobile = $background_color_safe_convenient_mobile;
    } else {
        $background_mobile = $background_color_safe_convenient;
    }

    if($image_safe_convenient_mobile) {
        $mobile_image = $image_safe_convenient_mobile;
    } else {
        $mobile_image = $image_safe_convenient;
    }
?>
<div id="safe-convenient">
    <?php if($image_safe_convenient) { ?>
        <div class="hide-mobile image_safe_convenient <?php if($full_image_safe_convenient) { echo 'full_image_safe_convenient'; }?>" style="background-image:url('<?= $image_safe_convenient; ?>')"> </div>
    <?php } ?>
    <?php if($image_safe_convenient) { ?>
        <div class="hide-desktop image_safe_convenient <?php if($full_image_safe_convenient) { echo 'full_image_safe_convenient'; }?>" style="background-image:url('<?= $mobile_image; ?>')"> </div>
    <?php } ?>
    <div class="image_safe_convenient_box">
        <div>
            <?php if($smal_text_safe_convenient) { ?>
                <p class="smal_text_safe_convenient"><?= $smal_text_safe_convenient; ?></p>
            <?php } ?>
            <h2 class="title_safe_convenient"><?= $title_safe_convenient ?></h2>
            <?php if($content_safe_convenient) { ?>
                <div class="content_safe_convenient_box row">
                    <?php foreach ($content_safe_convenient as $key => $content) {?>
                        <div class="col-md-6 content_safe_convenient">
                            <div class="circle" style="background-color: <?= $content['circle_color']; ?>"></div>
                            <p class="content_safe_convenient_title"><?= $content['title']; ?></p>
                            <p class="content_safe_convenient_description"><?= $content['description']; ?></p>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if($cta_text_safe_convenient && $cta_link_safe_convenient) { ?>
                <a class="content_safe_convenient_link" href="<?= $cta_link_safe_convenient; ?>"><?= $cta_text_safe_convenient; ?></a>
            <?php } ?>
        </div>
    </div>
</div>

<style>
    #safe-convenient {
        background-color: <?= $background_mobile ?>; 
    }

    @media screen and (min-width: 981px) {
        #safe-convenient {
            background-color: <?= $background_color_safe_convenient ?>; 
        }
    }
</style>