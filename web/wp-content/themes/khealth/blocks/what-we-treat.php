<?php
    $image_background_what_we_treat = get_field('image_background_what_we_treat');
    $title_what_we_treat = get_field('title_what_we_treat');
    $small_text_what_we_treat = get_field('small_text_what_we_treat');
    $treatments_what_we_treat = get_field('treatments_what_we_treat');
    $text_what_we_treat = get_field('text_what_we_treat');
    $cta_text_what_we_treat = get_field('cta_text_what_we_treat');
    $cta_link_what_we_treat = get_field('cta_link_what_we_treat');
    $text_below_cta_what_we_treat = get_field('text_below_cta_what_we_treat');
?>
<div id="what-we-treat" style="background-image:url('<?= $image_background_what_we_treat; ?>')">
    <div class="container">
        <p class="small_text_what_we_treat"><?= $small_text_what_we_treat; ?></p>
        <h2 class="title_what_we_treat"><?= $title_what_we_treat; ?></h2>
        <?php if($treatments_what_we_treat) { ?>
            <div class="row">
                <?php foreach ($treatments_what_we_treat as $key => $treat) { ?>
                    <div class="col-md-6">
                        <?php if($treat['link']) { ?>
                            <a class="treat_link" href="<?= $treat['link']?>">
                        <?php } ?>
                            <div class="treat_link_box">
                                <div class="treat_link_info">
                                    <?php if($treat['image']) { ?>
                                        <img class="treat_image" src="<?= $treat['image']?>"/>
                                    <?php } ?>
                                    <p class="treat_text"><?= $treat['text'] ?></p>
                                </div>
                                <svg width="8" height="12" viewBox="0 0 8 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M0.910582 0.410704C1.23602 0.0852667 1.76366 0.0852667 2.08909 0.410704L7.08909 5.4107C7.41453 5.73614 7.41453 6.26378 7.08909 6.58921L2.08909 11.5892C1.76366 11.9147 1.23602 11.9147 0.910582 11.5892C0.585145 11.2638 0.585145 10.7361 0.910582 10.4107L5.32133 5.99996L0.910582 1.58921C0.585145 1.26378 0.585145 0.736141 0.910582 0.410704Z" fill="#003355"/>
                                </svg>
                            </div>
                        <?php if($treat['link']) { ?>
                            </a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <div>
            <p class="text_what_we_treat"><?= $text_what_we_treat?></p>
        </div>
        <a class="btn btn-primary cta_link_what_we_treat" href="<?= $cta_link_what_we_treat?>"><?= $cta_text_what_we_treat?></a>
        <?php if($text_below_cta_what_we_treat) { ?>
            <p class="text_below_cta_what_we_treat"><?= $text_below_cta_what_we_treat ?></p>
        <?php } ?>
    </div>
</div>

<style>
    #what-we-treat {
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>