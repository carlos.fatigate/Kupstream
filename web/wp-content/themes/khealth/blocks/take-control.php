<?php
    $title_take_control = get_field('title_take_control');
    $itens_take_control = get_field('itens_take_control');
    $web_forum_title_take_control = get_field('web_forum_title_take_control');
    $app_store_image_take_control = get_field('app_store_image_take_control');
    $app_store_link_take_control = get_field('app_store_link_take_control');
    $google_play_image_take_control = get_field('google_play_image_take_control');
    $google_play_link_take_control = get_field('google_play_link_take_control');
    $download_app_text_take_control = get_field('download_app_text_take_control');
    $background_image_take_control = get_field('background_image_take_control');
?>
<div id="take-control" >
    <div id="take-control-bg" style="background-image:url('<?= $background_image_take_control; ?>')">
        <div class="container">
            <h2><?= $title_take_control ?><div class="circle"></div></h2>
            <?php if($itens_take_control) { ?>
                <div class="itens_take_control_container">
                    <?php foreach ($itens_take_control as $key => $item) { ?>
                        <div class="itens_take_control <?php if($key % 2 != 0) echo 'itens_take_control_reverse'; ?>">
                            <div class="itens_take_control_image_box">
                                <img class="itens_take_control_img <?php if($item['round_image']) echo 'itens_take_control_img_round'; ?>" src="<?= $item['image'] ?>"/>
                                <?php if($item['chart']) { ?>
                                    <div class="itens_take_control_chart_box">
                                        <?php foreach ($item['chart'] as $key => $chart) { ?>
                                            <div class="itens_take_control_chart">
                                                <p class="itens_take_control_chart_number" style="color: <?= $chart['number_color']?>"><?= $chart['number']; ?>%</p>
                                                <div>
                                                    <p class="itens_take_control_chart_title"><?= $chart['title']; ?></p>
                                                    <p class="itens_take_control_chart_description"><?= $chart['description']; ?></p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="itens_take_control_text_box">
                                <p class="itens_take_control_title"><?= $item['title'] ?></p>
                                <p class="itens_take_control_description"><?= $item['description'] ?></p>
                                <a class="itens_take_control_link" href="<?= $item['cta_link'] ?>"><?= $item['cta_text'] ?></a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
            <div class="take-control-web-forum-box">
                <div class="take-control-web-forum">
                    <h3><?= $web_forum_title_take_control; ?><div class="circle"></div></h3>
                    <div>
                        <p class="download_app_text_take_control"><?= $download_app_text_take_control; ?></p>
                        <a href="<?= $app_store_link_take_control; ?>">
                            <img class="store_image_take_control" src="<?= $app_store_image_take_control; ?>" />
                        </a>
                        <a href="<?= $google_play_link_take_control; ?>">
                            <img class="store_image_take_control" src="<?= $google_play_image_take_control; ?>" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    #take-control-bg {
        background-size: cover;
        background-position: center 10%;
        background-repeat: no-repeat;
    }
</style>