<?php 
    $title_doctor_dots = get_field('title_doctor_dots');
    $logos_doctor_dots = get_field('logos_doctor_dots');
    $itens_doctor_dots = get_field('itens_doctor_dots');
    $doctor_dots = get_field('doctor_dots');
    $cta_text_doctor_dots = get_field('cta_text_doctor_dots');
    $cta_link_doctor_dots = get_field('cta_link_doctor_dots');
?>
<div id="doctor-dots">
    <div class="container">
        <h2 class="title_doctor_dots"><?= $title_doctor_dots; ?></h2>
        <?php if($logos_doctor_dots) { ?>
            <div class="logos_doctor_dots">
                <?php foreach ($logos_doctor_dots as $key => $logo) { ?>
                    <div class="logos_doctor_dots_image">
                        <img src="<?= $logo['image']; ?>" />
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if($itens_doctor_dots) { ?>
            <div class="itens_doctor_dots">
                <?php foreach ($itens_doctor_dots as $key => $item) { ?>
                    <div class="itens_doctor_dots_block">
                        <img src="<?= $item['image']; ?>" />
                        <p><?= $item['text']; ?></p>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if($doctor_dots) {  
            $num_doctors = [1, 2, 3, 2, 3, 2];
            $positions_doctors = [[7], [2,10], [0,5,8], [4,9], [1,6,11], [3,8]];
            $positions_width = [ [16,16,16,16,16,16,16,16,16,16,16,16,16],
                                  [7,16,16,16,16,16,7,16,16,16,16,16,7],
                                  [16,16,22,16,11,16,16,16,16,16,11,16,16],
                                  [16,28,16,16,16,16,16,28,16,16,16,16,16],
                                  [16,16,16,16,16,16,16,16,16,16,16,16,16],
                                  [16,16,16,16,16,16,16,16,16,28,16,16,16]
                                ];
            $positions_colors = [ [1,2,1,3,1,4,1,0,1,3,1,4,1],
                                  [8,1,0,1,2,7,8,1,1,1,0,7,8],
                                  [0,5,8,6,1,0,5,5,0,6,1,1,5],
                                  [4,7,1,2,0,8,4,7,1,0,5,8,4],
                                  [5,0,7,1,7,1,0,2,7,1,7,0,5],
                                  [2,5,4,0,1,8,2,5,0,5,1,8,2]
                                ];
        ?>
            <div class="row a-center">
                <div class="col-md-3">
                    <div class="doctor-dots-big-img-box">
                        <div class="doctor-dots-big-img" id="doctor-dots-big-img"></div>
                        <div id="doctor-dots-big-img-text-box"></div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="doctor_dots_scroll">
                        <?php $first = true;
                        $global_arry_key = 0; ?>
                        <?php for ($i = 0; $i < 6; $i++) {
                                $k = 0;
                            ?>
                            <div class="doctor_dots">
                                <?php for ($j = 0; $j < 13; $j++) { 
                                    $size_doctors_rows = sizeof($positions_doctors[$i]);
                                        if(isset($positions_doctors[$i][$k]) && $positions_doctors[$i][$k] == $j) {
                                            $doctor = $doctor_dots[$global_arry_key];
                                            $doctor_img = wp_get_attachment_image_src( get_post_thumbnail_id( $doctor ), 'full' );
                                            if($doctor_img) {
                                                $doctor_img = $doctor_img[0];
                                            } else {
                                                $doctor_img = get_field("doctor_biography_image_profile",$doctor);
                                            }
                                            $doctor_name = get_the_title($doctor);
                                            $doctor_role = get_field("role", $doctor);
                                        ?>
                                            <div id="<?php if($first) { echo 'first-doctor-dots'; }?>" class="doctor-dots-small-img" onclick="showDoctor('<?= $doctor_name; ?>', '<?= $doctor_img; ?>', '<?= $doctor_role; ?>')"> 
                                                <img src="<?= $doctor_img; ?>" />
                                            </div>
                                        <?php if($first) {$first = false;} 
                                        $k++; 
                                        unset($doctor_dots[$global_arry_key]);
                                        $global_arry_key++;
                                    ?>  
                                    <?php } else { 
                                            $dot_size = 6;
                                            ?>
                                            <div class="circle-box">
                                                <div class="circle color-<?= $positions_colors[$i][$j]; ?>" style="width: <?= $positions_width[$i][$j]?>px; height: <?= $positions_width[$i][$j]?>px"></div>
                                            </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if($cta_text_doctor_dots && $cta_link_doctor_dots) { ?>
                <a class="cta_doctor_dots" href="<?= $cta_link_doctor_dots; ?>"><?= $cta_text_doctor_dots; ?></a>
            <?php } ?>
        <?php } ?>
    </div>
</div>