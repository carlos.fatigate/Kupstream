<?php
    $hero_parallax_title = get_field("hero_parallax_title");
    $hero_parallax_subtitle = get_field("hero_parallax_subtitle");
    $hero_parallax_cta_text = get_field("hero_parallax_cta_text");
    $hero_parallax_cta_link = get_field("hero_parallax_cta_link");
    $hero_parallax_cta_text_color = get_field("hero_parallax_cta_text_color");
    $hero_parallax_cta_background = get_field("hero_parallax_cta_background");
    $hero_parallax_cta_background_hover = get_field("hero_parallax_cta_background_hover");
    $hero_parallax_cta_link_new_tab = get_field("hero_parallax_cta_link_new_tab");
    $hero_parallax_content = get_field("hero_parallax_content");
    $hero_parallax_logos = get_field("hero_parallax_logos");
    $hero_image = get_field("hero_image");  
    $hero_image_mobile = get_field("hero_image_mobile");  
    $hero_parallax_image = get_field("hero_parallax_image");
    $hero_parallax_image_mobile = get_field("hero_parallax_image_mobile");
    $hero_parallax_image_mobile_iphone = get_field("hero_parallax_image_mobile_iphone");
    $hero_video = get_field("hero_video");

    $hero_parallax_show_second_cta = get_field("hero_parallax_show_second_cta");
    $hero_parallax_second_cta_text = get_field("hero_parallax_second_cta_text");
    $hero_parallax_second_cta_link = get_field("hero_parallax_second_cta_link");
    $hero_parallax_second_cta_text_color = get_field("hero_parallax_second_cta_text_color");
    $hero_parallax_second_cta_text_color_hover = get_field("hero_parallax_second_cta_text_color_hover");
    $hero_parallax_second_cta_background = get_field("hero_parallax_second_cta_background");
    $hero_parallax_second_cta_background_hover = get_field("hero_parallax_second_cta_background_hover");
    $hero_parallax_second_cta_border_color = get_field("hero_parallax_second_cta_border_color");
    $hero_parallax_second_cta_border_color_hover = get_field("hero_parallax_second_cta_border_color_hover");
    $hero_parallax_second_cta_link_new_tab = get_field("hero_parallax_second_cta_link_new_tab");

    $hero_parallax_two_colors = get_field("hero_parallax_two_colors");
    $hero_parallax_background_color = get_field("hero_parallax_background_color");
    $hero_parallax_show_legitscript_logo = get_field("hero_parallax_show_legitscript_logo");

    $hero_parallax_image_leaning_on_the_bottom = get_field("hero_parallax_image_leaning_on_the_bottom");

    $hero_parallax_phone_form = get_field("hero_parallax_phone_form");
    $hero_parallax_cta_text = get_field("hero_parallax_cta_text");
    $hero_parallax_cta_link = get_field("hero_parallax_cta_link");

    $hero_type = '';
    if(strcmp($hero_parallax_background_color, '#00406b') == 0) {
        $hero_type = 'hero-parallax-dark';
    }
    $style_hero_parallax = "style = ";
    $btn_background_hero_parallax = "#8ef3d0";
    
    if ($hero_parallax_cta_background) { 
        $style_hero_parallax .= 'background-color:'.$hero_parallax_cta_background.';'; 
        $btn_background_hero_parallax = $hero_parallax_cta_background;
    }
    if ($hero_parallax_cta_text_color) { 
        $style_hero_parallax .= 'color:'.$hero_parallax_cta_text_color.';'; 
    }

    $mouse_over_hero_parallax = "' '";
    $mouse_out_hero_parallax = "' '";

    if ($hero_parallax_cta_background_hover) {
        $mouse_over_hero_parallax = 'this.style.background="'.$hero_parallax_cta_background_hover.'"';
        $mouse_out_hero_parallax = 'this.style.background="'.$btn_background_hero_parallax.'"';
    }

    if(!$hero_parallax_image_mobile){
        $hero_parallax_image_mobile = $hero_parallax_image;
    }

    if($hero_parallax_show_second_cta) {
        $style_hero_parallax .= "min-width:331px;";
        $style_second_cta_hero_parallax = "style=min-width:331px;";
        $second_btn_background_average_monthly = "#8ef3d0";
    
        if ($hero_parallax_second_cta_background) { 
            $style_second_cta_hero_parallax .= 'background-color:'.$hero_parallax_second_cta_background.';'; 
            $second_btn_background_average_monthly = $hero_parallax_second_cta_background;
        }

        if ($hero_parallax_second_cta_text_color) { 
            $style_second_cta_hero_parallax .= 'color:'.$hero_parallax_second_cta_text_color.';'; 
        }

        if($hero_parallax_second_cta_border_color) {
            $border = "border-style:solid;";
            $border .= "border-color:".$hero_parallax_second_cta_border_color.';'; 
            $border .= "border-width:2px;";
            $style_second_cta_hero_parallax .= $border; 
        }
    
        $style_second_cta_hero_parallax .= "' '";
        $second_mouse_over_average_monthly = "";
        $second_mouse_out_average_monthly = "";
    
        if ($hero_parallax_second_cta_background_hover) {
            $second_mouse_over_average_monthly .= "elem.style.background='$hero_parallax_second_cta_background_hover'";
            $second_mouse_out_average_monthly .= "elem.style.background='$hero_parallax_second_cta_background'";
        }

        if ($hero_parallax_second_cta_text_color_hover) {
            $second_mouse_over_average_monthly .= ";elem.style.color='$hero_parallax_second_cta_text_color_hover'";
            $second_mouse_out_average_monthly .= ";elem.style.color='$hero_parallax_second_cta_text_color'";
        }

        if ($hero_parallax_second_cta_border_color_hover) {
            $second_mouse_over_average_monthly .= ";elem.style.borderColor='$hero_parallax_second_cta_border_color_hover'";
            $second_mouse_out_average_monthly .= ";elem.style.borderColor='$hero_parallax_second_cta_border_color'";
        }

    }

    $style_hero_parallax .= "' '";


$search = 'Iphone';
$user_agent = $_SERVER['HTTP_USER_AGENT'];
$is_iphone = false;
if(preg_match("/{$search}/i", $user_agent)) {
    $is_iphone = true;
    if($hero_parallax_image_mobile_iphone){
        $hero_parallax_image_mobile = $hero_parallax_image_mobile_iphone;
    } else {
        $hero_parallax_image_mobile = $hero_parallax_image;
    }
}

if ( is_front_page() ) :
    $padding = "style='padding-top:35px;'";
    $padding_hero = "style='padding-bottom:0px;'";
else :
    $padding = "style='padding-top:50px'";
    $padding_hero = "";
endif;

if ($hero_parallax_image_leaning_on_the_bottom && !is_front_page()) {
    $padding_hero = "style='padding-bottom:0px;'";
}

?>

<script>
    function mouseOver (elem){
        <?= $second_mouse_over_average_monthly ?>
    }

    function mouseOut (elem){
        <?= $second_mouse_out_average_monthly ?>
    }
</script>

<div class="section-hero section-hero-parallax <?php if($hero_parallax_two_colors) { echo 'hero_parallax_two_colors'; }?>" id="hero-parallax" style="background-color:<?= $hero_parallax_background_color; ?>">
    <div class="parallax parallax-background <?php if($is_iphone) { echo "parallax-background-iphone"; }?> <?= $hero_type ?>" <?= $padding; ?>>
        <div class="container">
            <div class="row a-center">
                <div class="col-md-6" style="padding-bottom: 40px;">
                    <div class="hero-parallax-title">
                        <?= $hero_parallax_title; ?>
                    </div>
                    <div class="hero-parallax-subtitle">
                        <?= $hero_parallax_subtitle; ?>
                    </div>
                    <?php if(get_field('hero_parallax_show_search')) { ?>
                        <div class="search-drugs-box">
                            <div class="search-drugs">
                                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.25 3C5.3505 3 3 5.3505 3 8.25C3 11.1495 5.3505 13.5 8.25 13.5C11.1495 13.5 13.5 11.1495 13.5 8.25C13.5 5.3505 11.1495 3 8.25 3ZM1.5 8.25C1.5 4.52208 4.52208 1.5 8.25 1.5C11.9779 1.5 15 4.52208 15 8.25C15 11.9779 11.9779 15 8.25 15C4.52208 15 1.5 11.9779 1.5 8.25Z" fill="#00406B"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M11.957 11.9572C12.2499 11.6643 12.7247 11.6643 13.0176 11.9572L16.2801 15.2197C16.573 15.5126 16.573 15.9875 16.2801 16.2804C15.9872 16.5733 15.5124 16.5733 15.2195 16.2804L11.957 13.0179C11.6641 12.725 11.6641 12.2501 11.957 11.9572Z" fill="#00406B"/>
                                </svg>
                                <input type="text" name="search" id="search" placeholder="Search for your medication"/>
                                <a class="close-search" onclick="closeSearch()">
                                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M14 1.4L12.6 0L7 5.6L1.4 0L0 1.4L5.6 7L0 12.6L1.4 14L7 8.4L12.6 14L14 12.6L8.4 7L14 1.4Z" fill="#00406B"/>
                                    </svg>
                                </a>
                            </div>
                            <div class="search-drugs-response"></div>
                        </div>
                    <?php } ?>
                    <?php if($hero_parallax_cta_text || $hero_parallax_show_second_cta) { ?>
                        <div class="hero-parallax-cta">
                            <?php if($hero_parallax_cta_text) { ?>
                                <a href="<?= $hero_parallax_cta_link; ?>" class="btn btn-primary" 
                                    <?php if ($hero_parallax_cta_link_new_tab) { echo 'target="_blank"'; } ?> 
                                    <?php if(isset($style_hero_parallax)) { echo $style_hero_parallax; }; ?> 
                                    onMouseOver = <?php if(isset($hero_parallax_cta_background_hover)) { echo $mouse_over_hero_parallax; }; ?>
                                    onMouseOut = <?php if(isset($hero_parallax_cta_background_hover)) { echo $mouse_out_hero_parallax; }; ?>
                                >
                                    <?= $hero_parallax_cta_text; ?>
                                </a>
                            <?php } ?>
                            <?php if($hero_parallax_show_second_cta) { ?>
                                <a href="<?= $hero_parallax_second_cta_link; ?>" class="hero-parallax-cta-pharmacy btn btn-primary" 
                                    <?php if ($hero_parallax_second_cta_link_new_tab) { echo 'target="_blank"'; } ?> 
                                    <?php if(isset($style_second_cta_hero_parallax)) { echo $style_second_cta_hero_parallax; }; ?> 
                                    onmouseover = "mouseOver(this)"
                                    onmouseout = "mouseOut(this)"
                                >
                                    <?= $hero_parallax_second_cta_text; ?>
                                </a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="hero-parallax-content">
                        <?= $hero_parallax_content; ?>
                    </div>
                    <?php if($hero_parallax_show_legitscript_logo) { ?>
                        <div class="accreditation_logos_grid accreditation_logos_grid_background accreditation-logos-parallax">
                            <script src="https://static.legitscript.com/seals/9571275.js"></script>
                        </div>
                    <?php } ?>
                    <?php if($hero_parallax_phone_form) { ?>
                        <div>
                            <select>
                                <option>Text</option>
                            </select>
                            <input type="tel" />
                        </div>
                    <?php } ?>
                </div>
                <div class="col-md-6 a-s-e hero-parallax-attachment-full" <?= $padding_hero; ?>>
                    <?php if($hero_video) { ?>
                        <div class="hero_video hide-mobile">
                            <video loop autoplay muted height="100%" width="100%">
                            <source src="<?= $hero_video; ?>" type="video/mp4" />
                            </video>
                        </div>
                    <?php } else { ?>
                        <?php if ($hero_image) { ?>
                            <img class="attachment-full size-full hide-mobile" src="<?= $hero_image; ?>" />
                        <?php } ?>
                    <?php } ?>
                    <?php if ($hero_image_mobile) { ?>
                        <img class="attachment-full size-full hide-desktop" src="<?= $hero_image_mobile; ?>" />
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .parallax-background {
        background-image:url('<?= $hero_parallax_image_mobile; ?>')
    }

    .parallax-background-iphone {
        background-image:url('<?= $hero_parallax_image_mobile; ?>')
    }

    @media screen and (min-width: 981px) {
        .parallax-background {
            background-image:url('<?= $hero_parallax_image; ?>')
        }
    }
</style>