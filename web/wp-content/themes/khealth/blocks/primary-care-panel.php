<div id="primary-care-panel">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6">
                <div class="primary-care-info">
                    <h3><?= get_field('left_card_title_primary_care'); ?></h3>
                    <div class="primary-care-subtitle">
                        <?= get_field('left_card_subtitle_primary_care'); ?>
                    </div>
                    <div class="primary-care-description">
                        <?= get_field('left_card_description_primary_care'); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="primary-care-block">
                    <h3><?= get_field('right_card_title_primary_care'); ?></h3>
                    <?php if(get_field('right_card_tesxt_lines_primary_care') && sizeof(get_field('right_card_tesxt_lines_primary_care')) > 0) { ?>
                        <?php foreach (get_field('right_card_tesxt_lines_primary_care') as $line) { ?>
                            <div class="primary-care-check">
                                <img class="primary-care-check-image" src="<?= get_field("primary_care_panel_check","options"); ?>">
                                <p><?= $line['text']; ?></p>
                            </div>
                        <?php } ?>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>