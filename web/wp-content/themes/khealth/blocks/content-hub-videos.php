<?php 
$video1 = get_field('video1');
$video2 = get_field('video2');
$video3 = get_field('video3');
$video1_id = explode("/", $video1['video_url'])[4];
$video2_id = explode("/", $video2['video_url'])[4];
$video3_id = explode("/", $video3['video_url'])[4];
$background_color = get_field('background_color');
$reduce_top_padding_checkbox = get_field('reduce_top_padding');
$top_padding = "";

if($reduce_top_padding_checkbox) {
    $top_padding = '25px';
}
      
?>

<div id="content-hub-videos-cont" style="background-color: <?php echo esc_attr($background_color); ?>; padding-top: <?= esc_attr($top_padding); ?> ;">
    <div class="main-video">
        <div class="main-video__video">
            <iframe 
            class="main-video__iframe"
            src="<?php echo esc_attr( $video1['video_url'] ); ?>" 
            title="YouTube video player" 
            frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
            allowfullscreen></iframe>
        </div>
        <p>
            <a class="main-video_doctorUrl" href="<?php echo esc_attr( $video1['doctor_name'][0]->guid ); ?>">
                <span class="main-video_doctorName"><?php echo esc_attr( $video1['doctor_name'][0]->post_title ); ?></a>
            </span> &nbsp;&nbsp;|&nbsp;&nbsp; 
            <span class="main-video_date"><?php echo esc_attr( $video1['date'] ); ?></span></p>
        <p><?php echo esc_attr( $video1['video_blurb'] ); ?></p>
        <a href="https://www.youtube.com/c/KHealthinc">SEE ALL VIDEOS</a>
        <!-- hide via CSS, render so info accessible when videos get switched -->
        <span id="main-video-details-template" class="hide-video-details">
            <p><?php echo esc_attr( $video1['video_url'] ); ?></p>
            <p><?php echo esc_attr( $video1['date'] ); ?></p>
            <p><?php echo esc_attr( $video1['doctor_name'][0]->post_title ); ?></p>
            <p><?php echo esc_attr( $video1['doctor_name'][0]->guid ); ?></p>
            <p><?php echo esc_attr( $video1['video_blurb'] ); ?></p>
            <p><?php echo esc_attr( $video1['title'] ); ?></p>
            <p><?php echo esc_attr( $video1['date_time'] ); ?></p>
            <p><?php echo esc_attr( $video1['image_alt'] ); ?></p>
        </span>
        <!--  ------------------------------------------------------- -->
    </div>
    <div class="videos-column">
        <div class="videos-column--active">
            <div>
                <img src="<?php echo esc_attr('https://i.ytimg.com/vi/' . $video1_id . '/maxresdefault.jpg'); ?>" alt="<?php echo esc_attr($video1['image_alt']); ?>" >
            </div>
            <div> 
                <p><?php echo esc_attr( $video1['title'] ); ?></p>    
                <p><?php echo esc_attr( $video1['date_time'] ); ?></p>       
            </div>
            <!-- hide via CSS, render so info accessible when videos get switched -->
            <span id="video1-details-template" class="hide-video-details">
                <p><?php echo esc_attr( $video1['video_url'] ); ?></p>
                <p><?php echo esc_attr( $video1['date'] ); ?></p>
                <p><?php echo esc_attr( $video1['doctor_name'][0]->post_title ); ?></p>
                <p><?php echo esc_attr( $video1['doctor_name'][0]->guid ); ?></p>
                <p><?php echo esc_attr( $video1['video_blurb'] ); ?></p>
                <p><?php echo esc_attr( $video1['title'] ); ?></p>
                <p><?php echo esc_attr( $video1['date_time'] ); ?></p>
                <p><?php echo esc_attr( $video1['image_alt'] ); ?></p>
            <span>
            <!--  ------------------------------------------------------- -->
        </div>
        <div>
            <div>
                <img src="<?php echo esc_attr('https://i.ytimg.com/vi/' . $video2_id . '/maxresdefault.jpg'); ?>" alt="<?php echo esc_attr($video2['image_alt']); ?>" >
            </div>
            <div> 
                <p><?php echo esc_attr( $video2['title'] ); ?></p>    
                <p><?php echo esc_attr( $video2['date_time'] ); ?></p>       
            </div>
            <!-- hide via CSS, render so info accessible when videos get switched -->
            <span id="video2-details-template" class="hide-video-details">
                <p><?php echo esc_attr( $video2['video_url'] ); ?></p>
                <p><?php echo esc_attr( $video2['date'] ); ?></p>
                <p><?php echo esc_attr( $video2['doctor_name'][0]->post_title ); ?></p>
                <p><?php echo esc_attr( $video2['doctor_name'][0]->guid ); ?></p>
                <p><?php echo esc_attr( $video2['video_blurb'] ); ?></p>
                <p><?php echo esc_attr( $video2['title'] ); ?></p>
                <p><?php echo esc_attr( $video2['date_time'] ); ?></p>
                <p><?php echo esc_attr( $video2['image_alt'] ); ?></p>
            <span>
            <!--  ------------------------------------------------------- -->
        </div>
        <div>
            <div>
                <img src="<?php echo esc_attr('https://i.ytimg.com/vi/' . $video3_id . '/maxresdefault.jpg'); ?>" alt="<?php echo esc_attr($video3['image_alt']); ?>" >
            </div>
            <div> 
                <p><?php echo esc_attr( $video3['title'] ); ?></p>    
                <p><?php echo esc_attr( $video3['date_time'] ); ?></p>       
            </div>
            <!-- hide via CSS, render so info accessible when videos get switched -->
            <span id="video3-details-template" class="hide-video-details">
                <p><?php echo esc_attr( $video3['video_url'] ); ?></p>
                <p><?php echo esc_attr( $video3['date'] ); ?></p>
                <p><?php echo esc_attr( $video3['doctor_name'][0]->post_title ); ?></p>
                <p><?php echo esc_attr( $video3['doctor_name'][0]->guid ); ?></p>
                <p><?php echo esc_attr( $video3['video_blurb'] ); ?></p>
                <p><?php echo esc_attr( $video3['title'] ); ?></p>
                <p><?php echo esc_attr( $video3['date_time'] ); ?></p>
                <p><?php echo esc_attr( $video3['image_alt'] ); ?></p>
            <span>
            <!--  ------------------------------------------------------- -->
        </div>
        <div  id="all-videos-link"">
            <a href="https://www.youtube.com/c/KHealthinc" target="_blank">SEE ALL VIDEOS</span></a> 
        </div>    
    </div>
</div>

<?php ?>





