<div id="btn-full" style="background-color: <?= get_field('background_color_cta_full'); ?>">
    <?php 
        $is_drug_page = get_field('is_drug_page_cta_full'); 
        if($is_drug_page) { ?>
            <div class="container">
                <div class="row">
                    <div class="col-12">
    <?php } ?>
        <div class="btn-full-box">
            <p class="subtitle-cta-full" style="color: <?= get_field('subtitle_color_cta_full'); ?>"><?= get_field('subtitle_cta_full'); ?></p>
            <h2 class="title-cta-full" style="color: <?= get_field('title_color_cta_full'); ?>"><?= get_field('title_cta_full'); ?></h2>
            <a class="btn btn-primary link_cta_full" href="<?php 
									$url = get_field("link_cta_full");
									$url = str_replace("[acf field='", "", $url);
									$url = str_replace("']", "", $url);
									print_r(get_field($url, "option"));
								?>" style="color: <?= get_field('link_text_color_cta_full'); ?>">
                <?= get_field('link_text_cta_full'); ?>
            </a>
        </div>          
    <?php 
        if($is_drug_page) { ?>
                    </div>
                </div>
            </div>
    <?php } ?>
</div>