<?php $doctors_list = get_field('doctors_list_block'); 
    $doctors_list_size = sizeof($doctors_list); 
    $for_base = $doctors_list_size >= 4 ? 4 : $doctors_list_size?>
<div id="doctors-list-block" class="section-text bg-white">
    <div class="container">
        <?php if (get_field("title_doctors_list")) { ?>
            <h2 class="section-head">
                <?php the_field("title_doctors_list") ?>
            </h2>
        <?php } ?>
        <?php if (get_field("description_doctors_list")) { ?>
            <div class="sub-entry">
                <?php the_field("description_doctors_list") ?>
            </div>
        <?php } ?>
        <?php if(get_field('block_type_carousel')) { ?>
            <div class="slider-3-items row gutters-30">
                <?php foreach ($doctors_list as $doctor) {
                    $id = $doctor->ID;
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials' );
                    $imageR = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials@2x' );?>
                    <div class="col-sm-6 col-md-4 slide-item">
                        <a href="<?= esc_url(get_permalink($id)); ?>"  class="doctor-item featured">
                            <div>
                                <div class="image">
                                    <img data-src="<?= $image[0]; ?>" data-srcset="<?= $imageR[0]; ?> 2x" alt="">
                                </div>
                                <h4><?= $doctor->post_title ?></h4>
                                <?php if (get_field("role", $id)) { ?>
                                    <div class="role">
                                        <?= get_field("role", $id) ?>
                                    </div>
                                <?php } ?>
                                <div>
                                    <?= get_field("description", $id); ?>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php }?>
            </div>
        <?php } else { ?>
            <div class="doctors-list row gutters-30 doctors-list-block">
                <?php for ($i=0; $i < $for_base; $i++) {
                    $id = $doctors_list[$i]->ID;
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials' );
                    $imageR = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials@2x' );?>
                    <div class="col-md-6">
                        <a href="<?= esc_url(get_permalink($id)); ?>"  class="doctor-item featured">
                            <div class="image">
                                <img data-src="<?= $image[0]; ?>" data-srcset="<?= $imageR[0]; ?> 2x" alt="">
                            </div>
                            <h4><?= $doctors_list[$i]->post_title ?></h4>
                            <?php if (get_field("role", $id)) { ?>
                                <div class="role">
                                    <?= get_field("role", $id) ?>
                                </div>
                            <?php } ?>
                            <?= get_field("description", $id); ?>
                        </a>
                    </div>
                <?php }?>
            </div>
            <?php if ($doctors_list_size > 4) { ?>
                <div class="text-more js-more">
                    <?php _e("View more", "theme") ?>
                </div>
                <div class="doctors-list row gutters-30" style="display:none">
                    <?php for ($i=$for_base; $i < $doctors_list_size; $i++) {
                        $id = $doctors_list[$i]->ID;
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials' );
                        $imageR = wp_get_attachment_image_src( get_post_thumbnail_id($id), 'testimonials@2x' );?>
                        <div class="col-md-6">
                            <a href="<?= esc_url(get_permalink($id)); ?>"  class="doctor-item featured">
                                <div class="image">
                                    <img data-src="<?= $image[0]; ?>" data-srcset="<?= $imageR[0]; ?> 2x" alt="">
                                </div>
                                <h4><?= $doctors_list[$i]->post_title ?></h4>
                                <?php if (get_field("role", $id)) { ?>
                                    <div class="role">
                                        <?= get_field("role", $id) ?>
                                    </div>
                                <?php } ?>
                                <?= get_field("description", $id); ?>
                            </a>
                        </div>
                    <?php }?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>
