<?php 
    $related_term = get_field('price_table_drug_price');
    $drug_price = false;

    $doses = array();
    $quantities = array();
    $packaging_array = array();
    $price_drug_array = array();

    foreach ($related_term as $key => $term) {
        $drug_name_array[] = $term->name;
    }
        $posts_array = get_posts(
            array(
                'posts_per_page' => -1,
                'post_type' => 'drug_price',
                'post_status' => 'private',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'drugpricecat',
                        'field' => 'term_id',
                        'terms' => $related_term[0]->term_id,
                        'include_children' => false
                    )
                )
            )
        );
    
        foreach ($posts_array as $post) { 
            $dose = get_field('strength_drug_table', $post->ID);
            $quantity = get_field('quantity_drug_table', $post->ID);
            $packaging = get_field('packaging_drug_table', $post->ID);
            $price_drug = get_field('price_drug_table', $post->ID);
    
            if (!in_array($packaging, $packaging_array)) { 
                array_push($packaging_array, $packaging);
            }
    
            $dose_array = explode(' ',$dose);
    
            if (!in_array($dose_array[0], $doses)) { 
                array_push($doses, $dose_array[0]);
            }
    
            if (!in_array($quantity, $quantities)) { 
                array_push($quantities, $quantity);
            }
            array_push($price_drug_array, (int)explode('$',$price_drug)[1]);
        }

        $min_price_key = array_keys($price_drug_array, min($price_drug_array))[0];
        sort($doses);
        foreach ($doses as $key => $value) {
            $doses[$key] = $value.' '.$dose_array[1];
        }
        sort($quantities);


    $packaging_get = isset($_GET['packaging']) ? $_GET['packaging'] : get_field('packaging_drug_table', $posts_array[$min_price_key]->ID);
    $dose_get = isset($_GET['dose']) ? $_GET['dose'] : get_field('strength_drug_table', $posts_array[$min_price_key]->ID);
    $quantity_get = isset($_GET['quantity']) ? $_GET['quantity'] : get_field('quantity_drug_table', $posts_array[$min_price_key]->ID);
    $related_term_name = $drug_name_array[0];
    
    if($packaging_get != '' && $dose_get != '' && $quantity_get != ''){
        $drug_price = get_posts( 
            array( 
                'name' => strtolower($related_term[0]->name).'-'.strtolower($packaging_get).'-'.strtolower($dose_get).'-'.strtolower($quantity_get), 
                'post_type' => 'drug_price',
                'post_status' => 'private',
                'posts_per_page' => 1
            ) 
        );
        if(sizeof($drug_price) > 0){
            $drug_price = $drug_price[0];
        }
    }
?>
<div id="drug-price">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div>
                    <h2 class="title-drug-price">
                        <?= get_field('title_drug_price') ?>
                    </h2>
                    <p class="description-drug-price">
                        <?= get_field('description_drug_price') ?>
                    </p>
                </div>
                <div class="row drug-price-calculator">
                    <div class="col-sm-12 col-md-3 nopadding"> 
                            <div id="name-box" class="name-box">
                                <select name="related_term_name" id="related_term_name" style= "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)">
                                    <option value="">Name</option>
                                    <?php foreach ($drug_name_array as $name) { 
                                        $name_selected = '';
                                        if (strcmp($name,$related_term_name) == 0 ) { 
                                            $name_selected = 'selected';
                                        } ?>
                                        <option value="<?= $name ?>" <?= $name_selected ?>>
                                            <?= $name ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="packaging-box" class="calculator-box">
                                <select name="packaging" id="packaging" style= "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)">
                                    <option value="">Packaging</option>
                                    <?php foreach ($packaging_array as $packaging) { 
                                        $packaging_selected = '';
                                        if (strcmp($packaging_get,$packaging) == 0 ) { 
                                            $packaging_selected = 'selected';
                                        } ?>
                                        <option value="<?= $packaging ?>" <?= $packaging_selected ?>>
                                            <?= $packaging ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="dose-box" class="calculator-box">
                                <select name="dose" id="dose" style= "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)">
                                    <option value="">Dose</option>
                                    <?php foreach ($doses as $dose) { 
                                        $dose_selected = '';
                                        if ($dose_get == $dose ) { 
                                            $dose_selected = 'selected';
                                        } ?>
                                        <option value="<?= $dose ?>" <?= $dose_selected ?>>
                                            <?= $dose ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div id="quantity-box" class="calculator-box">
                                <select name="quantity" id="quantity" style= "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)">
                                    <option value="">Quantity</option>
                                    <?php foreach ($quantities as $quantity) { 
                                        $quantity_selected = '';
                                        if ($quantity_get == $quantity ) { 
                                            $quantity_selected = 'selected';
                                        } ?>
                                        <option value="<?= $quantity ?>" <?= $quantity_selected ?>>
                                            <?= $quantity ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                    </div>
                    <div class="col-sm-12 col-md-1 nopadding">
                        <p class="drug-price-calculator-divider">
                            <i class="arrow-drugs-template right"></i>
                            <i class="arrow-drugs-template down"></i>
                        </p>
                    </div>
                    <div class="col-sm-12 col-md-4 col-lg-3 nopadding">
                        <div class="drugs-name-dose">
                            <div id="drug-calculator-name" class="drugs-calculator-box drug-calculator-name">
                                <p>
                                    <?php if($drug_price) {
                                        echo $related_term[0]->name;
                                    } ?>
                                </p>
                            </div>
                            <div id="drug-calculator-dose" class="drugs-calculator-box drug-calculator-dose">
                                <p>
                                    <?php if($drug_price) {
                                        echo get_field('strength_drug_table', $drug_price->ID);
                                    } ?>
                                </p>
                            </div>
                        </div>
                        <div class="drugs-calculator-box drug-calculator-price">
                            <p class="drug-calculator-value">
                                <?php if($drug_price) { ?>
                                    <img class="price_icon_drug_price" src="<?= get_field('price_icon_drug_price') ?>" />
                                    <span id="drug-calculator-value">
                                        <?= str_replace('$','',get_field('price_drug_table', $drug_price->ID)); ?>
                                    </span>
                                <?php } ?>
                            </p>
                            <p class="drug-calculator-month">
                                <?php if($drug_price) { ?>
                                    per month
                                <?php } ?>
                            </p>
                            <p class="drug-calculator-promotion">
                                <?php $less_than_text_drug_price = get_field('less_than_text_drug_price');
                                if($drug_price && $less_than_text_drug_price) {
                                    echo $less_than_text_drug_price;
                                } ?>
                            </p>
                        </div>
                        <div class="drugs-name-dose">
                            <div id="drug-calculator-tablet" class="drugs-calculator-box drug-calculator-qty">
                                <p>
                                    <?php if($drug_price) {
                                        echo get_field('packaging_drug_table', $drug_price->ID);
                                    } ?>
                                </p>
                            </div>
                            <div id="drug-calculator-qty" class="drugs-calculator-box drug-calculator-qty">
                                <p>
                                    <?php if($drug_price) {
                                        echo 'Quantity: ' . get_field('quantity_drug_table', $drug_price->ID);
                                    } ?>
                                </p>
                            </div>
                        </div>
                        <div class="drugs-calculator-box">
                            <p>
                                <img src="<?= get_field('khealth_logo_calculator_drug_price')?>" />
                            </p>
                        </div>
                    </div>
                    <div class="col-12">
                        <a class="btn btn-primary drugs-calculator-order" href="<?= get_field('link_order_now_drug_price');?>">
                            <?php if(get_field('cta_text_drug_price') != "") {
								echo get_field('cta_text_drug_price'); 
							} else {
								echo "Get started"; 
							} ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
