
<div id="text-block" class="section-text bg-<?php the_field("background") ?>">
  <div class="container">
    <?php if (get_field("title")) {
      ?>
      <h2 class="section-head">
        <?php the_field("title") ?>
      </h2>
      <?php
    }  ?>
    <?php if (get_field("description")) {
      ?>
      <div class="sub-entry">
        <?php the_field("description") ?>
      </div>
      <?php
    }  ?>

    <?php switch (get_field("content_type")) {
      case 'text':
      ?>
      <div class="entry">
        <?php the_field("content") ?>
      </div>
      <?php
      break;
      case 'benefits':
      ?>
      <div class="row">
        <?php if( have_rows('benefits') ):
          while( have_rows('benefits') ): the_row(); ?>

            <div class="col-md-6">
              <div class="benefit-item">
                <h3><?php the_sub_field('title'); ?></h3>
                <p><?php the_sub_field('text'); ?>
              </p>
            </div>
          </div>

        <?php endwhile;
      endif; ?>
    </div>
    <?php
    break;

    case 'numbers':
    ?>
    <div class="number-list row">
      <?php if( have_rows('numbers') ):
        while( have_rows('numbers') ): the_row(); ?>

          <div class="number-item col-md-4">
            <div class="number">
              <?php the_sub_field('title'); ?>
            </div>
            <div class="desc">
              <?php the_sub_field('text'); ?>
            </div>
          </div>
        <?php endwhile;
      endif; ?>
    </div>
    <?php
    break;
    case 'investors':
    ?>
    <div class="investors-list">
      <?php if( have_rows('investors') ):
        while( have_rows('investors') ): the_row(); ?>

          <div class="investor-item">
            <div class="img-h">
              <?php if (get_sub_field('link')) { ?>
                <a href="<?php the_sub_field('link'); ?>">
                  <?php 
                }?>
                <?php echo wp_get_attachment_image( get_sub_field('ico'), "full" ); ?>
                <?php if (get_sub_field('link')) { ?>
                </a>
                <?php 
              }?>
            </div>
          </div>
        <?php endwhile;
      endif; ?>
    </div>
    <?php
    break;

    case 'how':
    ?>
    <div class="row how-steps">
      <div class="col-md-9 entry">
        <?php if( have_rows('how') ):
          while( have_rows('how') ): the_row(); ?>

            <div class="hitem">
              <div class="ico">
                <?php echo wp_get_attachment_image( get_sub_field('ico'), "full" ); ?>
              </div>
              <h4><?php the_sub_field('title'); ?></h4>
              <p><?php the_sub_field('text'); ?></p>
            </div>
          <?php endwhile;
        endif; ?>
      </div>
      <div class="col-md-3 a-s-e">
        <div class="image">
          <?php echo wp_get_attachment_image( get_field('how_copy'), "full" ); ?>
        </div>
      </div>
    </div>
    <?php
    break;
    case 'compare':
    ?>
    <div class="compare-table">
      <?php the_field("content") ?>
    </div>
    <?php
    break;

    case 'coverage':
    ?>
    <div class="entry covert">
      <?php the_field("content") ?>
      <div class="text-more js-more">
        <?php _e("View more", "theme") ?>
      </div>
      <div class="more-content">
        <?php the_field("coverage") ?>
      </div>
    </div>
    <?php
    break;

    case 'team':
    ?>

        <div class="doctors-list row">
      <?php if( have_rows('teamlist') ):
        while( have_rows('teamlist') ): the_row(); ?>

          <div class="col-sm-6 col-md-3">
            <div class="doctor-item">
              <div class="image">
                <?php echo wp_get_attachment_image( get_sub_field('image'), "testimonials@2x" ); ?>
              </div>
              <h4><?php the_sub_field('title'); ?></h4>
              <div class="role">
                <?php the_sub_field('role'); ?>
              </div>
            </div>
          </div>
        <?php endwhile;
      endif; ?>
        </div>    <?php
    break;

    case 'doctors':
    ?>

      <div class="doctors-list row gutters-30"><?php 
        $perpage = get_field("list_style") ? 4 : 8;
        $args = array(
          'post_type' => array( 'doctors' ),
          'posts_per_page' => $perpage,
          'tax_query' => array(
            array(
              'taxonomy' => 'doccat',
              'field'    => 'term_id',
              'terms'    => get_field("list_style_copy"),
              'operator' => 'IN'
            ),
          ),
        );
        $wp_query = new WP_Query( $args );
        while ($wp_query->have_posts()) : $wp_query->the_post(); 
        include(get_template_directory()."/part-doctor.php");
        endwhile;
        ?>
      </div>
<?php if ($wp_query->found_posts > $perpage) { ?>
            <div class="text-more js-more">
              <?php _e("View more", "theme") ?>
            </div>
            <div class="more-content">
              <div class="doctors-list row gutters-30">
                
<?php wp_reset_query();

$args["offset"] = $perpage;
$args["posts_per_page"] = 999;

$wp_query = new WP_Query( $args );

while ($wp_query->have_posts()) : $wp_query->the_post(); 
 include(get_template_directory()."/part-doctor.php");
endwhile;
wp_reset_query(); ?>
              </div>
</div>
  <?php 
} 

    break;

    default:

    break;
  } ?>
</div>
</div>

