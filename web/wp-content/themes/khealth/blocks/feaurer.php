<div id="featured-block" class="section-featured">
	<div class="container">
		<div class="row gutters-30 a-center">
			<div class="col-sm-4 col-md-2">
				<span class="featured"><?php the_field("title") ?></span>
			</div>
			<?php if( have_rows('list') ): 
				while( have_rows('list') ): the_row(); ?>
					<?php $link = get_sub_field('link'); 
					$image = wp_get_attachment_image_src( get_sub_field('icon'), "featured" );
					$imageR = wp_get_attachment_image_src( get_sub_field('icon'), "featured@2x" );
					?>
					<div class="col-sm-4 col-md-2">
						<?php if( $link ): ?>
							<a href="<?php echo $link['url']; ?>" <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?>>
						<?php endif; ?>
						<img data-src="<?php echo $image[0]; ?>" data-srcset="<?php echo $imageR[0]; ?> 2x" alt="">
						<?php if( $link ): ?>
							</a>
						<?php endif; ?>
					</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</div>