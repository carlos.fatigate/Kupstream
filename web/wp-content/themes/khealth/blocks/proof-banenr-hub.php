<?php 
    $itens_proof_banner_hub = get_field('itens_proof_banner_hub');
    $banckground_color_proof_banner_hub = get_field('banckground_color_proof_banner_hub');
    $text_color_proof_banner_hub = get_field('text_color_proof_banner_hub');
?>
<?php if($itens_proof_banner_hub) { ?>
    <div id="proof-banner-hub" style="background-color:<?= $banckground_color_proof_banner_hub; ?>" >
        <div class="container">
            <div class="itens_proof_banner_hub">
                <?php foreach ($itens_proof_banner_hub as $key => $item) { ?>
                    <div class="itens_proof_banner_hub_content <?php if($key == 3) { echo "itens_proof_banner_hub_content_last_item";} else { echo "itens_proof_banner_hub_content_item";}?>">
                        <img src="<?= $item['icon']; ?>" />
                        <p style="color:<?= $text_color_proof_banner_hub; ?>" ><?= $item['text']; ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>
<style>
    .itens_proof_banner_hub_content_last_item {
        border-color: <?= $text_color_proof_banner_hub; ?>;
    }
</style>