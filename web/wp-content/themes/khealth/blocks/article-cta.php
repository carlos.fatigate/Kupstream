<div id="article-cta-block">
	<div class="article-cta-box">
		<div class="article-cta-box-flex">
			<div class="article-cta-logo">
				<img src="<?= get_field('khealth_logo_article_cta')?>" />
			</div>
			<div>
				<h3 class="article-cta-header"><?php the_field('header_article_cta'); ?></h3>
				<a href="<?php 
							$url = get_field("button_link_article_cta");
							$url = str_replace("[acf field='", "", $url);
							$url = str_replace("']", "", $url);
							print_r(get_field($url, "option"));
						?>" class="btn btn-primary article-cta">
					<?php the_field('button_text_article_cta'); ?>
				</a>
			</div>
		</div>
	</div>
</div>