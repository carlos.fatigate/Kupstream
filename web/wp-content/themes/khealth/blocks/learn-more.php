<?php
    $title_learn_more = get_field('title_learn_more');
    $small_text_learn_more = get_field('small_text_learn_more');
    $image_learn_more = get_field('image_learn_more');
    $content_title_learn_more = get_field('content_title_learn_more');
    $content_learn_more = get_field('content_learn_more');
    $image_background_learn_more = get_field('image_background_learn_more');
?>
<div id="learn-more" style="background-image:url('<?= $image_background_learn_more; ?>')">
    <div class="container">
        <p class="small_text_learn_more"><?= $small_text_learn_more; ?></p>
        <h2 class="title_learn_more"><?= $title_learn_more; ?></h2>
        <div class="content_learn_more">
            <img class="image_learn_more" src="<?= $image_learn_more; ?>"/>
            <div class="learn_more_content_box">
                <div class="content_title_learn_more_box">
                    <h3 class="content_title_learn_more"><?= $content_title_learn_more; ?></h3>
                    <div>
                        <?= $content_learn_more; ?>
                    </div>
                </div>
                <div class="background-transparent"></div>
            </div>
        </div>
    </div>
</div>

<style>
    #learn-more {
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>