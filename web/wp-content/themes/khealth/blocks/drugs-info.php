<?php $is_drug_page = get_field('is_drug_page'); 
	$learn_more = get_field('learn_more');
	$tabs_list = '';
	$tabs_content = '';
?>
<?php if($is_drug_page) { ?>
	<div class="container">
<?php } ?>
	<div id="drugs-block">
		<div class="row">
			<div class="col-md-12 col-lg-2 col-lx-3 nopadding">
				<div class="slideshow-container">
					<?php $slid_images = get_field('drugs_gallery');?>
					<?php foreach ($slid_images as $slid) { ?>
						<div class="drugs-slides fade" style="display:none">
							<img src="<?= $slid['image']?>" alt="<?= $slid['image_alt'] ?>"/>
						</div>
					<?php } ?>
					<a class="slideshowprev" id="drugs-slides-prev" onclick="plusSlides(-1)">&#10094;</a>
					<a class="slideshownext" id="drugs-slides-next" onclick="plusSlides(1)">&#10095;</a>
				</div>
			</div>
			<div class="col-md-12 col-lg-10 col-lx-9">
				<h1><span><?= get_field('title_first_line_drugs') ?></span> <?= get_field('title_second_line') ?></h1>
				<div>
					<a href="#drug-price" class="see_pricing_drugs"> 
						See Pricing
					</a>
				</div>
				<div id="drugs-nav-mobile">
					<?php $drugs_tabs = get_field('drugs_tabs');?>
					<?php foreach ($drugs_tabs as $key => $tab) { 
						$is_active = $key === 0 ? 'active' : ''; 
						$tab_size = sizeof($drugs_tabs)-1; ?>
						<?php 
								$tabs_list .= "<li class={$is_active}>
								<a href=''>{$tab['name']}</a>
							</li>";
							if($tab['is_related_tab']) {
								$tabs_content .="<div class='drugs-tabs-panel drugs-tabs-panel-related' data-index={$tab_size}>";
								$tabs_content .=" <div class='row'>";?>
									<?php foreach ($tab['related_tab'] as $drugs) {
										$tabs_content .="<div class='col-4'>
											<a class='drugs-related-link' href={$drugs['link']}>
												<div class='drugs-related-link-name'>
													<p>{$drugs['name']}</p>
													<p><i class='arrow right'></i></p>
												</div>
											</a>
										</div>";
									}
								$tabs_content .=" </div> ";
								$tabs_content .= "</div>";
							} else {
								$tabs_content .="<div class='drugs-tabs-panel {$is_active}' data-index={$key}>
									<div>{$tab['description']}</div>
								</div>";
							}?>
						<a href="#" class="dropdown-drugs-link">
							<div class="dropdown-drugs-box">
								<span>
									<?= $tab['name']?>
								</span>
								<span class="dropdown-drugs-flex-item drugs-arrow-down">
									<div class="toggler"></div>
								</span>
							</div>
						</a>
						<?php if($tab['is_related_tab']) { ?>
							<div class="drugs-tabs-description" style="display: none;">
								<div class='row'>
									<?php foreach ($tab['related_tab'] as $drugs) { ?>
										<div class='col-6'>
											<a class='drugs-related-link' href=<?= $drugs['link'] ?>>
												<div class='drugs-related-link-name'>
													<p><?= $drugs['name'] ?></p>
													<p><i class='arrow right'></i></p>
												</div>
											</a>
										</div>
									<?php }?>
								</div>
							</div>
						<?php } else { ?>
							<div class="drugs-tabs-description" style="display: none;">
								<?= $tab['description']?>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="drugs-tabs-container">
					<ul class="drugs-tabs">
						<?= $tabs_list; ?>
					</ul>
					<div class="drugs-tabs-content">
						<?= $tabs_content; ?>
					</div>
					<div>
						<a class="learn-more-link">
							<span class="learn_more_drugs_info">
								<?php if (get_field('learn_more_drugs_info') != '') { 
									echo get_field('learn_more_drugs_info');
								} else {
									echo 'Learn more';
								} ?>
							</span>
							<span class="learn_less_drugs_info" style="display:none">
								<?php if (get_field('learn_less_drugs_info') != '') { 
									echo get_field('learn_less_drugs_info');
								} else {
									echo 'Less';
								} ?>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php if($is_drug_page) { ?>
	</div>
<?php } ?>
