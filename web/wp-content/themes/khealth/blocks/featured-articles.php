<?php
    $featured_articles = get_field('feat_articles');
?>

<div id="featured-articles-block" class="featured_articles">
    <div class="desktop-only">
        <div id="tabs" class="row">
            <div class="col-md-4 nav-hldr">
                <div class="nav-content">
                    <h2>Featured<br/>Articles</h2>
                    <?php 
                    // Check $featured_articles exists.
                    if( $featured_articles ):
                         ?>
                    <ul class="nav-items">
                    <?php 
                        // set index to distinguish each post for click function
                        $index = 1;
                        foreach ($featured_articles as $featArt => $value):
                            // set post ID from relationship CF
                            $articleID = $value['block_feat_article'][0];
                            $permalink = get_permalink( $articleID );
                            $title = get_the_title( $articleID );
                            $date = get_the_date('F j, Y', $articleID);
                    ?>
                        <li>
                            <a id="tab<?= $index; ?>" title="<?= $title; ?>"><span class="nav-dot"></span><span class="title-text"><?= $title; ?></span></a>
                            <div class="date-read desktop-only"><?= $date ?></div>
                        </li>
                    <?php 
                        $index++;
                        endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-8 feat-aticles-hldr">
                <?php 
                // Check $featured_articles exists.
                if( $featured_articles ):
                        ?>                

                <?php 
                // set index to distinguish each post for click function
                $index = 1;
                
                foreach ($featured_articles as $featArt => $value):
                    // set post ID from relationship CF
                    $articleID = $value['block_feat_article'][0];
                    $permalink = get_permalink( $articleID );
                    $title = get_the_title( $articleID );
                    $date = get_the_date('F j, Y', $articleID);
                    $feat_img = get_the_post_thumbnail_url($articleID,'full');
                    $contentex = get_post_field( 'post_content', $articleID );
                    
                    $med_reviewed = get_field( 'medically_reviewed_check', $articleID );
                    $category = get_the_category($articleID);

                    $author = get_post_field( 'post_author', $articleID );
                    $author_name = get_the_author_meta( 'display_name', $author );

                    $feat_art_font_color = $value['feat_article_font_color'];
                    $article_excerpt = $value['article_excerpt'];
                    $readTime = $value['read_time'];
                    $bg_overlay = $value['bg_overlay'];
                    
                    $bg_dark = '';
                    if($bg_overlay){
                        $bg_dark = 'bg-dark';
                    }
                ?>
                
                <div class="article-item <?= $bg_dark; ?>" id="tab<?= $index; ?>C" style="background:url(<?= $feat_img; ?>) no-repeat center center; background-size: cover; color: #<?= $feat_art_font_color; ?>;">
                    <div class="article-content container">
                        <h2 style="color: #<?= $feat_art_font_color; ?>;">Featured<br/>Articles</h2>
                        <h4 class="category-read"><?php echo esc_html( $category[0]->cat_name ); ?> – <?= $readTime; ?> MIN READ</h4>
                        <h3 style="color: #<?= $feat_art_font_color; ?>;"><a href="<?php echo esc_url( $permalink ); ?>" title="<?= $title; ?>"><?= $title; ?></a></h3>
                        

                        <?php // print_r($meta);?>
                        <?php ?>

                        <div class="author-date" style="color: #<?= $feat_art_font_color; ?>;">By <?= $author_name; ?> | <?= $date; ?></div>
                        <?php if($med_reviewed) { ?>
                            <div class="medically-reviewed-box">
                                <img class="medically-reviewed-image" src="https://khealth.com/wp-content/uploads/2021/03/checkbox-dark-green.png">Medically reviewed
                            </div>	
                        <?php } ?>
                        <div class="article-excerpt">
                            <?php if($article_excerpt) { ?>
                                <p><?= $article_excerpt; ?></p>
                            <?php } else {?>
                                <p><?php echo wp_trim_words( $contentex, 25, '...' ); ?></p>
                            <?php } ?>
                        </div>
                        <a href="<?php echo esc_url( $permalink ); ?>" title="Read more" class="read-more">READ MORE</a>
                    </div>
                </div>
                <?php $index++;
                        endforeach; ?>

                <?php endif; ?>
                
                
            </div>
        </div>

    </div>

    
        <div class="featured_articles-slider mobile-only">
            
            <?php 
                // Check $featured_articles exists.
                if( $featured_articles ):
                        ?>                

                <?php 
                // set index to distinguish each post for click function
                $index = 1;
                
                foreach ($featured_articles as $featArt => $value):
                    // set post ID from relationship CF
                    $articleID = $value['block_feat_article'][0];
                    $permalink = get_permalink( $articleID );
                    $title = get_the_title( $articleID );
                    $date = get_the_date('F j, Y', $articleID);
                    $feat_img = get_the_post_thumbnail_url($articleID,'full');
                    $contentex = get_post_field( 'post_content', $articleID );
                    
                    $med_reviewed = get_field( 'medically_reviewed_check', $articleID );
                    $category = get_the_category($articleID);

                    $author = get_post_field( 'post_author', $articleID );
                    $author_name = get_the_author_meta( 'display_name', $author );

                    $feat_art_font_color = $value['feat_article_font_color'];
                    $readTime = $value['read_time'];
                    $bg_overlay = $value['bg_overlay'];
                    
                    $bg_dark = '';
                    if($bg_overlay){
                        $bg_dark = 'bg-dark';
                    }
                ?>
                <div class="slide-item">
                    <div class="feat-item <?= $bg_dark; ?>" id="tab<?= $index; ?>C" style="background:url(<?= $feat_img; ?>) no-repeat center center; background-size: cover; color: #<?= $feat_art_font_color; ?>;">
                        <div class="article-content">
                            <h2 style="color: #<?= $feat_art_font_color; ?>;">Featured<br/>Articles</h2>
                            <h4 class="category-read"><?php echo esc_html( $category[0]->cat_name ); ?> – <?= $readTime; ?> MIN READ</h4>
                            <h3 style="color: #<?= $feat_art_font_color; ?>;"><a href="<?php echo esc_url( $permalink ); ?>" title="<?= $title; ?>"><?= $title; ?></a></h3>
                            

                            <?php // print_r($meta);?>
                            <?php ?>

                            <div class="author-date" style="color: #<?= $feat_art_font_color; ?>;">By <?= $author_name; ?> | <?= $date; ?></div>
                            <?php if($med_reviewed) { ?>
                                <div class="medically-reviewed-box">
                                    <img class="medically-reviewed-image" src="https://khealth.com/wp-content/uploads/2021/03/checkbox-dark-green.png">Medically reviewed
                                </div>	
                            <?php } ?>
                            <div class="article-excerpt">
                                <p><?php echo wp_trim_words( $contentex, 25, '...' ); ?></p>
                            </div>
                            <a href="<?php echo esc_url( $permalink ); ?>" title="Read more" class="read-more">READ MORE</a>
                        </div>
                    </div>
                </div>


                <?php $index++;
                        endforeach; ?>

                <?php endif; ?>
            

        </div>
    </div>
</div>

