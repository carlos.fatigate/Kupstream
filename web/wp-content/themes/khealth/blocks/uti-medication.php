<?php
    $title_uti_medication = get_field('title_uti_medication');
    $small_text_uti_medication = get_field('small_text_uti_medication');
    $drugs_uti_medication = get_field('drugs_uti_medication');
    $drugs_see_more_uti_medication = get_field('drugs_see_more_uti_medication');
    $see_all_text_uti_medication = get_field('see_all_text_uti_medication');
    $below_text_uti_medication = get_field('below_text_uti_medication');
?>
<div id="uti_medication">
    <div class="container">
        <p class="small_text_uti_medication"><?= $small_text_uti_medication ?></p>
        <h2 class="title_uti_medication"><?= $title_uti_medication ?></h2>
        <div class="drugs_uti_medication hide-mobile">
            <?php if($drugs_uti_medication) { ?>
                <div class="drugs_uti_medication_box">
                    <?php foreach ($drugs_uti_medication as $key => $medication) { ?>
                        <div class="drugs_uti_medication_item">
                            <img class="medication_image" src="<?= $medication['image']?>" />
                            <p class="medication_name" ><?= $medication['name']?></p>
                            <p class="medication_laboratory" ><?= $medication['laboratory']?></p>
                            <p class="medication_mg" ><?= $medication['mg']?></p>
                            <?php if($medication['link']) { ?>
                                <a class="medication_link" href="<?= $medication['link']?>">Learn more</a>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="hide-desktop">
            <div class="slider-3-items row gutters-30">
                <?php if($drugs_uti_medication) { ?>
                    <?php foreach ($drugs_uti_medication as $key => $medication) { ?>
                        <div class="col-12 col-sm-4 slide-item">
                            <div class="drugs_uti_medication_carousel">
                                <img class="medication_image" src="<?= $medication['image']?>" />
                                <p class="medication_name" ><?= $medication['name']?></p>
                                <p class="medication_laboratory" ><?= $medication['laboratory']?></p>
                                <p class="medication_mg" ><?= $medication['mg']?></p>
                                <?php if($medication['link']) { ?>
                                    <a class="medication_link" href="<?= $medication['link']?>">Learn more</a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
                <?php if($drugs_see_more_uti_medication ) { ?>
                    <?php foreach ($drugs_see_more_uti_medication    as $key => $medication) { ?>
                        <div class="col-12 col-sm-4 slide-item">
                            <div class="drugs_uti_medication_carousel">
                                <img class="medication_image" src="<?= $medication['image']?>" />
                                <p class="medication_name" ><?= $medication['name']?></p>
                                <p class="medication_laboratory" ><?= $medication['laboratory']?></p>
                                <p class="medication_mg" ><?= $medication['mg']?></p>
                                <?php if($medication['link']) { ?>
                                    <a class="medication_link" href="<?= $medication['link']?>">Learn more</a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
        <?php if($drugs_see_more_uti_medication) { ?>
            <div class="hide-mobile">
                <div id="drugs_uti_medication_box_see_more">
                    <div class="drugs_uti_medication_box_see_more">
                        <?php foreach ($drugs_see_more_uti_medication as $key => $medication) { ?>
                            <div class="drugs_uti_medication_item">
                                <img class="medication_image" src="<?= $medication['image']?>" />
                                <p class="medication_name" ><?= $medication['name']?></p>
                                <p class="medication_laboratory" ><?= $medication['laboratory']?></p>
                                <p class="medication_mg" ><?= $medication['mg']?></p>
                                <?php if($medication['link']) { ?>
                                    <a class="medication_link" href="<?= $medication['link']?>">Learn more</a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="see_all_link_uti_medication">
                <div>
                    <?php // $see_all_text_uti_medication?>
                    <span class="see_all_link_uti_medication_view_all">View all</span>
                    <span class="see_all_link_uti_medication_view_less">View less</span>
                    <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M1.10198 0.851818C1.45996 0.493837 2.04036 0.493837 2.39834 0.851818L7.89835 6.35182C8.25633 6.7098 8.25633 7.2902 7.89835 7.64818L2.39834 13.1482C2.04036 13.5062 1.45996 13.5062 1.10198 13.1482C0.744001 12.7902 0.744001 12.2098 1.10198 11.8518L5.9538 7L1.10198 2.14818C0.744001 1.7902 0.744001 1.2098 1.10198 0.851818Z" fill="#2B8FFF"/>
                    </svg>
                </div>
            </div>
        <?php } ?>

        <?php if($below_text_uti_medication) { ?>
            <div>
                <p class="below_text_uti_medication"><?= $below_text_uti_medication; ?></p>
            </div>
        <?php } ?>
    </div>
</div>