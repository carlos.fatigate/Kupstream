<?php 
    $background_color_hero_twillio = get_field('background_color_hero_twillio');
    $title_hero_twillio = get_field('title_hero_twillio');
    $description_hero_twillio = get_field('description_hero_twillio');
    $imagem_hero_twillio = get_field('imagem_hero_twillio');
    $cta_text_hero_twillio = get_field('cta_text_hero_twillio');
    $cta_text_mobile_hero_twillio = get_field('cta_text_mobile_hero_twillio');
    $cta_link_mobile_hero_twillio = get_field('cta_link_mobile_hero_twillio');
    $apple_store_img_hero_twillio = get_field('apple_store_img_hero_twillio');
    $google_play_img_hero_twillio = get_field('google_play_img_hero_twillio');
    $subheader_hero_twillio = get_field('subheader_hero_twillio');
    $offer_terms_text_hero_twillio = get_field('offer_terms_text_hero_twillio');
    $show_newsletter_hero_twillio = get_field('show_newsletter_hero_twillio');
    $successful_newsletter_message = get_field("newsletter", "option");
    $success_message_newsletter_hero_twillio = get_field("success_message_newsletter_hero_twillio");

    if($success_message_newsletter_hero_twillio) {
        $successful_newsletter_message = $success_message_newsletter_hero_twillio;           
    }
    

    if(!$cta_text_mobile_hero_twillio){
        $cta_text_mobile_hero_twillio = $cta_text_hero_twillio;
    }

    $iphone = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone");
    $ipad   = strpos($_SERVER['HTTP_USER_AGENT'],"iPad");
    $ipod   = strpos($_SERVER['HTTP_USER_AGENT'],"iPod");

    $store_img = $google_play_img_hero_twillio;

    if ($iphone || $ipad || $ipod) {
        $store_img = $apple_store_img_hero_twillio;
    }

?>

<div id="hero-twillio" style="background-color:<?= $background_color_hero_twillio;?>">
    <div class="container">
        <div class="row a-center reverse-row">
            <div class="col-md-6">
                <?php if($subheader_hero_twillio) { ?>
                    <h2 class="subheader_hero_twillio"><?= $subheader_hero_twillio; ?></h2>
                <?php } ?>
                <h1 class="title_hero_twillio"><?= $title_hero_twillio; ?></h1>
                <div class="description_hero_twillio"><?= $description_hero_twillio; ?></div>
                <?php if($show_newsletter_hero_twillio) { ?>
                    <form class="newsletter-email-hero" method="post" id="newsletter-email-hero" action="#hero-twillio">
                        <div class="newsletter_input_box">
                            <?= do_shortcode("[braze-newsletter]"); ?>
                        </div>
                        <?php if($cta_text_hero_twillio){ ?>
                                <button onclick="sendNewsletterHero()" id="nesletter_send_btn" class="cta_text_newsletter hide-mobile"><?= $cta_text_hero_twillio; ?></button>
                                <button onclick="sendNewsletterHero()" id="nesletter_send_btn" class="cta_text_newsletter hide-desktop"><?= $cta_text_mobile_hero_twillio; ?></button>
                        <?php } ?>
                        <div class="newsletter_request_response newsletter_request_hero">
                            <?= $successful_newsletter_message['successful_newsletter_message']; ?>
                        </div>
                    </form>
                <?php } else { ?>
                    <?php echo do_shortcode("[sendex]"); ?>
                    <?php if($cta_link_mobile_hero_twillio) { ?>
                        <div class="cta_hero_mobile_twillio">
                            <a href="<?= $cta_link_mobile_hero_twillio; ?>" target="_blank">
                                <div class="cta_hero_mobile_twillio_flex">
                                    <img src="<?= $store_img ?>" />
                                </div>    
                            </a>
                        </div>
                    <?php } ?>
                <?php } ?>
                <?php if($offer_terms_text_hero_twillio) { ?>
                    <div class="offer_terms_text_hero_twillio">
                        <?= $offer_terms_text_hero_twillio; ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-6">
                <img class="imagem_hero_twillio" src="<?= $imagem_hero_twillio; ?>" />
            </div>
        </div>
    </div>
</div>

<script>
    function sendNewsletterHero() {

        analytics.track("Button Clicked", {
            button_context: "body",
            button_href: "Subscribe to newsletter",
            cta_text : "<?= $cta_text_hero_twillio; ?>",
            page_url : window.location.pathname,
            session_id: analytics.user().anonymousId()
        });
       
        $( `#newsletter-email-hero` ).submit(function( event ) {

            // Stop form from submitting normally
            event.preventDefault();
            event.stopImmediatePropagation();
            
            $("#nesletter_send_btn").prop("disabled",true);
            $sending = false
            if(!$sending){
                $sending = true
                // Get some values from elements on the page:
                var $form = $( this ),
                newsletter_email = $form.find( "input[name='newsletter_email']" ).val(),
                campaign_newsletter = $form.find( "input[name='campaign_newsletter']" ).val(),
                url = $form.attr( "action" );

                email_domain = newsletter_email.split("@")[1]

                analytics.track("Email Added", {
                    context: 'body',
                    email_domain: email_domain,
                    newsletter_campaign_name : campaign_newsletter,
                    session_id: analytics.user().anonymousId()
                });
    
                // Send the data using post
                var posting = $.post( url, { newsletter_email, campaign_newsletter } );
    
                // Put the results in a div
                posting.done(function( data ) {
                    $sending = false
                    $("#nesletter_send_btn").prop("disabled",false);
                    $( `#newsletter-email-hero` ).find( "input[name='newsletter_email']" ).val("")
                    $(".newsletter_request_hero").attr("style", "display:block")
                });
            }
        });
    }
</script>
