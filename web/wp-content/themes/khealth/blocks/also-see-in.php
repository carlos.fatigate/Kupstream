<?php
    $title_also_see_in = get_field('title_also_see_in');
    $logos_also_see_in = get_field('logos_also_see_in');
    $font_press = get_field('font_press');
    $logo_press = get_field('logo_press');
    $after_the_press_block_also_see_in = get_field('after_the_press_block_also_see_in');
?>
<div id="also-see-in"  <?php if($after_the_press_block_also_see_in) { echo 'class="after_the_press_block"'; } ?>>
    <div class="container">
        <h2 class="title_also_see_in"><?= $title_also_see_in ?></h2>
        <?php if($logos_also_see_in) { ?>
            <div class="logos_also_see_in">
                <?php foreach ($logos_also_see_in as $key => $logo) { ?>
                    <div class="logo_also_see_in">
                        <img src="<?= $logo['image']?>" />
                </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>