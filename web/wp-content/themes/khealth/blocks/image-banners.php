<?php 
    $banner_images = get_field("banner_images");
    $background_color_banner_images = get_field("background_color_banner_images");
?>
<div class="image-banners">
    <div id="image-banners" class="<?= $background_color_banner_images; ?>">
        <?php if($banner_images) { ?>
            <div class="image-banners-box">
                <?php foreach ($banner_images as $key => $image) { ?>
                    <div class="image-banners-box-item">
                        <img class="image-banners-src" src="<?= $image['src']?>" />
                        <p><?= $image['text']?></p>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>