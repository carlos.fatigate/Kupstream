
<div id="feature-block" class="section-feature <?php the_field("style") ?> bg-<?php the_field("background") ?>">
	<div class="container">
		<?php if (get_field("title")) { ?>
			<h2 class="section-head">
				<?php the_field("title") ?>
			</h2>
		<?php }?>
		<?php if (get_field("subtitle_feature_block")) { ?>
			<div class="sub-entry">
				<p><?= get_field("subtitle_feature_block") ?></p>
			</div>
		<?php }?>
		<div class="row a-center">
			<div class="col-md-6">
				<div class="entry">
					<?php the_field("content") ?>
				</div>
			</div>
			<div class="col-md-6">
				<?php if (get_field("video")) { ?>
					<div class="video">
						<video data-src="<?php the_field("video") ?>" autoplay muted playinline loop></video>
					</div>
				<?php } else { ?>
					<?php if(get_field("desktop_image_feature_block")) { ?>
						<div class="video">
							<img src="<?= get_field("desktop_image_feature_block") ?>" />
						</div>
					<?php } ?>
				<?php } ?>
				<?php $images = get_field('mobile_gallery');
				if( $images ): ?>
					<div class="gallery phone-only">
						<div class="slider-3-items">
							<?php foreach( $images as $image ):
								$image = wp_get_attachment_image_src( $image, "full" );

							 ?>
								<div class="slide-item">
									<img data-flickity-lazyload="<?php echo $image[0] ?>" alt="" />
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<?php if (get_field("bottom_row")) { ?>
			<p class="section-bottom entry">
				<?php the_field("bottom_row") ?>
			</p>
		<?php } ?>
	</div>
</div>