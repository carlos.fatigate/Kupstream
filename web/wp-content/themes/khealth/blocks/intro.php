<div id="intro-block" class="section-intro bg-<?php the_field("background") ?>">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="entry lg">
					<?php the_field("content") ?>
				</div>
			</div>
			<div class="col-md-6">
				<?php if (get_field('article_image_copy')) { ?>
					<div class="video_container">
						<iframe class="responsive-iframe" src="<?php echo get_field('article_image_copy') ?>"></iframe>
					</div>
				<?php } else { ?>
					<div class="article-insert">
						<div class="image">
							<?php echo wp_get_attachment_image( get_field('article_image'), "full" ); ?>
						</div>
						<div class="text entry">
							<?php the_field("article_content") ?>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>