<?php
    $title_recommended_content = get_field('title_recommended_content');
    $posts_recommended_content = get_field('posts_recommended_content');
    $cta_text_recommended_content = get_field('cta_text_recommended_content');
    $cta_link_recommended_content = get_field('cta_link_recommended_content');
?>
<div id="recommended-content">
    <div class="container">
        <h2 class="title_recommended_content"><?= $title_recommended_content ?></h2>
        <div class="slider-3-items row gutters-30">
            <?php foreach ($posts_recommended_content as $key => $post) { ?>
                <div class="col-12 col-md-6 col-lg-3 col-xl-3 slide-item">
                    <a href="<?= esc_url( get_permalink( $post ) ); ?>" >
                        <div class="posts_recommended_content_carousel">
                            <div class="posts_recommended_info" style="background-color:<?php 
                                    if($key % 4 == 0){ echo '#D7BDD7'; } 
                                    if($key % 4 == 1){ echo '#FC8C8D'; } 
                                    if($key % 4 == 2){ echo '#8DEDCF'; } 
                                    if($key % 4 == 3){ echo '#FECD51'; } 
                                ?>">
                                <p class="posts_recommended_content_name" >
                                    <?= wp_strip_all_tags( get_the_title($post), true ); ?> 
                                </p>
                                <p class="posts_recommended_content_excerpt" >
                                    <?= wp_strip_all_tags( get_the_excerpt($post), true ); ?>    
                                </p>
                                <div class="posts_recommended_content_link">Read more</div>
                            </div>
                            <div class="recommended_post_thumbnail" style="background-image:url(<?= get_the_post_thumbnail_url($post,'post-thumbnail' ); ?>)"></div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
        <?php if($cta_link_recommended_content && $cta_text_recommended_content ) { ?>
            <a class="cta_link_recommended_content" href="<?= $cta_link_recommended_content?>">
                <?= $cta_text_recommended_content?>
                <svg width="9" height="14" viewBox="0 0 9 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M1.10198 0.851818C1.45996 0.493837 2.04036 0.493837 2.39834 0.851818L7.89835 6.35182C8.25633 6.7098 8.25633 7.2902 7.89835 7.64818L2.39834 13.1482C2.04036 13.5062 1.45996 13.5062 1.10198 13.1482C0.744001 12.7902 0.744001 12.2098 1.10198 11.8518L5.9538 7L1.10198 2.14818C0.744001 1.7902 0.744001 1.2098 1.10198 0.851818Z" fill="#2B8FFF"/>
                </svg>
            </a>
        <?php } ?>
    </div>
</div>