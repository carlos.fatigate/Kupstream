<?php
    $title_photo_bullet = get_field('title_photo_bullet');
    $logos_photo_bullet = get_field('logos_photo_bullet');
    $description_photo_bullet = get_field('description_photo_bullet');
    $itens_list_photo_bullet = get_field('itens_list_photo_bullet');
    $cta_text_photo_bullet = get_field('cta_text_photo_bullet');
    $cta_link_photo_bullet = get_field('cta_link_photo_bullet');
    $image_photo_bullet = get_field('image_photo_bullet');
    $image_background_photo_bullet = get_field('image_background_photo_bullet');
?>
<div id="photo-bullet" style="background-image:url('<?= $image_background_photo_bullet; ?>')">
    <div class="container">
        <div class="row a-center">
            <div class="col-md-5">
                <?php if($image_photo_bullet) { ?>
                    <div class="doctors_word_class_doctors_img">
                        <img src="<?= $image_photo_bullet ?>" />
                    </div>
                <?php } ?>
            </div>
            <div class="col-md-7">
                <h2><?= $title_photo_bullet; ?></h2>
                <?php if($description_photo_bullet) { ?>
                    <p class="description_photo_bullet"><?= $description_photo_bullet; ?></p>
                <?php } ?>
                <?php if($logos_photo_bullet) { ?>
                    <div class="logos_word_class_doctors_box">
                        <?php foreach ($logos_photo_bullet as $key => $logo) { ?>
                            <img src="<?= $logo['image']?>" />
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if($itens_list_photo_bullet) { ?>
                    <ul class="itens_list_word_class_doctors">
                        <?php foreach ($itens_list_photo_bullet as $key => $item) { ?>
                            <li>
                                <div><p><?= $item['text']?></p></div>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <?php if($cta_text_photo_bullet && $cta_link_photo_bullet) { ?>
                    <a class="cta_text_word_class_doctors" href="<?= $cta_link_photo_bullet ?>" ><?= $cta_text_photo_bullet; ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<style>
    #photo-bullet {
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>