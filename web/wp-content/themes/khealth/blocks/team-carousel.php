<?php 
    $title_team_carousel = get_field('title_team_carousel');
    $description_team_carousel = get_field('description_team_carousel');
    $team_team_carousel = get_field('team_team_carousel');
    $background_color_team_carousel = get_field('background_color_team_carousel');
    $show_rows_team_carousel = get_field('show_rows_team_carousel');
    $background_imagem_team_carousel = get_field('background_imagem_team_carousel');
    $background_imagem_team_carousel_mobile = get_field('background_imagem_team_carousel_mobile');
    $text_color_team_carousel = get_field('text_color_team_carousel');
    $number_of_columns_team_carousel = get_field('number_of_columns_team_carousel');

    if($number_of_columns_team_carousel == 4) {
        $cols = "col-md-3";
    } else {
        $cols = "col-md-2-5";
    }
    
    
    if(!$background_color_team_carousel){
        $background_color_team_carousel = '#00406B';
    }
    if(!$text_color_team_carousel){
        $text_color_team_carousel = '#fff';
    }

    $text_color_team_carousel_read_more = '#C6F7E7';

    if($text_color_team_carousel){
        $text_color_team_carousel_read_more = $text_color_team_carousel;
    }
?>

<div style="background-color: <?= $background_color_team_carousel; ?>">
    <div class="team-carousel-background-image team-carousel-background-image-<?= $block['id'] ?>">
        <div id="team_carousel">
            <div class="container">
                <h2 class="title_team_carousel"><?= $title_team_carousel; ?></h2>
                <p class="description_team_carousel"><?= $description_team_carousel; ?></p>
                <?php if($team_team_carousel) { ?>
                    <div class="team_carousel">
                        <?php if($show_rows_team_carousel) { ?>
                            <div class='container_team_carousel hide-mobile'>
                                <div class="row">
                                    <?php foreach ($team_team_carousel as $key => $doctor) { 
                                        $profile_img = get_field('doctor_biography_image_profile', $doctor->ID);
                                        if(!$profile_img){
                                            $profile_img = get_the_post_thumbnail_url($doctor->ID,'full');
                                        }    
                                    ?>
                                        <div class="col-sm-6 col-md-4">
                                            <div class="doctor_team_carousel">
                                                <a href="<?= get_permalink($doctor->ID) ?>">
                                                    <div class="profile_img_team_carousel">
                                                        <img src="<?= $profile_img; ?>" alt="<?= $doctor->post_title?>" />
                                                    </div>
                                                    <p class="doctor_name_team_carousel"><?= $doctor->post_title?></p>
                                                    <?php if (get_field("role", $doctor->ID)) { ?>
                                                        <div class="role_team_carousel">
                                                            <?= get_field("role", $doctor->ID) ?>
                                                        </div>
                                                    <?php } ?>
                                                    <?php if( get_field("doctor_biography_meet_doctor_description", $doctor->ID)) { ?>
                                                        <div class="text-more js-more read_more_team_carousel">
                                                            <?php _e("Read more", "theme") ?>
                                                        </div>
                                                        <div class="doctor_description_team_carousel" style="display:none">
                                                            <?=  get_field("doctor_biography_meet_doctor_description", $doctor->ID); ?>
                                                            <div class="text-more read_more_team_carousel doctors-leader-read-more-prev">
                                                                <?php _e("Read less", "theme") ?>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </a>
                                            </div>        
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="slider-3-items row gutters-30 read-more-carousel <?php if($show_rows_team_carousel) { echo 'hide-desktop';}?>">
                            <?php foreach ($team_team_carousel as $key => $doctor) { 
                                $profile_img = get_field('doctor_biography_image_profile', $doctor->ID);
                                if(!$profile_img){
                                    $profile_img = get_the_post_thumbnail_url($doctor->ID,'full');
                                }    
                                ?>
                                <div class="col-sm-6 <?= $cols ?> slide-item">
                                    <div class="team-carousel-box">
                                        <div class="profile_img_team_carousel">
                                            <img src="<?= $profile_img; ?>" alt="<?= $doctor->post_title?>" />
                                        </div>
                                        <p class="doctor_name_team_carousel"><?= $doctor->post_title?></p>
                                        <?php if (get_field("role", $doctor->ID)) { ?>
                                            <div class="role_team_carousel">
                                                <?= get_field("role", $doctor->ID) ?>
                                            </div>
                                        <?php } ?>
                                        <?php if( get_field("doctor_biography_meet_doctor_description", $doctor->ID)) { ?>
                                            <div class="text-more js-more-carousel read_more_team_carousel">
                                                <?php _e("Read more", "theme") ?>
                                            </div>
                                            <div class="doctor_description_team_carousel" style="display:none">
                                                <?= get_field("doctor_biography_meet_doctor_description", $doctor->ID); ?>
                                                <div class="text-more read_more_team_carousel js-more-carousel-more-prev">
                                                    <?php _e("Read less", "theme") ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>        
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<style>
    .team-carousel-background-image-<?= $block['id'] ?> {
        background-image:url('<?= $background_imagem_team_carousel_mobile ?>')
    }

    .title_team_carousel,  .description_team_carousel, .doctor_name_team_carousel, .role_team_carousel, .doctor_description_team_carousel {
        color:<?= $text_color_team_carousel ?>;
    }

    .read_more_team_carousel {
        color:<?= $text_color_team_carousel_read_more ?>;
    }

    @media screen and (min-width: 981px) {
        .team-carousel-background-image-<?= $block['id'] ?> {
            background-image:url('<?= $background_imagem_team_carousel ?>')
        }
    }
</style>