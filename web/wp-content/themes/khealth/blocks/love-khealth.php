<?php
    $title_love_khealth = get_field('title_love_khealth');
    $small_text_love_khealth = get_field('small_text_love_khealth');
    $small_text_bottom_love_khealth = get_field('small_text_bottom_love_khealth');
    $itens_love_khealth = get_field('itens_love_khealth');
    $cta_text_love_khealth = get_field('cta_text_love_khealth');
    $cta_link_love_khealth = get_field('cta_link_love_khealth');
    $above_of_warning_block_love_khealth = get_field('above_of_warning_block_love_khealth');
?>

<div id="love-khealth" <?php if($above_of_warning_block_love_khealth) { echo "style='padding-bottom:7px'"; }?>>
    <div class="container">
        <p class="small_text_love_khealth"><?= $small_text_love_khealth ?></p>
        <h2><?= $title_love_khealth ?></h2>
        <?php if($itens_love_khealth) { ?>
            <div class="row">
                <?php foreach ($itens_love_khealth as $key => $item) { ?>
                    <?php if($key == sizeof($itens_love_khealth) -1 ) {
                        $style_margin = 'style="margin-bottom:0px"';
                    } else {
                        $style_margin = '';
                    }?>
                    <div class="col-md-4">
                        <div class="itens_love_khealth" <?= $style_margin; ?>>
                            <img class="itens_love_khealth_img" src="<?= $item['image'] ?>"/>
                            <p class="itens_love_khealth_title"><?= $item['title'] ?></p>
                            <p class="itens_love_khealth_description"><?= $item['description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
        <?php if($small_text_bottom_love_khealth) { ?>
            <p class="small_text_bottom_love_khealth"><?= $small_text_bottom_love_khealth ?></p>
        <?php } ?>
        <?php if($cta_link_love_khealth && $cta_text_love_khealth) { ?>
            <a class="btn btn-primary cta_love_khealth" href="<?= $cta_link_love_khealth ?>"><?= $cta_text_love_khealth ?></a>
        <?php } ?>
    </div>
</div>