<?php 
$anchors_details = get_field('anchors_details');

if( $anchors_details ) { ?>
    <div id="anchors-search-bar-block"> 
        <div id="anchors-links">
            <div class="anch-link anch1-link">
                <a href="#<?php echo esc_attr( $anchors_details['anchor1'] ); ?>">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" fill="#fff"/>
                    </svg>
                    <?php echo esc_attr( $anchors_details['anchor1_link_text'] ); ?>
                </a>
            </div>
            <div id="vertical-line"></div>
            <div class="anch-link anch2-link">
                <a href="#<?php echo esc_attr( $anchors_details['anchor2'] ); ?>"> 
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-down" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z" fill="#fff"/>
                    </svg> 
                    <?php echo esc_attr( $anchors_details['anchor2_link_text'] ); ?>
                </a> 
            </div>
        </div>    
        <div id="search-term-container">
             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" fill="#97A0BD" />
            </svg>
            <input type="text" class="search-term" placeholder="Search for articles about..." id="search-term" autocomplete="off">
        </div> 
        <div class="search-overlay">
            <svg class="search-overlay__close"width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M18.7071 5.29289C19.0976 5.68342 19.0976 6.31658 18.7071 6.70711L6.70711 18.7071C6.31658 19.0976 5.68342 19.0976 5.29289 18.7071C4.90237 18.3166 4.90237 17.6834 5.29289 17.2929L17.2929 5.29289C17.6834 4.90237 18.3166 4.90237 18.7071 5.29289Z" fill="#00406B"/>
                <path fill-rule="evenodd" clip-rule="evenodd" d="M5.29289 5.29289C5.68342 4.90237 6.31658 4.90237 6.70711 5.29289L18.7071 17.2929C19.0976 17.6834 19.0976 18.3166 18.7071 18.7071C18.3166 19.0976 17.6834 19.0976 17.2929 18.7071L5.29289 6.70711C4.90237 6.31658 4.90237 5.68342 5.29289 5.29289Z" fill="#00406B"/>
            </svg>
            <div id="search-overlay__results"></div>
        </div>   
    </div>   
  
<?php } ?>
 