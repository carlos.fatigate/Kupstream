<?php 
$extraColsClass = array("a-s-e");
$extraClass = array();
if (get_field("background")) {
  $extraClass[] = "bg-img";
  $background = wp_get_attachment_image_src( get_field('background'), "hero1" );
}
if (get_field("background_image_mobile")) {
  $extraClass[] = "bg-img";
  $background_image_mobile = wp_get_attachment_image_src( get_field('background_image_mobile'), "hero1" );
}
switch (get_field("style")) {
  case 'novoverlay':
  $extraClass[] = "no-gradient";
  break;
  case 'long':
  $extraClass[] = "bg-blue";
  $extraClass[] = "long";
  break;
  case 'dark':
  $extraClass[] = "bg-blue";
  $extraClass[] = "dark";
  break;
  case 'solid':
  $extraClass[] = "bg-blue";
  $extraClass[] = "solid";
  break;
  case 'blue':
  $extraClass[] = "bg-blue";
  break;
  default:
  break;
}
switch (get_field("style_copy")) {
  case 'aic':
  $extraColsClass = array("a-i-c");
  break;
  default:
  break;
}
if (get_field("style") == "dark" && get_field("style_copy") == "aic") {
  $secondLink = true;
}

$related_term = get_field('price_table_hero');
if($related_term) {

  $drug_price = false;
  $posts_array = get_posts(
	  array(
		  'posts_per_page' => -1,
		  'post_type' => 'drug_price',
		  'post_status' => 'private',
		  'tax_query' => array(
			  array(
				  'taxonomy' => 'drugpricecat',
				  'field' => 'term_id',
				  'terms' => $related_term->term_id,
				  'include_children' => false
			  )
		  )
	  )
  );
  $doses_hero = array();
  $quantities_hero = array();
  $packaging_array_hero = array();
  $price_drug_array_hero = array();
  $drug_name_array_hero = $related_term->name;
  
  foreach ($posts_array as $key => $post) { 
	  $dose_hero = get_field('strength_drug_table', $post->ID);
	  $quantity_hero = get_field('quantity_drug_table', $post->ID);
	  $packaging_hero = get_field('packaging_drug_table', $post->ID);
	  $price_drug = get_field('price_drug_table', $post->ID);
  
	  if (!in_array($packaging_hero, $packaging_array_hero)) { 
		  array_push($packaging_array_hero, $packaging_hero);
	  }
  
	  $dose_array_hero = explode(' ',$dose_hero);
  
	  if (!in_array($dose_array_hero[0], $doses_hero)) { 
		  array_push($doses_hero, $dose_array_hero[0]);
	  }
  
	  if (!in_array($quantity_hero, $quantities_hero)) { 
		  array_push($quantities_hero, $quantity_hero);
	  }
	  array_push($price_drug_array_hero, (int)explode('$',$price_drug)[1]);
  }
  $min_price_key = array_keys($price_drug_array_hero, min($price_drug_array_hero))[0];
  sort($doses_hero);
  foreach ($doses_hero as $key => $value) {
	  $doses_hero[$key] = $value.' '.$dose_array_hero[1];
  }
  sort($quantities_hero);
  if($posts_array[$min_price_key]->ID) {
	$packaging_hero_get = isset($_GET['packaging']) ? $_GET['packaging'] : get_field('packaging_drug_table', $posts_array[$min_price_key]->ID);
	$dose_hero_get = isset($_GET['dose']) ? $_GET['dose'] : get_field('strength_drug_table', $posts_array[$min_price_key]->ID);
	$quantity_hero_get = isset($_GET['quantity']) ? $_GET['quantity'] : get_field('quantity_drug_table', $posts_array[$min_price_key]->ID);
  } ?>
  <input type="hidden" id="packaging-hero-get" value="<?= $packaging_hero_get ?>">
  <input type="hidden" id="dose-hero-get" value="<?= $dose_hero_get ?>">
  <input type="hidden" id="quantity-hero-get" value="<?= $quantity_hero_get ?>">
  <input type="hidden" id="drug-name-array-hero" value="<?= $drug_name_array_hero ?>">
<?php } ?>

<?php 
  $cta_background_color_hero = get_field('cta_background_color_hero');
  $cta_background_color_hover_hero = get_field('cta_background_color_hover_hero');
  $cta_text_color_hero = get_field('cta_text_color_hero');

  $style_hero = "style = ";
  $btn_background_hero = "#8ef3d0";
  
  if ($cta_background_color_hero) { 
	$style_hero .= 'background-color:'.$cta_background_color_hero.';'; 
	$btn_background_hero = $cta_background_color_hero;
  }
  if ($cta_text_color_hero) { 
	$style_hero .= 'color:'.$cta_text_color_hero.';'; 
  }

  $style_hero .= "' '";
  $mouse_over_hero = "' '";
  $mouse_out_hero = "' '";

  if ($cta_background_color_hover_hero) {
	$mouse_over_hero = 'this.style.background="'.$cta_background_color_hover_hero.'"';
	$mouse_out_hero = 'this.style.background="'.$btn_background_hero.'"';
  }

?>

<div id="hero-block" class="section-hero <?php echo implode(" ", $extraClass) ?>">
  <?php if (isset($background)) { ?>
	<div class="bg hero-bg" style="background-image: url(<?php echo $background[0]; ?>);"></div>
  <?php } ?>
  <?php if(!get_field('remove_background_mobile_image')) { ?>
		<?php if (isset($background_image_mobile)) { ?>
			<div class="bg hero-bg-mobile" style="background-image: url(<?php echo $background_image_mobile[0]; ?>);"></div>
		<?php } else { ?>
			<?php if (isset($background)) { ?>
				<div class="bg hero-bg-mobile" style="background-image: url(<?php echo $background[0]; ?>);"></div>
			<?php } ?>
		<?php } ?>
  <?php } else { ?>
		<div class="bg hero-bg-mobile"></div>
  <?php } ?>
	  <div class="container">
		<div class="row a-center">
		  <div class="<?php 
			if (get_field("style_copy") == "dynamic") {
			  echo "col-sm-7 col-md-8 col-xl-7";
			} elseif (get_field("style_copy") == "half") {
			  echo "col-md-6";
			} else {
			  echo get_field("style_copy") == "fullcenter" ? "col-md-12" : "col-md-7";
			}
			?>">
			<div class="sub-head">
			  <?php the_field("sub_title") ?>
			</div>
			<h<?php echo get_field("style") =="solid" ? "2":"1" ?>><?php the_field("title") ?></h<?php echo get_field("style") =="solid" ? "2":"1" ?>>
			<div class="entry <?php echo get_field("style_copy") == "ase" ? "lg" : "" ?>">
			  <?php the_field("content") ?>
			</div>
			<?php $logos = get_field('accreditation_logos_hero');
			  if(isset($logos) && is_array($logos) > 0) { ?>
				<div class="accreditation_logos_grid">
				  <div class="btns">
					<?php $inde =0; if( have_rows('buttons') ): while( have_rows('buttons') ): the_row(); $inde++; ?>
					  <?php $link = get_sub_field('button'); 
					  if( $link ): ?>
						<a href="<?php echo $link['url']; ?>" class="<?php 
						  if (isset($secondary)) {
							echo "btn btn-secondary";
						  } else {
							echo ($inde ==2 && isset($secondLink)) ? "link" : "btn btn-primary";
						  } ?>" 
						  <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?> 
						  <?php if(isset($style_hero)) { echo $style_hero; }; ?> 
						  onMouseOver = <?php if(isset($cta_background_color_hover_hero)) { echo $mouse_over_hero; }; ?>
						  onMouseOut = <?php if(isset($cta_background_color_hover_hero)) { echo $mouse_out_hero; }; ?>
						>
							<?php echo $link['title']; ?>
						</a>
					  <?php endif; ?>
					<?php endwhile; endif; ?>
				  </div>
					<div class="hide-mobile">
						<?php foreach ($logos as $image) { ?>
						  <img class="accreditation_logos" src="<?= $image['logo']; ?>" />
						<?php } ?>
					</div>
				</div>
			  <?php } else { ?>
				<div class="btns">
				  <?php $inde =0; if( have_rows('buttons') ): while( have_rows('buttons') ): the_row(); $inde++; ?>
					<?php $link = get_sub_field('button'); 
					if( $link ): ?>
					  <a href="<?php echo $link['url']; ?>" class="<?php 
						if (isset($secondary)) {
						  echo "btn btn-secondary";
						} else {
						  echo ($inde ==2 && isset($secondLink)) ? "link" : "btn btn-primary";
						} ?>" 
						<?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?> 
						<?php if(isset($style_hero)) { echo $style_hero; }; ?> 
						onMouseOver= <?php if(isset($cta_background_color_hover_hero)) { echo $mouse_over_hero; } else { echo ""; }; ?>
						onMouseOut= <?php if(isset($cta_background_color_hover_hero)) { echo $mouse_out_hero; } else { echo ""; }; ?>
					  >
						<?php echo $link['title']; ?>
					  </a>
					<?php endif; ?>
				  <?php endwhile; endif; ?>
				</div>
			  <?php } ?>
		<?php if( have_rows('links') ):?>
				  <div class="links">
		  <?php
		  while( have_rows('links') ): the_row(); ?>

			<?php $link = get_sub_field('link'); if( $link ): ?>
			<a href="<?php echo $link['url']; ?>" <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?>><?php echo wp_get_attachment_image( get_sub_field('icon'), "full" ); ?></a>
			<?php endif; ?>
		<?php endwhile;?>
			</div>
		<?php endif;?>
			<?php if (get_field("note") && get_field("style_copy") != "a-i-c") { 
			  echo "<div class='note'>".get_field("note")."</div>";
			} ?>
		  </div>
		  <div class="<?php if (get_field("style_copy") == "half") {
			echo "col-md-6";
			} else {
			echo "col-md-5";
			} ?> <?php echo implode(" ", $extraColsClass) ?>">
			<?php if (get_field("video")) { ?>
			<div class="video-holder">
			  <div class="video">
				<video data-src="<?php the_field("video") ?>" autoplay muted playsinline loop></video>
			  </div>
			</div>
			  <?php 
			} ?>
			<?php if (get_field("image")) { ?>
			  <?php echo wp_get_attachment_image( get_field('image'), "full hero-bg" ); ?>
			<?php } ?>
			<?php if(!get_field('remove_mobile_image')) { ?>
				<?php if (get_field("image_mobile")) { ?>
					<?php echo wp_get_attachment_image( get_field('image_mobile'), "full hero-bg-mobile" ); ?>
				<?php } else { ?>
					<?php echo wp_get_attachment_image( get_field('image'), "full hero-bg-mobile" ); ?>
				<?php } ?>
			<?php } ?>
		  </div>
		</div>
			<?php if (get_field("note") && get_field("style_copy") == "a-i-c") { 
			  echo "<div class='note'>".get_field("note")."</div>";
			} ?>
	  </div>
	  <?php if(isset($logos) && is_array($logos) > 0){ ?>
		<div class="accreditation_logos_grid accreditation_logos_grid_background ">
		  <?php $logos_mobile = get_field('accreditation_logos_mobile_hero'); ?>
		  <?php if($logos_mobile) { ?>
			<?php foreach ($logos_mobile as $image) { ?>
			  <img class="accreditation_logos" src="<?= $image['logo']; ?>" />
			<?php } ?>
		  <?php } else { ?>
			<?php foreach ($logos as $image) { ?>
			  <img class="accreditation_logos" src="<?= $image['logo']; ?>" />
			<?php } ?>
		  <?php } ?>
		</div>
	  <?php } ?>
  </div>
