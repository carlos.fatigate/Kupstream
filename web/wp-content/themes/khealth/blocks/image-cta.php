<?php
    $src_image_cta = get_field('src_image_cta');
    $background_image_cta_url = get_field('background_image_cta');
    $title_image_cta = get_field('title_image_cta');
    $cta_text_image_cta = get_field('cta_text_image_cta');
    $cta_link_image_cta = get_field('cta_link_image_cta');
    $background_color_image_cta = get_field('background_color_image_cta');  
?>
<div id="image-cta" style=" background-color: <?= $background_color_image_cta?>; background-image: url('<?= $background_image_cta_url; ?>');">
    <div class="container">
        <div class="image_cta_box <?php if(!$src_image_cta) { echo 'image_cta_box_center';}?>">
            <?php if($src_image_cta) { ?>
                <div class="image_cta_src">
                    <img src="<?= $src_image_cta; ?>" />
                </div>
            <?php } ?>
            <div style="width:100%">
                <h2 class="title_image_cta"><?= $title_image_cta ?><div class="circle"></div></h2>
                <?php if($cta_text_image_cta && $cta_link_image_cta) { ?>
                    <a class="btn btn-primary cta_link_image_cta" href="<?= $cta_link_image_cta ?>"><?= $cta_text_image_cta ?></a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<style>
    #image-cta {
        background-size: cover;
        background-position: center;
        background-repeat: no-repeat;
    }
</style>