<div id="how-to-order">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2><?= get_field('how_to_order_title'); ?></h2>
                <?php $how_to_order_cards = get_field('how_to_order_cards'); ?>
                <div class="how-to-order-cards-grid">
                    <?php foreach ($how_to_order_cards as $key => $card) { ?>
                        <div class="how-to-order-card">
                            <p class="how-to-order-number"><?= $key+1?></p>
                            <div class="how-to-order-title">
                                <?php if($card['mobile_title_how_to_order'] != '') { ?>
                                    <span class="hide-mobile"><?= $card['title_how_to_order']?></span>
                                    <span class="hide-desktop"><?= $card['mobile_title_how_to_order']?></span>
                                <?php } else { ?>
                                    <?= $card['title_how_to_order']?>
                                <?php } ?>
                            </div>
                            <p class="how-to-order-description"><?= $card['description_how_to_order']?></p>
                        </div>
                    <?php } ?>   
                </div>
                <div class="col-12">
                    <a class="btn btn-primary drugs-calculator-order" href="<?php 
                                                                            $url = get_field("how_to_order_get_start_button");
                                                                            $url = str_replace("[acf field='", "", $url);
                                                                            $url = str_replace("']", "", $url);
                                                                            print_r(get_field($url, "option"));
                                                                            ?>">
                        <?= get_field('how_to_order_get_start_button_text');?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
