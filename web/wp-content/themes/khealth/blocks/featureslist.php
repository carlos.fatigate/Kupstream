<?php 
	if (get_field("per_row") == "4pl") {
		$per_row = "3";
	} else {
		$per_row = (12/get_field("per_row"));
	}
?>
<div id="feature-list-block" class="section-features bg-<?php the_field("background") ?>">
  <div class="container">
	<?php if (get_field("sub_title")) { ?>
		<div class="sub-head">
			<?php the_field("sub_title") ?>
		</div>
	<?php }  ?>
	<?php if (get_field("title")) { ?>
		<h2 class="section-head">
			<?php the_field("title") ?>
		</h2>
	<?php }  ?>
	<?php if (get_field("description")) { ?>
		<div class="sub-entry">
			<?php the_field("description") ?>
		</div>
	<?php } ?>
	<div class="row <?php echo get_field("per_row") == "4pl" ? "included" : "" ?>">
		<?php if( have_rows('features') ):
			while( have_rows('features') ): the_row(); ?>
				<div class="col-md-<?php echo $per_row; ?>">
					<div class="simple-feature  <?php echo get_field("icon_sie") == "medium" ? 'small-icon': '' ?>">
						<div class="image   <?php echo get_field("icon_sie") == "small" ? 'sm': '' ?>">
							<?php echo wp_get_attachment_image( get_sub_field('icon'), "full" ); ?>
						</div>
						<<?php echo get_field("icon_sie_copy") ?>><?php the_sub_field('title'); ?></<?php echo get_field("icon_sie_copy") ?>>
						<p><?php the_sub_field('description'); ?></p>
						<?php $link = get_sub_field('link'); 
						if( $link ): ?>
							<a href="<?php echo $link['url']; ?>" class="btn btn-secondary" <?php if ($link['target']) { echo 'target="'.$link['target'].'"'; } ?>>
								<?php echo $link['title']; ?>
							</a>
						<?php endif; ?>
					</div>
				</div>
			<?php endwhile;
		endif; ?>
	</div>
</div>
</div>
