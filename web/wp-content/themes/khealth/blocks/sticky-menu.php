<?php $drugs_tabs = get_field('links');?>
<div class="drugs-sticky-menu">
	<?php foreach ($drugs_tabs as $key => $tabs) { ?>
		<a href="<?php echo $tabs['anchor_type'] ? '#'.$tabs['anchor'] : $tabs['link'] ?>" class="drugs-sticky-menu-link"><?= $tabs['title'] ?></a>
	<?php } ?>
</div>

<div class="dropdown-drugs">
	<div class="container">
		<div id="toggle-dropdown" class="dropdown-drugs-box toggle-dropdown" onclick="toggleDropdown()">
			<span class="dropdown-drugs-flex-item">On this page</span>
			<span class="dropdown-drugs-flex-item drugs-arrow-down"><i class="arrow down"></i></span> 
			<span class="dropdown-drugs-flex-item drugs-arrow-up"><i class="arrow up"></i></span> 
		</div>
		<div id="toggle-dropdown-drugs-content">
			<div class="dropdown-drugs-content">
				<?php foreach ($drugs_tabs as $key => $tabs) { ?>
					<p>
						<a href="<?php echo $tabs['anchor_type'] ? '#'.$tabs['anchor'] : $tabs['link'] ?>" ><?= $tabs['title'] ?></a>
					</p>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<script>
	document.addEventListener("DOMContentLoaded", function(event) {
		$(".body").css("overflow", "initial")
	});
</script>
