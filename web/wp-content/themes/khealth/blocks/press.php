<?php
    $title_press = get_field('title_press');
    $content_press = get_field('content_press');
    $font_press = get_field('font_press');
    $logo_press = get_field('logo_press');
?>

<div id="press">
    <div class="container">
        <h2 class="title_press"><?= $title_press ?></h2>
        <p class="content_press"><?= $content_press ?></p>
        <p class="font_press"><?= $font_press ?></p>
        <?php if($logo_press){ ?>
            <img src="<?= $logo_press ?>" />
        <?php } ?>
    </div>
</div>