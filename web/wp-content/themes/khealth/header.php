<?php
/**
 * Header
 *
 * Contains dummy HTML to show the default structure of WordPress header file
 *
 * Remember to always include the wp_head() call before the ending </head> tag
 *
 * Make sure that you include the <!DOCTYPE html> in the same line as ?> closing tag
 * otherwise ajax might not work properly
 */
?><!DOCTYPE html>
<html class="<?php
if (is_user_logged_in()) {
	 echo  "user-logged";
} ?>" <?php language_attributes(); ?>>

<head>
  <?php 
	$page_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$url_array = explode('/', $page_url);
	$is_pagination = array_search('page', $url_array);

	if( array_search('page', $url_array) || array_search('doctors', $url_array) 
		|| array_search('testiomonials', $url_array) || array_search('faq', $url_array))  { ?>
		<meta name="robots" content="noindex, nofollow">
	<?php  } ?>
	<link rel="preload" as="font" href="<?php bloginfo( 'stylesheet_directory' ); ?>/fonts/SofiaPro-Bold.woff2" type="font/woff2" crossorigin="anonymous">
	<link rel="preload" as="font" href="<?php bloginfo( 'stylesheet_directory' ); ?>/fonts/SofiaPro-Bold.woff" type="font/woff" crossorigin="anonymous">

	<link rel="preload" as="font" href="<?php bloginfo( 'stylesheet_directory' ); ?>/fonts/SofiaPro-Medium.woff2" type="font/woff2" crossorigin="anonymous">
	<link rel="preload" as="font" href="<?php bloginfo( 'stylesheet_directory' ); ?>/fonts/SofiaPro-Medium.woff" type="font/woff" crossorigin="anonymous">

	<link rel="preload" as="font" href="<?php bloginfo( 'stylesheet_directory' ); ?>/fonts/SofiaPro-Regular.woff2" type="font/woff2" crossorigin="anonymous">
	<link rel="preload" as="font" href="<?php bloginfo( 'stylesheet_directory' ); ?>/fonts/SofiaPro-Regular.woff" type="font/woff" crossorigin="anonymous">

	<link rel="preload" as="font" href="<?php bloginfo( 'stylesheet_directory' ); ?>/fonts/DharmaGothicRndE-Bold.woff2" type="font/woff2" crossorigin="anonymous">
	<link rel="preload" as="font" href="<?php bloginfo( 'stylesheet_directory' ); ?>/fonts/DharmaGothicRndE-Bold.woff" type="font/woff" crossorigin="anonymous">

  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <?php if(get_field('meta_title_drug_template')) { ?>
	<title><?= get_field('meta_title_drug_template'); ?></title>
  <?php } else { ?>
  	<title><?php wp_title('');?></title>
  <?php } ?>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!-- <meta name="description" content="<?php // bloginfo( 'description' ); ?>"> -->
  <meta name="format-detection" content="telephone=no" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
  <link rel="icon" type="image/x-icon" href="<?php bloginfo( 'stylesheet_directory' ); ?>/favicon.ico">

  <?php if(get_field('meta_description_drug_template_copy')) { ?>
	<meta name="description" content="<?= get_field('meta_description_drug_template_copy'); ?>">
  <?php } ?>
  <?php 
	if (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
		if($_ENV['PANTHEON_ENVIRONMENT'] == 'live'){ 
			echo ("<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-M6ZZJ8V');</script>
			<!-- End Google Tag Manager --> ");
		} else { 
			echo ("<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-K4DVF2V');</script>
			<!-- End Google Tag Manager --> ");
	 	} 
	}
  ?>

  <?php
  wp_head();
  $body_class_var = array();
  if (get_field("header")) {
   $body_class_var[] = get_field("header")."-header";
 }
  if (is_archive() || is_author() || is_search() || is_single() || is_page_template("page-sidebar.php") || is_page_template("page-text.php") ) {
   $body_class_var[] = "white-header";
 }

 if(isset($_SERVER) && isset($_SERVER["REQUEST_URI"]) && strcmp($_SERVER["REQUEST_URI"], '/health-guides/') == 0) {
	$body_class_var[] = "default-header";
 }
?>
 
   <link href="<?php bloginfo( 'stylesheet_directory' ); ?>/dist/bundle.css" rel="stylesheet" type="text/css">
</head>
<body class="<?php echo implode(" ", $body_class_var) ?>">
<?php
	if ( is_front_page() ) :
		get_template_part('blocks/newsletter', 'popup') ;
	endif;
?>

<?php if (isset($_ENV['PANTHEON_ENVIRONMENT'])) {
		if($_ENV['PANTHEON_ENVIRONMENT'] == 'live'){ 
			echo (' <!-- Google Tag Manager (noscript) -->
				<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M6ZZJ8V"
				height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) --> ');
	    } else { 
			echo ('<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4DVF2V"
			height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
			<!-- End Google Tag Manager (noscript) --> ');
	    } 
	} ?>

<?php
if(isset($_GET['app'])){
	$is_app = boolval($_GET['app']);
} else {
	$is_app = false;
}

$treatments_menu = get_field('treatments');

?>

<?php 
	$link = false;
	$categories =  $categories = get_the_terms( get_the_ID(), 'pagecat' );
	$top_bar_text_mobile = get_field('top_bar_text_mobile');
	if($top_bar_text_mobile) {
		$link = $top_bar_text_mobile;
	} else {
		$header_mobile_information_per_category = get_field('header_mobile_information_per_category', "option");
		if($header_mobile_information_per_category) {
			foreach ($header_mobile_information_per_category as $key => $value) {
				if(isset($categories[0]->term_id) && $value["category"] == $categories[0]->term_id) {
					$link = $value["header_mobile_information"];
				}
			}
		}
	}
	if(!$link) {
		$link = get_field('header_mobile_information', "option");
	}
?>

<?php
	if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on'){
		$actual_link = "https";
	} else {
		$actual_link = "http";
	}
	$actual_link .= "://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
?>
<?php if(!get_field('nav_for_paid_traffic')) { ?>
	<header class="header">
		<?php if(!get_field("remove_top_bar_mobile")) { ?>
			<?php if( $link ): ?>
				<?php if(!$is_app) { ?>
					<?php if(get_field('show_on_desktop_top_bar_mobile')) { ?>
						<div class="header-bar">
					<?php } else { ?>
							<div class="header-bar hide-desktop">
					<?php } ?>
						<?php echo $link; ?>
					</div>
				<?php } ?>
			<?php endif;?>
		<?php } ?>
		<?php 
			$start_link = get_field("get_start_link");
			if ($start_link) {
				$get_start_link = $start_link;
			} else {
				$get_start_link = get_field("get_started_default_link","options"); 
			}
			$get_start_link = str_replace("[acf field='", "", $get_start_link);
			$get_start_link = str_replace("']", "", $get_start_link);
			$get_start_link = get_field($get_start_link, "option");

			$start_link_mobile = get_field("get_start_mobile_link");
			if ($start_link_mobile) {
				$get_start_mobile_link = $start_link_mobile;
				$get_start_mobile_link = str_replace("[acf field='", "", $get_start_mobile_link);
				$get_start_mobile_link = str_replace("']", "", $get_start_mobile_link);
				$get_start_mobile_link = get_field($get_start_mobile_link, "option");
			} else {
				$get_start_mobile_link = $get_start_link; 
			}
			

			if(get_field("get_start_text")) {
				$get_start_text = get_field("get_start_text");
			} else {
				$get_start_text = get_field("get_start_text_default","options");
			}

			if(get_field("get_start_mobile_text")) {
				$get_start_mobile_text = get_field("get_start_mobile_text");
			} else {
				$get_start_mobile_text = $get_start_text;
			}
		?>
		<?php if(!$is_app) { ?>
			<div class="container">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="logo">
					<img class="light-logo" src="/wp-content/uploads/2021/08/k-health-sky-blue-80px.png" />
					<img class="dark-logo" src="/wp-content/uploads/2021/08/k-health-dark-blue-80px.png" />
				</a>
				<nav class="menu new-menu">
					<div class="hide-mobile">
						<ul class="menu-list">
							<?php 
							wp_nav_menu( array(
							'theme_location' => 'mainnavigation',
							'container' => false,
							'items_wrap' => '%3$s'
							) ); 
							?>
						</ul>
					</div>
					<ul class="menu-list menu-list-login">
						<?php 
							$login_button = get_field("login_button","options"); 
							if($login_button && isset($login_button['link']) && $login_button['link']) {
						?>
								<li class="menu-item menu-zindex">
									<a href="<?= $login_button['link'] ?>" aria-current="page">
										<svg width="45" height="45" viewBox="0 0 45 45" fill="none" xmlns="http://www.w3.org/2000/svg">
											<circle cx="22.5" cy="22.5" r="22" fill="#B2DEFE"/>
											<path d="M22.4997 25.6243C25.9515 25.6243 28.7497 22.826 28.7497 19.3743C28.7497 15.9225 25.9515 13.1243 22.4997 13.1243C19.0479 13.1243 16.2497 15.9225 16.2497 19.3743C16.2497 22.826 19.0479 25.6243 22.4997 25.6243Z" fill="#B2DEFE" stroke="#00406B" stroke-width="2" stroke-miterlimit="10"/>
											<path d="M13.0256 31.0941C13.9861 29.4315 15.3672 28.051 17.0301 27.0912C18.6931 26.1315 20.5794 25.6262 22.4994 25.6262C24.4195 25.6262 26.3057 26.1315 27.9686 27.0914C29.6316 28.0512 31.0127 29.4317 31.9731 31.0943" fill="#B2DEFE"/>
											<path d="M13.0256 31.0941C13.9861 29.4315 15.3672 28.051 17.0301 27.0912C18.6931 26.1315 20.5794 25.6262 22.4994 25.6262C24.4195 25.6262 26.3057 26.1315 27.9686 27.0914C29.6316 28.0512 31.0127 29.4317 31.9731 31.0943" fill="#B2DEFE" stroke="#00406B" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
										</svg>
									</a>
								</li>
						<?php } ?>
						<li class="btn-menu menu-item menu-zindex hide-mobile">
							<a href="<?= $get_start_link ?>" aria-current="page" style="color:#fff !important">
								<?= $get_start_text; ?> 
							</a>
						</li>
						<li class="menu-item">
							<div class="entire-menu">
								<input type="checkbox" id="change-hamburguer" />
								<a class="hamburguer menu-zindex" href="#">
									<span></span>
									<label for="change-hamburguer"></label>
								</a>
								<div class="dropdown">
									<?php  
										$menu_items = wp_get_nav_menu_items(172);
										foreach( $menu_items as $menu_item ) { 
											$link = $menu_item->url;
											$title = $menu_item->title;
										?>
											<a href="<?= $link; ?>"><?= $title; ?></a>
										<?php } ?>
									<div class="hide-mobile">
										<div class="header-bagdes">
											<?php if ( !dynamic_sidebar( 'footer-6' ) ) : ?>
											<?php endif; ?>
										</div>
									</div>
									<div class="btn-menu menu-item menu-zindex hide-desktop">
										<a href="<?= $get_start_mobile_link ?>" aria-current="page" class="hide-get-start-mobile" style="color:#fff !important">
											<?= $get_start_mobile_text; ?> 
										</a>
									</div>
								</div>
							</div>
						</li>
					</ul>
				</nav>
			</div>
			<div class="hide-desktop menu-list-mobile_block">
				<div class="container">
					<ul class="menu-list menu-list-mobile" >
						<?php 
						wp_nav_menu( array(
						'theme_location' => 'mainnavigation',
						'container' => false,
						'items_wrap' => '%3$s'
						) ); 
						?>
					</ul>
				</div>
			</div>
			<?php if($treatments_menu) { ?>
				<div class="treatments_menu_block">
					<div class="treatments_menu_block_container">
						<div class="hide-mobile">
							<div class="treatments_menu_arrows">
								<p class="treatments_menu_title">Common conditions we treat</p>
								<div class="treatments_menu_arrows_box">
									<a class="treatments_menu_arrows_btn" onclick="document.getElementById('treatments_menu').scrollLeft -= 100;">
										<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M11.7492 21.5377C6.3424 21.5377 1.95933 17.1547 1.95933 11.7479C1.95933 6.34108 6.3424 1.95801 11.7492 1.95801C17.156 1.95801 21.5391 6.34108 21.5391 11.7479C21.5391 17.1547 17.156 21.5377 11.7492 21.5377Z" stroke="white" stroke-width="1.39749" stroke-linecap="round" stroke-linejoin="round"/>
											<path d="M11.748 15.6639L7.8321 11.748L11.748 7.83203" stroke="white" stroke-width="1.39749" stroke-linecap="round" stroke-linejoin="round"/>
											<path d="M15.6641 11.748H7.83217" stroke="white" stroke-width="1.39749" stroke-linecap="round" stroke-linejoin="round"/>
										</svg>
									</a>
									<a class="treatments_menu_arrows_btn" onclick="document.getElementById('treatments_menu').scrollLeft += 100;">
										<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M12.243 21.5377C17.6498 21.5377 22.0329 17.1547 22.0329 11.7479C22.0329 6.34108 17.6498 1.95801 12.243 1.95801C6.8362 1.95801 2.45312 6.34108 2.45312 11.7479C2.45312 17.1547 6.8362 21.5377 12.243 21.5377Z" stroke="white" stroke-width="1.39749" stroke-linecap="round" stroke-linejoin="round"/>
											<path d="M12.2441 15.6639L16.1601 11.748L12.2441 7.83203" stroke="white" stroke-width="1.39749" stroke-linecap="round" stroke-linejoin="round"/>
											<path d="M8.32812 11.748H16.16" stroke="white" stroke-width="1.39749" stroke-linecap="round" stroke-linejoin="round"/>
										</svg>
									</a>
								</div>
							</div>
						</div>
						<div class="text-more js-more hide-desktop common_conditions_mobile">
							<p class="treatments_menu_title">Common conditions we treat</p>
							<div class="treatments_menu_title_svg_box">
								<svg class="arrow-left" width="8" height="14" viewBox="0 0 8 14" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M1 13L7 7L1 1" stroke="#E8F5FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
								</svg>
								<svg class="arrow-down" width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M1 0.999999L7 7L13 1" stroke="#E8F5FF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
								</svg>
							</div>
						</div>
						<div class="treatments_menu_box" >
							<div class="treatments_menu" id="treatments_menu">
								<?php foreach ($treatments_menu as $key => $page) { ?>
									<?php if($page["page"] && isset($page["page"][0])) { ?>
										<div class="treatments_menu_item_box <?php if(strcmp($actual_link, get_permalink($page["page"][0]->ID)) == 0) { echo 'active';}?> ">
											<div class="treatments_menu_item">
												<a href='<?= get_permalink($page["page"][0]->ID); ?>'>
													<div class="treatments_menu_item_block">
														<div>
															<img src="<?= $page['imagem'] ?>" />
															<p><?= $page['title'] ?></p>
														</div>
														<svg width="5" height="9" viewBox="0 0 5 9" fill="none" xmlns="http://www.w3.org/2000/svg">
															<path fill-rule="evenodd" clip-rule="evenodd" d="M0.183058 0.308058C0.427136 0.0639806 0.822864 0.0639806 1.06694 0.308058L4.81694 4.05806C5.06102 4.30214 5.06102 4.69786 4.81694 4.94194L1.06694 8.69194C0.822864 8.93602 0.427136 8.93602 0.183058 8.69194C-0.0610194 8.44786 -0.0610194 8.05213 0.183058 7.80806L3.49112 4.5L0.183058 1.19194C-0.0610194 0.947864 -0.0610194 0.552136 0.183058 0.308058Z" fill="#00406B"/>
														</svg>
													</div>
												</a>
											</div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	</header>
<?php } else { ?>
	<header class="header header-paid-traffic">
		<?php if(!get_field("remove_top_bar_mobile")) { ?>
			<?php $link = get_field('header_mobile_information', "option"); if( $link ): ?>
				<?php if(!$is_app) { ?>
					<?php if(get_field('show_on_desktop_top_bar_mobile')) { ?>
						<div class="header-bar">
					<?php } else { ?>
							<div class="header-bar hide-desktop">
					<?php } ?>
						<?php if(get_field('top_bar_text_mobile')) { 
							$link = get_field('top_bar_text_mobile');
						} ?>
						<?php echo $link; ?>
					</div>
				<?php } ?>
			<?php endif;?>
		<?php } ?>
		<div class="container">
			<div class="logo">
				<img class="light-logo" src="/wp-content/uploads/2021/08/k-health-sky-blue-80px.png" />
				<img class="dark-logo" src="/wp-content/uploads/2021/08/k-health-dark-blue-80px.png" />
			</div>
		</div>
	</header>
<?php } ?>

  <div class="body">
