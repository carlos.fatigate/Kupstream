<?php
/**
 * Template Name: Sidebar Page
 * 
 * pixelperfect lt: Sample Page Template
 * 
 * @package WordPress
 * @subpackage pixelperfectlt
 */
get_header();
if ( have_posts() ):
	while ( have_posts() ) : the_post();
		?>
		<div class="page-hero">
			<div class="container">
				<h1><?php the_title() ?></h1>
			</div>
		</div>
		<div class="page-with-sidebar">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="entry lg">
							<?php the_content();  ?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="sidebar">
							<?php if ( !dynamic_sidebar( 'side1' ) ) : ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	endwhile;
endif; 
get_footer(); ?>
