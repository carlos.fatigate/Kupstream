
                <div class="<?php echo isset($col) ? $col : "col-sm-6" ?>">
                  <a href="<?php echo get_permalink() ?>" class="m-blog-item">
                    <div class="image">
                      <?php the_post_thumbnail( "medium" ); ?> 
                    </div>
                    <h3><?php the_title() ?></h3>
                  </a>
                </div>