<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'testimonials' );
$imageR = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'testimonials@2x' );
?>
<?php if (!isset($sliderR)) { ?>
<div class="<?php echo get_field("list_style") ? "col-md-6" : "col-sm-6 col-md-3" ?>">
	<?php } ?>

	<?php 
	$id = (get_field("user", get_the_ID()));

	if ($id) {
		$div = "a href='".esc_url( get_author_posts_url( $id ) )."'";
		$div2 = "a";
	} else {
		$div = "div";
		$div2 = "div";
	}
	 ?>
	<<?php echo $div ?>  class="doctor-item <?php echo get_field("list_style") ? "featured" : "" ?>">
		<div class="image">
			<img data-src="<?php echo $image[0]; ?>" data-srcset="<?php echo $imageR[0]; ?> 2x" alt="">
		</div>
		<h4><?php the_title() ?></h4>
		<?php if (get_field("role", get_the_ID())) { ?>
			<div class="role">
				<?php echo get_field("role", get_the_ID()) ?>
			</div>
		<?php } ?>
		<div>
		<?php 
			if (get_field("list_style") == "featured") {
				echo get_field("description", get_the_ID()); 
			} else {
				if (isset($sliderR)) {
					echo get_field("slider_text", get_the_ID()); 
				} else {
					
					// echo get_the_content();
				}
			}
			?>
		</div>
		</<?php echo $div2 ?>>
<?php if (!isset($sliderR)) { ?>
</div>

	<?php } ?>