<?php
/**
 * Footer
 * 
 * Remember to always include the wp_footer() call before the </body> tag
 */

?>
</div>
<footer>
	<div class="footer-flex">
		<?php if(!get_field('footer_for_paid_traffic')) { ?>
			<div class="footer footer-logo">
				<div class="container-footer">
					<div class="footer-logo-svg">
						<img src="/wp-content/uploads/2021/08/k-health-primary-blue-124px.png" alt="Khealth">
					</div>
					<!-- <p class="footer-stay-touch">Stay in touch</p>
					<div class="footer-lead-form">
						<form>
							<input type="email" placeholder="Email address"/>
							<button type="submit">
								<svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M9.9747 1.50049L18.9492 10.456L9.9747 19.4115" stroke="#A0C8E5" stroke-width="1.38341" stroke-linecap="round" stroke-linejoin="round"/>
								<path d="M1.00023 10.4814H18.9492" stroke="#A0C8E5" stroke-width="1.38341" stroke-linecap="round" stroke-linejoin="round"/>
								</svg>
							</button>
						</form>
					</div> -->
					<?php echo do_shortcode( '[social]' ); ?>
					<p class="footer-copy hide-mobile"><?php the_field("copy", "option") ?></p>
				</div>
			</div>
		<?php } ?>
		<div class="footer footer-content">
			<div class="container-footer">
				<?php if(!get_field('footer_for_paid_traffic')) { ?>
					<div class="container-footer-widget">
						<div class="footer-widget-col">
							<?php if ( !dynamic_sidebar( 'footer-1' ) ) : ?>
							<?php endif; ?>
						</div>
						<div class="footer-widget-col">
							<?php if ( !dynamic_sidebar( 'footer-2' ) ) : ?>
							<?php endif; ?>
						</div>
						<div class="footer-widget-col">
							<?php if ( !dynamic_sidebar( 'footer-3' ) ) : ?>
							<?php endif; ?>
						</div>
						<div class="footer-widget-col">
							<?php if ( !dynamic_sidebar( 'footer-4' ) ) : ?>
							<?php endif; ?>
						</div>
						<div class="footer-widget-col">
							<?php if ( !dynamic_sidebar( 'footer-5' ) ) : ?>
							<?php endif; ?>
						</div>
					</div>
				<?php } else { ?>
						<div class="hero-bg-mobile">
							<img class="footer-paid-khealth-logo" src="<?= get_field('footer_paid_khealth_logo'); ?>" />
						</div>
						<div class="footer-paid-logo">
							<div class="hero-bg">
								<img class="footer-paid-khealth-logo" src="<?= get_field('footer_paid_khealth_logo'); ?>" />
							</div>
							<?php foreach (get_field('footer_paid_logos') as $key => $logo) { ?>
								<div>
									<img class="footer-paid-brand-logo" src="<?= $logo['image'] ?>" />
								</div>
							<?php } ?>
						</div>
					<?php } ?>
				<div class="footer-flex">
					<div class="copy <?php if(get_field('footer_for_paid_traffic')) { echo 'paid_traffic'; }?>">
						<div class="hide-desktop">
							<?php if ( !dynamic_sidebar( 'footer-6' ) ) : ?>
							<?php endif; ?>
						</div>
						<ul>
							<?php  
							wp_nav_menu( array(
							'theme_location' => 'footnavigation',
							'container' => false,
							'items_wrap' => '%3$s'
							) );
							?>
						</ul>
						<p class="footer-copy hide-desktop"><?php the_field("copy", "option") ?></p>
					</div>
					<?php if(!get_field('footer_for_paid_traffic')) { ?>
						<div class="hide-mobile">
							<?php if ( !dynamic_sidebar( 'footer-6' ) ) : ?>
							<?php endif; ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</footer>

<svg class="hide" xmlns="http://www.w3.org/2000/svg" width="0" height="0">
<defs>
	<symbol id="logo" viewBox="0 0 600 212" xml:space="preserve"><path d="M103.4 0h7.1c19.9.9 39.4 7.4 55.8 18.8 20.8 14.3 36.3 36.3 42.4 60.8 5.9 22.9 3.9 47.8-5.7 69.3-9.7 22-27 40.4-48.4 51.3-13.9 7.2-29.5 11.1-45.2 11.7H0V101.5c1.3-31 16.9-61.1 41.7-79.8C59.3 8.2 81.2.5 103.4 0m-9.8 20.7c-18 2.6-34.9 11.3-47.9 24-14.2 14.1-23.5 33.2-25.4 53.1-.6 5.7-.4 11.5-.4 17.2v75.1c-.2 1.6.5 2.2 2 2H106c14 0 28-3.4 40.3-10 18.1-9.5 32.6-25.7 39.9-44.8 6.7-17 7.7-36.1 3-53.6-5.5-20.7-19.1-39.1-37.2-50.5-17.1-11-38.3-15.4-58.4-12.5zM257.1 58.9c5.9.1 11.8-.1 17.6.1.1 10.7.1 21.3 0 32 3.2-4.6 7.9-8.2 13.4-9.4 7.8-1.7 16.8-1.1 23 4.3 5.4 4.7 7.5 12.2 7.3 19.1-.2 14.9.2 29.8-.2 44.7-5.8.1-11.6 0-17.5.1-.6-14.4 0-28.7-.3-43.1-.2-3.1-.8-6.7-3.3-8.8-3.8-3.1-9.2-2.7-13.6-1.5-5.7 1.9-8.6 8-8.7 13.7-.3 13.2 0 26.4-.2 39.6-5.9.2-11.7.1-17.6.1-.5-24.2-.1-48.5-.2-72.7.2-6.1-.1-12.2.3-18.2zM467.1 58.9c5.9.1 11.7-.2 17.6.2 0 30.2.1 60.4 0 90.6-5.9.2-11.7.1-17.6 0-.1-30.3-.2-60.6 0-90.8zM538.6 58.9h17.7c.1 10.8.2 21.5 0 32.3 6.5-10.7 21.7-13.1 32.5-8 6.9 3.3 10.5 10.7 11.2 18v48.2c-5.9.6-11.9.2-17.8.2-.2-14.2.1-28.5-.1-42.7 0-4.1-1.4-8.8-5.5-10.5-4.7-1.7-10.4-1.3-14.6 1.6-4 3-5.5 8.1-5.6 12.9-.2 12.9.2 25.8-.2 38.8-5.9.1-11.7 0-17.6 0-.3-30.2-.1-60.5 0-90.8z"/><path d="M68.1 60.8c6.6-.2 13.3-.2 19.9 0 .1 13.8-.1 27.7.1 41.5 1.4-.1 2.3-1.1 3.2-2 8.1-8.2 16.4-16.4 24.5-24.6 8.7.1 17.4-.2 26 .2l1 .9c-9.5 9.2-18.7 18.6-28 28 9.4 15.5 19.5 30.7 28.7 46.4-7.6.5-15.3.1-22.9.2-7-10.5-13.1-21.7-20.4-31.9-4.1 3.9-8 8-12.1 12v19.8c-6.6.1-13.3.3-19.9-.1 0-30.2-.1-60.3-.1-90.4zM502.1 64.5c5.6-.2 11.3 0 16.9-.1.4 5.7.1 11.4.2 17.2 3.6 0 7.2-.1 10.8.1 0 4.1.1 8.2-.1 12.2h-10.8V133c-.1 1.5.5 3.1 2 3.7 2.9.4 5.9.4 8.8.6v12.8c-7.3.7-15 1.4-22-1.3-4.6-1.8-6.5-7-6.6-11.6-.1-14.4 0-28.7 0-43.1-3-.1-6 .3-9-.3-.4-3.9-.8-8.3.2-12 2.9-.3 5.9-.1 8.8-.1.1-5.5-.1-11 .1-16.5l.7-.7zM338.4 87.3c7.3-6.2 17.5-7.3 26.6-6.3 7.8.8 15.2 4.7 19.9 11 5.8 8.1 7.2 18.7 6.4 28.4h-46.1c.5 4.9 1.4 10.5 5.4 13.9 6.8 5 19.2 4.1 22.4-4.7h17.7c-.9 6.9-5.5 13.1-11.4 16.6-8 5.1-18.1 6.1-27.4 4.6-7.3-1.2-14.1-5.2-18.3-11.3-5.5-8-6.7-18-6.3-27.5.3-9.2 3.6-18.8 11.1-24.7m13.1 10.1c-3.9 2.7-5.2 7.5-6.3 11.8 9.3.1 18.5 0 27.8.1-.6-4.8-2.2-10-6.7-12.5-4.6-2.1-10.5-2.3-14.8.6zM409.6 85.6c8.4-5.3 19-5.5 28.6-4.3 8.8 1.2 18.1 7.2 18.9 16.7.3 13.9 0 27.8.1 41.7.1 2.4.2 5.1 2.3 6.7 1 .6 2.2 2.3.8 3.2-6.2.4-12.4 0-18.5.2-.8-2.7-1.3-5.4-1.7-8.2-7.9 9.6-22.8 13-33.7 6.8-12.2-7.1-11.4-27.5.6-34.4 8.8-5.4 19.8-3.4 29.2-6.9 3.2-1 4.5-5.3 2.8-8.1-.9-2.1-3.2-3-5.3-3.6-4.3-1-9.2-1.1-13 1.4-2.5 1.6-3.2 4.7-3.8 7.4-5.6.2-11.2.1-16.8 0 .4-7.1 3.2-14.7 9.5-18.6m10.5 37.3c-6 2.8-5.8 12.9.4 15.3 6.1 1.7 13.2-.6 17.2-5.6 3.1-4.5 2-10.3 2.2-15.4-6.2 3.4-13.5 2.8-19.8 5.7z"/></symbol>
	<symbol id="logo-s" viewBox="0 0 350 350" xml:space="preserve"><path d="M335.9 106.1c-12-27.8-31.2-52.4-55.3-70.6C251.7 13.4 215.6.9 179.2 0H171c-23.7.6-47.4 5.9-69 15.9-35.6 16.3-65.6 45-83.2 80.1C7 119.1.8 144.9 0 170.8V350h178.5c27.5-.6 54.8-7.6 79-20.6 23.3-12.6 43.8-30.2 59.2-51.7 20.8-28.4 32.4-63.3 33.3-98.5v-7.7c-.5-22.4-5.3-44.7-14.1-65.4zm-45.2 151.5c-14.7 20.5-34.8 37.1-57.9 47.3-18.7 8.4-39.3 12.6-59.8 12.2H35.9c-1.4.1-3.6 0-3.1-2.1 0-47-.1-94 .1-141.1.2-30 10.4-59.8 28.4-83.7 24-32.6 62.5-54.2 103-57.1 26.6-2 53.9 3.5 77.4 16.2 20.7 10.9 38.5 27.1 51.5 46.5 15.6 23.2 24.2 51.1 24 79.1.2 29.5-9.4 58.8-26.5 82.7z"/><path d="M234.2 128.3c.9-.8 1.2-1.7 1.1-2.9-1.8-.3-3.5-.4-5.3-.4-13 .1-26-.1-38.9.1-14.7 14.6-29.3 29.3-44 43.9-1.4-.5-2-1.4-1.8-2.9-.1-22 .2-44-.2-65.9-10.4-.2-20.8-.2-31.1 0-2.1-.2-1.4 2.7-1.5 3.9v142c0 1.3.1 2.5.3 3.7 10.7.3 21.5.1 32.2.1.7-10.6 0-21.2.3-31.7 0-1.3 1.2-2 1.9-2.9 6-5.9 11.9-12 17.9-17.8 1 .5 1.7 1.2 2.3 2.1 9.9 16 20 32 29.9 48 .7.9 1.1 2.6 2.6 2.4 11.9 0 23.9.3 35.8-.2.1-.4.5-1.2.6-1.5-15.4-25.2-31.3-50.1-46.9-75.2l44.8-44.8z"/></symbol>
</defs>
</svg>
<?php 
// do not remove
wp_footer();
$secrets_file = $_SERVER['HOME'] . '/files/private/secrets.json';
$pharmacy_url = '';
$segment_key = SEGMENT_KEY;
if (file_exists($secrets_file)) {
    $secrets = json_decode(file_get_contents($secrets_file), 1);
    $pharmacy_url = $secrets['pharmacy_url'];
    $segment_key = $secrets['segment_key'];
} 
?>

<link rel="stylesheet" href="<?= bloginfo( 'stylesheet_directory' ); ?>/css/intlTelInput.css">
<script src="<?= get_template_directory_uri() ?>/js/intlTelInput.js"></script>
<script src="<?= get_template_directory_uri() ?>/dist/bundle.js"></script>
<script>
	!function(){var analytics=window.analytics=window.analytics||[];if(!analytics.initialize)if(analytics.invoked)window.console&&console.error&&console.error("Segment snippet included twice.");else{analytics.invoked=!0;analytics.methods=["trackSubmit","trackClick","trackLink","trackForm","pageview","identify","reset","group","track","ready","alias","debug","page","once","off","on","addSourceMiddleware","addIntegrationMiddleware","setAnonymousId","addDestinationMiddleware"];analytics.factory=function(e){return function(){var t=Array.prototype.slice.call(arguments);t.unshift(e);analytics.push(t);return analytics}};for(var e=0;e<analytics.methods.length;e++){var key=analytics.methods[e];analytics[key]=analytics.factory(key)}analytics.load=function(key,e){var t=document.createElement("script");t.type="text/javascript";t.async=!0;t.src="https://cdn.segment.com/analytics.js/v1/" + key + "/analytics.min.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n);analytics._loadOptions=e};
        analytics._writeKey="<?= $segment_key ?>";analytics.SNIPPET_VERSION="4.13.2";
		analytics.load('<?= $segment_key ?>');
		analytics.page(window.location.pathname);
	}}();

	function showDoctor(doctor, profile_image,role) {
		$("#doctor-dots-big-img-src").remove();
		$("#doctor-dots-big-img-text").remove();
		$("#doctor-dots-big-img-role").remove();
		
		var img = $('<img />', { 
			id: 'doctor-dots-big-img-src',
			src: profile_image,
			alt: doctor
		});
		img.appendTo($('#doctor-dots-big-img'));
		$('<p id="doctor-dots-big-img-text">'+doctor+'</p> <p id="doctor-dots-big-img-role">'+role+'</p>').appendTo('#doctor-dots-big-img-text-box');
	}

</script>

<script>
$("#jquery-intl-phone").intlTelInput({
    geoIpLookup: function(callback) {
    $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
        var countryCode = (resp && resp.country) ? resp.country : "";
        callback(countryCode);
    });
    }
});
const tabLinksSearch = document.querySelectorAll("#drugs-nav-mobile");
var $wrapper = document.querySelector('.menu-item-has-children')
if($wrapper){
    tempHtml = $wrapper.innerHTML,
    newHtml = '<span id="source-arrow"><i class="arrow down"></i></span>';
    tempHtml = tempHtml + newHtml;
    $wrapper.innerHTML = tempHtml;

    if(tabLinksSearch) {
        $(document).ready(function() {
            $('#drugs-nav-mobile .drugs-tabs-description').hide();
            $('#drugs-nav-mobile .dropdown-drugs-link').click(function(e) {
                e.preventDefault();
                
                var $menuItem = $(this).next('.drugs-tabs-description');
                var $menuClicked = $(this).next('.drugs-arrow-down');
                var classes = $(this).is( ".drugs-tabs-active" );

                $menuItem.slideToggle();

                if($(this).hasClass("drugs-tabs-active")){
                    $(this).removeClass("drugs-tabs-active");
                } else {
                    $(".drugs-tabs-active").removeClass("drugs-tabs-active");
                    $(this).addClass("drugs-tabs-active");
                }
                $('#drugs-nav-mobile .drugs-tabs-description').not($menuItem).slideUp();
            });
        });
    }
}
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("drugs-slides");
    if(slides.length){
        if (n > slides.length) {slideIndex = 1}    
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";  
        }
        slides[slideIndex-1].style.display = "block";  
        
        if(n == 1) {
            document.getElementById("drugs-slides-prev").style.display = "none";
        } else {
            document.getElementById("drugs-slides-prev").style.display = "initial";
        }
        
        if(n == slides.length) {
            document.getElementById("drugs-slides-next").style.display = "none";
        } else {
            document.getElementById("drugs-slides-next").style.display = "initial";
        }
    }
}
const tabLinks = document.querySelectorAll(".drugs-tabs a");
const tabPanels = document.querySelectorAll(".drugs-tabs-panel");

for (let el of tabLinks) {
    el.addEventListener("click", e => {
        e.preventDefault();

        document.querySelector(".drugs-tabs li.active").classList.remove("active");
        document.querySelector(".drugs-tabs-panel.active").classList.remove("active");

        const parentListItem = el.parentElement;
        parentListItem.classList.add("active");
        const index = [...parentListItem.parentElement.children].indexOf(parentListItem);

        const panel = [...tabPanels].filter(el => el.getAttribute("data-index") == index);
        panel[0].classList.add("active");
    });
}


let dose = document.getElementById("dose")

if(dose){
    document.getElementById("related_term_name").addEventListener("change", getSelectioOptions);
    document.getElementById("packaging").addEventListener("change", checkCalculator);
    dose.addEventListener("change", checkCalculator);
    document.getElementById("quantity").addEventListener("change", checkCalculator);
    
    function checkCalculator() {
        var send_data = {
            'get_drug_price_nonce': js_global.get_drug_price_nonce,
            'related_term_name': document.getElementById("related_term_name").value,
            'packaging': document.getElementById("packaging").value,
            'dose': document.getElementById("dose").value,
            'quantity': document.getElementById("quantity").value,
            'action': 'get_drug_price'
        }
        jQuery.ajax({
            url: js_global.xhr_url,
            type: 'POST',
            data: send_data,
            dataType: 'JSON',
            success: function(response) {
                if (response == '401'  ){
                    console.log('Invalid request')
                }
                else if (response == 402) {
                    document.getElementById("drug-calculator-dose").innerHTML = document.getElementById("dose").value;
                    document.getElementById("drug-calculator-tablet").innerHTML = document.getElementById("packaging").value;
                    document.getElementById("drug-calculator-qty").innerHTML = "Quantity: " + document.getElementById("quantity").value;
                    document.getElementById("drug-calculator-value").innerHTML = '';
                } else {
                    document.getElementById("drug-calculator-dose").innerHTML = "<p>" + response.dose + "</p>";
                    document.getElementById("drug-calculator-tablet").innerHTML = response.packaging;
                    document.getElementById("drug-calculator-qty").innerHTML = "Quantity: " + response.quantity;
                    document.getElementById("drug-calculator-value").innerHTML = response.price;
                }
            },
            error: function(jq,status,message) {
                console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
            }
        });
    }
    let href_packaging = window.location.href.indexOf('packaging')
    let href_dose = window.location.href.indexOf('dose')
    let href_quantity = window.location.href.indexOf('quantity')
    let href_calculator = window.location.href.indexOf('calculator')

    if(href_packaging != -1 && href_dose != -1 && href_quantity != -1 && href_calculator != -1){
        $("html, body").animate({
            scrollTop: $('#drug-price').offset().top-200
        }, 0);
    }
}

function getSelectioOptions () {
    
    var send_data = {
        'get_select_option_calculator_nonce': js_global.get_select_option_calculator_nonce,
        'related_term_name': document.getElementById("related_term_name").value,
        'action': 'get_select_option_calculator'
    }
    jQuery.ajax({
        url: js_global.xhr_url,
        type: 'POST',
        data: send_data,
        dataType: 'JSON',
        success: function(response) {
            if (response == '401'  ){
                console.log('Requisição inválida')
            }
            else if (response == 402) {
                console.log('Post not found.')
            } else {
                let selectObjectPackaging = document.getElementById("packaging");
                selectObjectPackaging.parentNode.innerHTML = '';

                let selectObjectDose = document.getElementById("dose");
                selectObjectDose.parentNode.innerHTML = '';

                let selectObjectQuantity = document.getElementById("quantity");
                selectObjectQuantity.parentNode.innerHTML = '';

                var selPackaging = document.createElement("select");
                selPackaging.setAttribute('id', 'packaging');

                var opt = document.createElement('option');
                opt.value = '';
                opt.innerHTML = "Packaging";
                selPackaging.add(opt);
                for (var i = 0; i < response.packaging_array.length; i++){
                    var opt = document.createElement('option');
                    opt.value = response.packaging_array[i];
                    opt.innerHTML = response.packaging_array[i];
                    if(response.packaging_array[i] == response.packaging_get) {
                        opt.setAttribute('selected', 'selected');
                    }
                    selPackaging.add(opt);
                }
                document.getElementById("packaging-box").appendChild(selPackaging);

                var selDose = document.createElement("select");
                selDose.setAttribute('id', 'dose');
                var opt = document.createElement('option');
                opt.value = '';
                opt.innerHTML = "Dose";
                selDose.add(opt);
                for (var i = 0; i < response.doses.length; i++){
                    var opt = document.createElement('option');
                    opt.value = response.doses[i];
                    opt.innerHTML = response.doses[i];
                    if(response.doses[i] == response.dose_get) {
                        opt.setAttribute('selected', 'selected');
                    }
                    selDose.add(opt);
                }
                document.getElementById("dose-box").appendChild(selDose);

                var sel = document.createElement("select");
                sel.setAttribute('id', 'quantity');

                var opt = document.createElement('option');
                opt.value = '';
                opt.innerHTML = "Quantity";
                sel.add(opt);
                for (var i = 0; i < response.quantities.length; i++){
                    var opt = document.createElement('option');
                    opt.value = response.quantities[i];
                    opt.text = response.quantities[i];
                    if(response.quantities[i] == response.quantity_get) {
                        opt.setAttribute('selected', 'selected');
                    }
                    sel.add(opt);
                }
                document.getElementById("quantity-box").appendChild(sel);
                document.getElementById("drug-calculator-name").innerHTML = document.getElementById("related_term_name").value

                createCustomSelectCalculator("calculator-box", checkCalculator) 
                checkCalculator()
            }
        },
        error: function(jq,status,message) {
            console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
        }
    });
}

$(".learn-more-link").click(function () {
    if($(".drugs-tabs-panel.active.open").length == 1) {
        $(".drugs-tabs-panel.active").removeClass('open');
        $(".learn_more_drugs_info").show();
        $(".learn_less_drugs_info").hide();
    } else {
        $(".drugs-tabs-panel.active").addClass('open');
        $(".learn_more_drugs_info").hide();
        $(".learn_less_drugs_info").show();
    }
});

$(".drugs-tabs li").click(function () {
    $(".drugs-tabs-panel").removeClass('open');
    $(".learn_more_drugs_info").show();
    $(".learn_less_drugs_info").hide();
    if($(".drugs-tabs-panel-related.active").length == 1) {
        $(".learn_more_drugs_info").hide();
        $(".learn_less_drugs_info").hide();
    }
});

function toggleDropdown() {
    document.getElementById("toggle-dropdown-drugs-content").classList.toggle("show");
    document.getElementById("toggle-dropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
    if (!event.target.matches('.toggle-dropdown')) {
        var dropdowns = document.getElementsByClassName("toggle-dropdown-drugs-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }

        var dropdowns = document.getElementsByClassName("oggle-dropdown");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}

var x, i, j, l, ll, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
    selElmnt = x[i].getElementsByTagName("select")[0];
    ll = selElmnt.length;
    /* For each element, create a new DIV that will act as the selected item: */
    a = document.createElement("DIV");
    a.setAttribute("class", "drug-price-select");
    a.setAttribute("style", "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)");
    a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
    x[i].appendChild(a);
    /* For each element, create a new DIV that will contain the option list: */
    b = document.createElement("DIV");
    b.setAttribute("class", "select-items select-hide");
    for (j = 1; j < ll; j++) {
        /* For each option in the original select element,
        create a new DIV that will act as an option item: */
        c = document.createElement("DIV");
        c.innerHTML = selElmnt.options[j].innerHTML;
        c.addEventListener("click", function(e) {
            /* When an item is clicked, update the original select box,
            and the selected item: */
            var y, i, k, s, h, sl, yl;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            sl = s.length;
            h = this.parentNode.previousSibling;
            // s.value = 
            for (i = 0; i < sl; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                    s.options[i].selected = 'selected'
                    s.selectedIndex = i;
                    h.innerHTML = this.innerHTML;
                    y = this.parentNode.getElementsByClassName("same-as-selected");
                    yl = y.length;
                    for (k = 0; k < yl; k++) {
                        y[k].removeAttribute("class");
                    }
                    this.setAttribute("class", "same-as-selected");
                    checkCalculator();
                    break;
                }
            }
            h.click();
        });
        b.appendChild(c);
    }
    x[i].appendChild(b);
    a.addEventListener("click", function(e) {
        /* When the select box is clicked, close any other select boxes,
        and open/close the current select box: */
        e.stopPropagation();
        closeAllSelect(this);
        this.setAttribute("style", "background-image:url(<?= get_field('arrow_up_drugs_template','options'); ?>)");
        if(this.classList.contains('select-arrow-active')) {
            this.setAttribute("style", "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)");
        }
        this.nextSibling.classList.toggle("select-hide");
        this.classList.toggle("select-arrow-active");
    });
}

function createCustomSelectCalculator(elmentName, functionElement) {
    let x, i, j, l, ll, selElmnt, a, b, c;
    /* Look for any elements with the class "custom-select": */
    x = document.getElementsByClassName(elmentName);
    l = x.length;
    for (i = 0; i < l; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        ll = selElmnt.length;
        /* For each element, create a new DIV that will act as the selected item: */
        a = document.createElement("DIV");
        a.setAttribute("class", "drug-price-select");
        a.setAttribute("style", "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /* For each element, create a new DIV that will contain the option list: */
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < ll; j++) {
            /* For each option in the original select element,
            create a new DIV that will act as an option item: */
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function(e) {
                /* When an item is clicked, update the original select box,
                and the selected item: */
                var y, i, k, s, h, sl, yl;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                sl = s.length;
                h = this.parentNode.previousSibling;
                // s.value = 
                for (i = 0; i < sl; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.options[i].selected = 'selected'
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        yl = y.length;
                        for (k = 0; k < yl; k++) {
                            y[k].removeAttribute("class");
                        }
                        this.setAttribute("class", "same-as-selected");
                        functionElement()
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            /* When the select box is clicked, close any other select boxes,
            and open/close the current select box: */
            e.stopPropagation();
            closeAllSelect(this);
            this.setAttribute("style", "background-image:url(<?= get_field('arrow_up_drugs_template','options'); ?>)");
            if(this.classList.contains('select-arrow-active')) {
                this.setAttribute("style", "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)");
            }
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }
}

$(function() { 
    createCustomSelectCalculator("name-box", getSelectioOptions) 
    createCustomSelectCalculator("calculator-box", checkCalculator) 
});

function closeAllSelect(elmnt) {
    /* A function that will close all select boxes in the document,
    except the current select box: */
    var x, y, i, xl, yl, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("drug-price-select");
    xl = x.length;
    yl = y.length;
    
    for (i = 0; i < yl; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
            if(y[i].classList.contains('select-arrow-active')) {
                y[i].setAttribute("style", "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)");
            }
        } else {
            y[i].classList.remove("select-arrow-active");
            y[i].setAttribute("style", "background-image:url(<?= get_field('arrow_down_drugs_template','options'); ?>)");
        }
    }
    for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);

if(document.getElementById("drug-lp-dose")){
    var dados_envio = {
        'get_drug_price_nonce': js_global.get_drug_price_nonce,
        'related_term_name': document.getElementById("drug-name-array-hero").value,
        'packaging': document.getElementById("packaging-hero-get").value,
        'dose': document.getElementById("dose-hero-get").value,
        'quantity': document.getElementById("quantity-hero-get").value,
        'action': 'get_drug_price'
    }
    jQuery.ajax({
        url: js_global.xhr_url,
        type: 'POST',
        data: dados_envio,
        dataType: 'JSON',
        success: function(response) {
            if (response == '401'  ){
                console.log('Requisição inválida')
            }
            else if (response == 402) {
                console.log('Post not found.')
            } else {
                document.getElementById("drug-lp-dose").innerHTML = response.dose + " ";
                document.getElementById("drug-lp-qty").innerHTML = response.quantity + " ";
                document.getElementById("drug-lp-value").innerHTML = "$" + response.price;
            }
        },
        error: function(jq,status,message) {
            console.log('A jQuery error has occurred. Status: ' + status + ' - Message: ' + message);
        }
    });
}

$(document).on('click', ".doctors-leader-read-more-prev", function (e) {
    e.preventDefault()

    $(this).toggleClass("").parent().slideToggle(300, function() {}); 
    $(this).parent().prev().removeClass("active")
    return false;
});

function closeSearch() {
	$( ".search-drugs-response" ).empty();
	$('.search-drugs-box').removeClass( "active" );
	$("#search").val("")
	$('.search-drugs-response').removeClass( "active" );
}

$('#search').on("click", function () {
	$('.search-drugs-box').addClass( "active" );
})

$(document).mouseup(function(e) 
{
    var container = $(".search-drugs-response");

    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        closeSearch();
    }
});

$("#search").keyup(function() {
    var valor = $(this).val().length;
    var searchParam = $(this).val();
    if (valor >= 3) {
		$.ajax({
            url: "<?= $pharmacy_url; ?>" + $(this).val(),
            type: "GET",
            crossDomain: true,
            contentType: 'application/json',
			dataType:'json',
			responseType:'application/json',
			xhrFields: {
				withCredentials: false
			},
			headers: {
				'Access-Control-Allow-Credentials' : true,
				'Access-Control-Allow-Origin':'*',
				'Access-Control-Allow-Methods':'GET',
				'Access-Control-Allow-Headers':'application/json',
			},
            success:function(json) {
				$('.search-drugs-response').addClass("active");
				$(".search-drugs-response").empty();
				let responseArray = [];
				let genericIdInArray = false
				json.results.forEach(function(drug, index ) {
					genericIdInArray = false
					if(index == 0){
						responseArray.push(drug)
					}else {
						responseArray.forEach(function(medicine, index) {
							if(medicine.generic_id == drug.generic_id) {
								medicine.brand_name += ", " + drug.brand_name
								genericIdInArray = true
							}
						})
						if(!genericIdInArray){
							responseArray.push(drug)
						}
					}
				});      
				$(".search-drugs-response").append("<div class='drug-link-pharmacy-box-header'><div><p class='drug-link-pharmacy-header'>Results (" + responseArray.length + ")</p></div> <div><p class='drug-link-pharmacy-header'></p></div></div>");
				responseArray.sort(function (a, b) {
						if (a.generic_name > b.generic_name) {
							return 1;
						}
						if (a.generic_name < b.generic_name) {
							return -1;
						}
						return 0;
					})
				responseArray.forEach(function(drug, index ) {
					let link = json._links.transfer.replace("{generic_id}", drug.generic_id);
					let reg = new RegExp(searchParam, 'gi');
					let final_str = drug.generic_name.replace(reg, function(str) {return '<b>'+str+'</b>'});
					final_str += " (" + drug.brand_name.replace(reg, function(str) {return '<b>'+str+'</b>'}) + ")";
					$( ".search-drugs-response" ).append( "<div><a target='_blank' href='" + link + "'><div class='drug-link-pharmacy-box'> <div><p class='drug-link-pharmacy-search'>" + final_str + "</p></div> <div><p>$" + drug.base_price/100 + "+</p></div> </div></a></div>" );
				});                   
            },
            error:function(error) {
                console.log('message Error' + JSON.stringify(error));
            }  
        });  
    } else {
		if(valor === 0) {
			$( ".search-drugs-response" ).empty();
			$('.search-drugs-box').removeClass( "active" );
			$('.search-drugs-response').removeClass( "active" );
		}
	}
});

$( document ).ready(function() {
	setTimeout(function(){ 
		var tags = document.querySelectorAll('a');
		for (i = 0; i < tags.length; i++) {
			var link = tags[i].href;
			if (link.indexOf("{outside_tracking_id}") != -1) tags[i].href= link.replace("{outside_tracking_id}", analytics.user().anonymousId());
		}
	}, 1000);
});

$( document ).ready(function() {
    $( ".flickity-prev-next-button" ).empty();
    $( ".flickity-prev-next-button.previous" ).append('<svg width="51" height="52" viewBox="0 0 51 52" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M25.5 49.2681C12.5213 49.2681 2 38.7468 2 25.7681C2 12.7894 12.5213 2.26807 25.5 2.26807C38.4787 2.26807 49 12.7894 49 25.7681C49 38.7468 38.4787 49.2681 25.5 49.2681Z" fill="white" stroke="#2B8FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M25.6666 35.1209L16.3333 25.8285L25.6666 16.5361" stroke="#2B8FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M35 25.8285H16.3333" stroke="#2B8FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>')
    $( ".flickity-prev-next-button.next" ).append( '<svg width="56" height="56" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M28.0003 4.66667C40.887 4.66667 51.3337 15.1134 51.3337 28C51.3337 40.8867 40.887 51.3333 28.0003 51.3333C15.1137 51.3333 4.66699 40.8867 4.66699 28C4.66699 15.1134 15.1137 4.66667 28.0003 4.66667Z" fill="white" stroke="#2B8FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M28 18.6667L37.3333 28L28 37.3333" stroke="#2B8FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M18.667 28L37.3337 28" stroke="#2B8FFF" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>' );
});

var divDropdown = document.querySelector(".dropdown");
var divHamburguer = document.querySelector(".hamburguer");
$(document).on("click", function(e) {
    var out = !divDropdown.contains(e.target) && !divHamburguer.contains(e.target);
    var cb = document.getElementById('change-hamburguer');
    if(out && cb.checked) {
        cb.click();
        $('#change-hamburguer').prop('checked', true);
    }
});

$(document).ready(function(){
    $(document).on('click', ".faq-title-image", function () {
        if ($(this).hasClass("active")) {
            $(this).toggleClass("active").next().slideToggle();
        } else {
            $('.faq_image_question_answer').css('display', 'none');
            $(".faq-title-image").removeClass("active")
            $(this).toggleClass("active").next().slideToggle();
        }
        return false;
    });

    $(document).on('click', ".see_all_link_uti_medication", function () {
        $(this).toggleClass("active")
        $('#drugs_uti_medication_box_see_more').toggleClass("active").next().slideToggle(300, function() {
            lazyload();
        });
        lazyload();
        return false;
    });
    delWidth = $('del').width();

    $( "del" ).append( "<div class='text-decoration-hero'></div>" );
    $('.text-decoration-hero').animate({"width" : delWidth+"px"}, 1000);

});

$(document).ready(function(){
    setTimeout(function(){ 
        $("#first-doctor-dots").trigger("click");
    }, 1000);

    $(document).on('click', ".js-more-testimonial", function () {
        

        $( ".testimonial_content.active" ).each(function( index ) {
            let parent = $(this).parent().parent().parent().parent().parent().parent()
            let testimonial_content = $(this)
            let testimonial_height = testimonial_content.height()
            
            testimonial_content.removeClass('active');
            $(this).parent().parent().removeClass('more-testimonial-active')

            let flickity_height = parent.height()
            let new_heigth = flickity_height - testimonial_height + 60
            parent.height(new_heigth)
        });

        let testimonial_content = $(this).prev()
        testimonial_content.addClass('active');
        $(this).parent().parent().addClass('more-testimonial-active')

        let testimonial_height = testimonial_content.height()
        let flickity_height = $(this).parent().parent().parent().parent().parent().parent().height()
        let new_heigth = testimonial_height + flickity_height - 60
        $(this).parent().parent().parent().parent().parent().parent().height(new_heigth)

        return false;
    });

    $(document).on('click', ".js-less-testimonial", function () {
        let parent = $(this).parent().parent().parent().parent().parent().parent()
        let testimonial_content = $(this).prev().prev()
        let testimonial_height = testimonial_content.height()

        testimonial_content.removeClass('active');
        $(this).parent().parent().removeClass('more-testimonial-active')

        let flickity_height = parent.height()
        
        let new_heigth = flickity_height - testimonial_height + 60
        parent.height(new_heigth)

        return false;
    });

    $(document).on('click', ".js-more-carousel", function () {
        $( ".read_more_team_carousel.active" ).each(function( index ) {
            description_height = $(this).next().height()
            flickity_height = $(this).parent().parent().parent().parent().height()
            new_heigth = flickity_height - description_height

            $(this).parent().parent().parent().parent().height(new_heigth)

            $(this).toggleClass("").next().slideToggle(300, function() {}); 
            $(this).removeClass("active")
        });

        description_height = $(this).next().height()
        $(this).next().height(description_height)
        flickity_height = $(this).parent().parent().parent().parent().height()
        new_heigth = description_height + flickity_height

        $(this).toggleClass("active").next().slideToggle(300, function() {
            $(this).parent().parent().parent().parent().height(new_heigth)
        });
        return false;
    });

    $(document).on('click', ".js-more-carousel-more-prev", function (e) {
        e.preventDefault()
        description_height = $(this).parent().height()
        flickity_height = $(this).parent().parent().parent().parent().height()
        new_heigth = flickity_height - description_height

        $(this).parent().parent().parent().parent().parent().height(new_heigth)

        $(this).toggleClass("").parent().slideToggle(300, function() {}); 
        $(this).parent().prev().removeClass("active")
        return false;
    });
});
</script>

</body>
</html>