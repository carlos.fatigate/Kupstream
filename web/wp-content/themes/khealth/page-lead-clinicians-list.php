<?php
/**
 * Template Name: Lead Clinicians List Page
 * 
 * 
 * @package WordPress
 */


$args = array(
    'post_type' => 'doctors',
    'posts_per_page' => '-1',
	'post_status' => 'publish',
    'order' => 'ASC',
    'orderby' => 'title',
);

$posts_array = new WP_Query( $args );
$letter = -1;
$key = 0 ;
get_header();
if ( have_posts() ):
	while ( have_posts() ) : the_post(); ?>
		<div class="container" style="padding-bottom:50px">
			<h1 class="page-lead-clinicians-title"><?php the_title() ?></h1>
			<div class="hide-mobile">
				<div class="row">
					<?php while($posts_array->have_posts()) : $posts_array->the_post(); ?>
						<?php if($letter == -1 && get_the_title() != "") { ?>
							<div class="col-md-3">
								<p class="page-lead-clinicians-letter"><?= substr(get_the_title(), 0, 1); ?></p>
								<?php $letter = substr(get_the_title(), 0, 1);?>
						<?php } ?>
						<?php if($letter != substr(get_the_title(), 0, 1) && $key > 0 && get_the_title() != "") { ?>
							</div>
							<div class="col-md-3">
								<p class="page-lead-clinicians-letter"><?= substr(get_the_title(), 0, 1); ?></p>
								<?php $letter = substr(get_the_title(), 0, 1);?>
						<?php } ?>
						<a class="page-lead-clinicians-link" href="<?= esc_url(get_permalink($post->ID)); ?>">
							<?= get_the_title(); ?>
						</a>
						<?php $key++; ?>
					<?php endwhile;wp_reset_query(); ?>
					</div>
				</div>
			</div>
			<?php $letter = -1; ?>
			<div class="hide-desktop">
				<?php while($posts_array->have_posts()) : $posts_array->the_post(); ?>
					<?php if($letter == -1 && get_the_title() != "") { ?>
						<div class="page-lead-clinicians-letter-mobile">
							<div class="text-more js-more page-lead-clinicians-btn">
								<p class="page-lead-clinicians-letter"><?= substr(get_the_title(), 0, 1); ?></p>
								<?php $letter = substr(get_the_title(), 0, 1);?>
								<svg class="minus-svg" width="16" height="2" viewBox="0 0 16 2" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd" d="M0 1C0 0.447715 0.447715 0 1 0H15C15.5523 0 16 0.447715 16 1C16 1.55228 15.5523 2 15 2H1C0.447715 2 0 1.55228 0 1Z" fill="#00406B"/>
								</svg>
								<svg class="plus-svg" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd" d="M12 4C12.5523 4 13 4.44772 13 5V19C13 19.5523 12.5523 20 12 20C11.4477 20 11 19.5523 11 19V5C11 4.44772 11.4477 4 12 4Z" fill="#00406B"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M4 12C4 11.4477 4.44772 11 5 11H19C19.5523 11 20 11.4477 20 12C20 12.5523 19.5523 13 19 13H5C4.44772 13 4 12.5523 4 12Z" fill="#00406B"/>
								</svg>

							</div>
							<div style="display:none">
					<?php } ?>
					<?php if($letter != substr(get_the_title(), 0, 1) && $key > 0 && get_the_title() != "" ) { ?>
						</div>
						</div>
						<div class="page-lead-clinicians-letter-mobile">
							<div class="text-more js-more page-lead-clinicians-btn">
								<p class="page-lead-clinicians-letter"><?= substr(get_the_title(), 0, 1); ?></p>
								<?php $letter = substr(get_the_title(), 0, 1);?>
								<svg class="minus-svg" width="16" height="2" viewBox="0 0 16 2" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd" d="M0 1C0 0.447715 0.447715 0 1 0H15C15.5523 0 16 0.447715 16 1C16 1.55228 15.5523 2 15 2H1C0.447715 2 0 1.55228 0 1Z" fill="#00406B"/>
								</svg>
								<svg class="plus-svg" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd" d="M12 4C12.5523 4 13 4.44772 13 5V19C13 19.5523 12.5523 20 12 20C11.4477 20 11 19.5523 11 19V5C11 4.44772 11.4477 4 12 4Z" fill="#00406B"/>
								<path fill-rule="evenodd" clip-rule="evenodd" d="M4 12C4 11.4477 4.44772 11 5 11H19C19.5523 11 20 11.4477 20 12C20 12.5523 19.5523 13 19 13H5C4.44772 13 4 12.5523 4 12Z" fill="#00406B"/>
								</svg>

							</div>
							<div style="display:none">
					<?php } ?>
					<a class="page-lead-clinicians-link" href="<?= esc_url(get_permalink($post->ID)); ?>">
						<?= get_the_title(); ?>
					</a>
				<?php endwhile;wp_reset_query(); ?>
				</div>
				</div>
			</div>
			<?php the_content();  ?>
		</div>
	<?php endwhile;
endif; 
get_footer(); ?>
