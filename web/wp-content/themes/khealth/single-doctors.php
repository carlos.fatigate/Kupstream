<?php
/*
 * pixelperfect lt: Page
 * 
 * @package WordPress
 * @subpackage pixelperfectlt
 */

$board_certifications = get_field('doctor_biography_board_certifications');
$undergraduate_degree = get_field('doctor_biography_undergraduate_degree');
$undergraduate_school = get_field('doctor_biography_undergraduate_school');
$medical_degree = get_field('doctor_biography_medical_degree');
$medical_school = get_field('doctor_biography_medical_school');
$residency = get_field('doctor_biography_residency');
$other_degrees = get_field('doctor_biography_other_degrees');

$news_block_description = get_field('in_the_news_block_description');
$the_news = get_field('doctor_biography_in_the_news');
$awards = get_field('doctor_biography_awards');
$doctor_position = get_field('doctor_biography_doctor_position');

$faq_doctors = get_field('faq_doctors' , 'options');

get_header(); ?>
<div id="doctor-box">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="doctor-box">
					<img class="doctor-box-profile" src="<?= get_field('doctor_biography_image_profile'); ?>" alt="<?php the_title(); ?>"/>
					<div class="doctor-box-info">
						<p class="doctor-box-title"><?php the_title(); ?></p>
						<?php if($doctor_position) { ?>
							<p class="doctor-box-type"><?= $doctor_position; ?></p>
						<?php } ?>
						<p class="doctor-box-local">
							<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
								viewBox="0 0 368.16 368.16" style="enable-background:new 0 0 368.16 368.16;" xml:space="preserve">
								<g>
									<g>
										<g>
											<path d="M184.08,0c-74.992,0-136,61.008-136,136c0,24.688,11.072,51.24,11.536,52.36c3.576,8.488,10.632,21.672,15.72,29.4
												l93.248,141.288c3.816,5.792,9.464,9.112,15.496,9.112s11.68-3.32,15.496-9.104l93.256-141.296
												c5.096-7.728,12.144-20.912,15.72-29.4c0.464-1.112,11.528-27.664,11.528-52.36C320.08,61.008,259.072,0,184.08,0z
												M293.8,182.152c-3.192,7.608-9.76,19.872-14.328,26.8l-93.256,141.296c-1.84,2.792-2.424,2.792-4.264,0L88.696,208.952
												c-4.568-6.928-11.136-19.2-14.328-26.808C74.232,181.816,64.08,157.376,64.08,136c0-66.168,53.832-120,120-120
												c66.168,0,120,53.832,120,120C304.08,157.408,293.904,181.912,293.8,182.152z"/>
											<path d="M184.08,64.008c-39.704,0-72,32.304-72,72c0,39.696,32.296,72,72,72c39.704,0,72-32.304,72-72
												C256.08,96.312,223.784,64.008,184.08,64.008z M184.08,192.008c-30.872,0-56-25.12-56-56s25.128-56,56-56s56,25.12,56,56
												S214.952,192.008,184.08,192.008z"/>
										</g>
									</g>
								</g>
							</svg>
							<?= get_field('doctor_biography_state'); ?>
						</p>
						<p class="doctor-box-testimonial"><?= get_field('doctor_biography_k_quote'); ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div>
					<h2 class="doctor-info-title">Biography</h2>
					<div class="doctor-info-biography">
						<?php the_content();?>
					</div>
				</div>
				<div>
					<h2 class="doctor-info-title">Education + Awards</h2>
					<div class="remove-row-margin">
						<div class="row">
							<?php if($board_certifications) { ?>
								<div class="col-md-4">
									<p class="doctor-education-title">Board Certifications</p>
									<p class="doctor-education-info"><?= $board_certifications?></p>
								</div>
							<?php } ?>
							<?php if($undergraduate_degree) { ?>
								<div class="col-md-4">
									<p class="doctor-education-title">Undergraduate Degree</p>
									<p class="doctor-education-info"><?= $undergraduate_degree?></p>
								</div>
							<?php } ?>
							<?php if($undergraduate_school) { ?>
								<div class="col-md-4">
									<p class="doctor-education-title">Undergraduate School</p>
									<p class="doctor-education-info"><?= $undergraduate_school?></p>
								</div>
							<?php } ?>
							<?php if($medical_degree) { ?>
								<div class="col-md-4">
									<p class="doctor-education-title">Medical Degree</p>
									<p class="doctor-education-info"><?= $medical_degree?></p>
								</div>
							<?php } ?>
							<?php if($medical_school) { ?>
								<div class="col-md-4">
									<p class="doctor-education-title">Medical School</p>
									<p class="doctor-education-info"><?= $medical_school?></p>
								</div>
							<?php } ?>
							<?php if($residency ) { ?>
								<div class="col-md-4">
									<p class="doctor-education-title">Residency</p>
									<p class="doctor-education-info"><?= $residency ?></p>
								</div>
							<?php } ?>
							<?php if($other_degrees) { ?>
								<div class="col-md-4">
									<p class="doctor-education-title">Other Degrees / Training</p>
									<p class="doctor-education-info"><?= $other_degrees; ?></p>
								</div>
							<?php } ?>
						</div>
					</div>
					<?php if($awards) { ?>
						<div class="remove-row-margin">
							<div class="row">
								<?php foreach ($awards as $key => $award) { ?>
									<div class="col-md-4">
										<p class="doctor-education-title"><?= $award['title']; ?></p>
										<p class="doctor-education-info"><?= $award['description']; ?></p>
									</div>
								<?php } ?>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php  if(get_field('doctor_biography_is_author')) {
				$args = array(
					'author'        =>  get_field('doctor_biography_author'),
					'orderby'       =>  'post_date',
					'order'         =>  'DESC',
					'posts_per_page' => 10
					);
				$current_user_posts = get_posts( $args );
				if($current_user_posts) {
			?>
				<div>
					<h2 class="doctor-more-post-title">
						More posts from <?php the_title(); ?>
					</h2>
					<div class="slider-3-items row ">
						<?php foreach ($current_user_posts as $key => $current_post) { ?>
							<div class="col-12 col-sm-6 col-md-4 slide-item">
								<div class="doctor-post-card">
									<a href="<?= get_permalink( $current_post->ID ) ?>">
										<div class="doctor-post-image" style="background-image: url(<?= get_the_post_thumbnail_url($current_post->ID, 'full');; ?>)"></div>
										<p class="doctor-post-title"><?= get_the_title($current_post->ID); ?></p>
									</a>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>
			<?php } 
		} ?>
		<?php if($news_block_description) { ?>
			<div>
				<h2 class="doctor-news-title">
					In the news
				</h2>
				<p class="doctor-news-info">
					<?= $news_block_description; ?>
				</p>
				<?php foreach ($the_news as $key => $new) { ?>
					<div class="doctor-news-event">
						<a href="<?= $new['link']; ?>">
							<p class="doctor-news-event-title"><?= $new['title']; ?> <span><?= $new['date']; ?></span></p>
							<p class="doctor-news-event-description"><?= $new['description']; ?> </p>
						</a>
					</div>
				<?php } ?>
			</div>
		<?php } ?>
		<?php if($faq_doctors) { ?>
			<div>
				<div id="faq-block" class="section-faq">
					<div class="drugs-template">
						<?php if ($faq_doctors["title"]) { ?>
							<h2 class="section-head">
								<?= $faq_doctors["title"] ?>
							</h2>
						<?php } ?>
						<div class="row">
							<div class="col-12 col-md-10 drugs-template-faq">
								<?php foreach ($faq_doctors['faqs'] as $key => $faqs) {
									$doctor_name = get_the_title($current_post->ID);
									$doctor_name = explode(',', $doctor_name);
									$doctor_name = explode(' ', $doctor_name[0]);
									$profile = '<a href="'.get_permalink($current_post->ID).'">Profile</a>';

									$question = str_replace("{first.name}", $doctor_name[0], $faqs['question']);
									$question = str_replace("{last.name}", $doctor_name[1], $question);
									$question = str_replace("{medical.degree}", $medical_degree, $question);
									$question = str_replace("{board.certifications}", $$board_certifications, $question);
									$answer = str_replace("{profile.link}", $profile, $question);

									$answer = str_replace("{first.name}", $doctor_name[0], $faqs['answer']);
									$answer = str_replace("{last.name}", $doctor_name[1], $answer);
									$answer = str_replace("{medical.degree}", $medical_degree, $answer);
									$answer = str_replace("{board.certifications}", $$board_certifications, $answer);
									$answer = str_replace("{profile.link}", $profile, $answer);
									
									$faq_ld = array(
												"@type" => "Question",
												"name" => $question,
												"acceptedAnswer" => array(
													"@type" => "Answer",
													"text" => $answer
												)
											);
									$faq_array_ld[] = $faq_ld; ?>
									<div class="col-md-12">
										<div class="faq-item">
											<div class="faq-title">
												<?= $question ?>
												<div class="toggler"></div>
											</div>
											<div class="faq-content">
												<div class="entry">
													<?= $answer ?>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							</div>
						</div>
						<?php $faq_array_ld_json = json_encode($faq_array_ld);?>
						<script type="application/ld+json">
							{
							"@context": "https://schema.org",
							"@type": "FAQPage",
							"mainEntity": [<?= $faq_array_ld_json ;?>]
							}
						</script>
					</div>
				</div>
			</div>
		<?php } ?>
	</div> 
</div> 
<?php get_footer(); ?>
