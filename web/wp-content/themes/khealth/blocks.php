<?php

add_action('acf/init', 'acf_block');
function acf_block() {
	
	if( function_exists('acf_register_block') ) {
		
		acf_register_block(array(
			'name'				=> 'carousel',
			'title'				=> ('Carousel'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'specs',
			'title'				=> ('Small features'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'intro',
			'title'				=> ('Introduction'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'featureslist',
			'title'				=> ('Features cols'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'featured-news',
			'title'				=> ('Featured news'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'article-cta',
			'title'				=> ('Article CTA'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'more-about',
			'title'				=> ('More about'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'cols',
			'title'				=> ('Text cols'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'faq',
			'title'				=> ('FAQ'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'youtube-video',
			'title'				=> ('Youtube video'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'text',
			'title'				=> ('Text'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'cta',
			'title'				=> ('CTA'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'feaure',
			'title'				=> ('Feature'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'feaurer',
			'title'				=> ('Featured in'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'hero',
			'title'				=> ('Hero'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'drugs-info',
			'title'				=> ('Drugs info'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'sticky-menu',
			'title'				=> ('Sticky menu'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'btn-full',
			'title'				=> ('CTA full'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'drug-price',
			'title'				=> ('Drug price'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'icon-banner',
			'title'				=> ('Icon banner'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'doctors-list',
			'title'				=> ('Doctors List'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'doctors-leader',
			'title'				=> ('Doctors Leader'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'how-to-order',
			'title'				=> ('How to order'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'hero-lp',
			'title'				=> ('Hero block lp'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'primary-care-panel',
			'title'				=> ('Primary care panel'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'react-widget',
			'title'				=> ('React widget'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'tab-plan',
			'title'				=> ('Tab plan'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'drug-calculator',
			'title'				=> ('Drug calculator'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'lead-clinicians',
			'title'				=> ('Lead clinicians'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'compare-others',
			'title'				=> ('Compare others'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'hero-parallax',
			'title'				=> ('Hero parallax'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'image-banners',
			'title'				=> ('Social proof banner'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		)); 
		acf_register_block(array(
			'name'				=> 'average-monthly',
			'title'				=> ('Average monthly'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'primary-care-callout',
			'title'				=> ('Primary care callout'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'love-khealth',
			'title'				=> ('3 columns block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'comprehensive-care',
			'title'				=> ('Program block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'take-control',
			'title'				=> ('S curve block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'word-class-doctors',
			'title'				=> ('Doctor carousel'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'press',
			'title'				=> ('Press'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'also-see-in',
			'title'				=> ('Also see in'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'image-cta',
			'title'				=> ('Footer CTA banner'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'testiomonial',
			'title'				=> ('Quote block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'safe-convenient',
			'title'				=> ('4 bullets block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'pricing-image',
			'title'				=> ('Photo and chart block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'uti-medication',
			'title'				=> ('Medication block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'faq-image',
			'title'				=> ('FAQ image'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'recommended-content',
			'title'				=> ('Recommended content carousel'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'learn-more',
			'title'				=> ('Photo and text content block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'what-we-treat',
			'title'				=> ('2 column list block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'steps-block',
			'title'				=> ('4 steps-block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'chart-block',
			'title'				=> ('Chart block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'team-carousel',
			'title'				=> ('Team carousel'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'photo-bullet',
			'title'				=> ('Photo and bullet block'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'disclaimer-banner',
			'title'				=> ('Disclaimer banner'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'bar-chart',
			'title'				=> ('Bar chart'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'warning-Block',
			'title'				=> ('Warning'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'doctor-dots',
			'title'				=> ('Doctor dots'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'hero-hub',
			'title'				=> ('Hero hub'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'proof-banenr-hub',
			'title'				=> ('Proof banenr hub'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'hero-twillio',
			'title'				=> ('Hero Twillio'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));	
		acf_register_block(array(
			'name'				=> 'featured-articles',
			'title'				=> ('Featured articles'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));
		acf_register_block(array(
			'name'				=> 'anchors-search-bar',
			'title'				=> ('Anchors Search Bar'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));  
		acf_register_block(array(
			'name'				=> 'content-hub-videos',
			'title'				=> ('Content Hub Videos'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));  
		acf_register_block(array(
			'name'				=> 'newsletter',
			'title'				=> ('Newsletter'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		));  
		acf_register_block(array(
			'name'				=> 'browse-by-category',
			'title'				=> ('Browse by category'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		)); 
		acf_register_block(array(
			'name'				=> 'browse-by-condition',
			'title'				=> ('Browse by condition'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		)); 
		acf_register_block(array(
			'name'				=> 'symptom-checker-cta',
			'title'				=> ('Symptom checker CTA'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		)); 
		acf_register_block(array(
			'name'				=> 'articles-carousel',
			'title'				=> ('Articles Carousel'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		)); 
		acf_register_block(array(
			'name'				=> 'key-terms',
			'title'				=> ('Key Terms'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		)); 
		acf_register_block(array(
			'name'				=> 'formal-table',
			'title'				=> ('Formal Table'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		)); 
		acf_register_block(array(
			'name'				=> 'newsletter-image',
			'title'				=> ('Newsletter with image option'),
			'render_callback'	=> 'pp_blocks',
			'category'			=> 'layout',
			'icon' => array(
				'background' => '#fdf8f8',
				'foreground' => '#484848',
				'src' => 'image-filter',
			),
		)); 
	}
}

function pp_blocks( $block ) {
	
	$slug = str_replace('acf/', '', $block['name']);
	
	if( file_exists( get_theme_file_path("/blocks/{$slug}.php") ) ) {
		include( get_theme_file_path("/blocks/{$slug}.php") );
	}
}